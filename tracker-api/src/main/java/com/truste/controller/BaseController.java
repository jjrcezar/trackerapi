package com.truste.controller;

import com.truste.common.DateAdapter;
import com.truste.exception.StatusException;
import com.truste.exception.*;
import com.truste.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * This class is meant to be extended by all REST resource "controllers".
 * It contains exception mapping and other common REST API functionality
 */
@Controller
public abstract class BaseController implements ApplicationEventPublisherAware {

    @Autowired
    SecurityService securityService;

    // EXCEPTION MESSAGES [start]
    protected static final String MESSAGE_FORBIDDEN_ACCESS = "Authorization was refused due lack of access rights by this API key." +
            " If you feel this is in error, please check that both your API key and the URL which " +
            "you are attempting to access are correct.";
    protected static final String MESSAGE_SERVER_ERROR = "Please report this failure";
    protected static final String MESSAGE_DATE_ERROR = "Unable to parse Date header. Long milliseconds (UTC/GMT) is recommended";
    protected static final String MESSAGE_CALL_NOT_ALLOWED = "This method without parameters is not allowed";
    protected static final String MESSAGE_API_INFORMATION_MISSING = "This request is missing required API HTTP Header fields.";
    // EXCEPTION MESSAGES [end]

    // EXCEPTION TITLES [start]
    protected static final String TITLE_DATA_STORE_EXCEPTION = "Data format incorrect.";
    protected static final String TITLE_DATA_NOT_FOUND = "Resource not found.";
    protected static final String TITLE_AUTHORIZATION_REFUSED = "Authorization refused.";
    protected static final String TITLE_STALE_REQUEST = "Stale request.";
    protected static final String TITLE_DATABASE_CONNECTION_PROBLEM = "Database Connection Problem";
    // EXCEPTION TITLES [end]

    // DEFAULT VALUES [start]
    public static final String BROWSER_ADDON_API_KEY = "b4e3abfc-6507-4871-8811-9204ec058670";
    protected static final String DEFAULT_PAGE_SIZE = "100";
    protected static final String DEFAULT_PAGE_NUM = "0";
    // DEFAULT VALUES [end]

    // VARIABLES [start]
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    protected ApplicationEventPublisher eventPublisher;
    // VARIABLES [end]

    // EXCEPTION HANDLERS [start]
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DataFormatException.class)
    @ResponseBody
    public StatusException handleDataStoreException(DataFormatException ex, WebRequest request, HttpServletResponse response) {
        log.info("Converting Data Store exception to RestResponse : " + ex.getMessage());
        return new StatusException(HttpStatus.BAD_REQUEST.value(), TITLE_DATA_STORE_EXCEPTION, ex);
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseBody
    public StatusException handleResourceNotFoundException(ResourceNotFoundException ex, WebRequest request, HttpServletResponse response) {
        log.info("ResourceNotFoundException handler:" + ex.getMessage());
        return new StatusException(HttpStatus.NOT_FOUND.value(), TITLE_DATA_NOT_FOUND, ex);
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AuthorizationException.class)
    @ResponseBody
    public StatusException handleAuthorizationException(AuthorizationException ex, WebRequest request, HttpServletResponse response) {
        log.info("AuthorizationException handler:" + ex.getMessage());
        return new StatusException(HttpStatus.FORBIDDEN.value(), TITLE_AUTHORIZATION_REFUSED, ex);
    }

    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    @ExceptionHandler(PreconditionException.class)
    @ResponseBody
    public StatusException handlePreconditionException(PreconditionException ex, WebRequest request, HttpServletResponse response) {
        log.info("PreconditionException handler:" + ex.getMessage());
        return new StatusException(HttpStatus.PRECONDITION_FAILED.value(), null, ex);
    }

    @ResponseStatus(HttpStatus.GONE)
    @ExceptionHandler(StaleRequestException.class)
    @ResponseBody
    public StatusException handleStaleRequestException(StaleRequestException ex, WebRequest request, HttpServletResponse response) {
        log.info("StaleRequestException handler:" + ex.getMessage());
        return new StatusException(HttpStatus.GONE.value(), TITLE_STALE_REQUEST, ex);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DatabaseException.class)
    @ResponseBody
    public StatusException handleDatabaseException(DatabaseException ex, WebRequest request, HttpServletResponse response) {
        log.info("DatabaseException handler:" + ex.getMessage());
        return new StatusException(HttpStatus.INTERNAL_SERVER_ERROR.value(), TITLE_DATABASE_CONNECTION_PROBLEM, ex);
    }
    // EXCEPTION HANDLERS [end]

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }

    protected void checkSecurityAccess(String apiKey, String date, String token, HttpServletRequest request) {
        if (apiKey == null || date == null || token == null
                || apiKey.length() == 0 || date.length() == 0 || token.length() == 0) {
            throw new AuthorizationException(MESSAGE_API_INFORMATION_MISSING);
        }

        String securityToken = null;

        try {
            token = token.replace(' ', '+');
            String command = request.getServletPath().split("/")[1];

            if (!BROWSER_ADDON_API_KEY.equals(apiKey)) {
                verifyTime(date, command);
            }

            securityToken = securityService.getSecurityToken(apiKey, command, request.getServletPath(), date + apiKey);

            if (!token.equals(securityToken)) {
                throw new NullPointerException();
            }
        } catch (NullPointerException ex) {
            throw new AuthorizationException(MESSAGE_FORBIDDEN_ACCESS);
        } catch (PreconditionException ex) {
            throw ex;
        } catch (StaleRequestException ex) {
            throw ex;
        } catch (DatabaseException ex) {
            throw ex;
        } catch (Exception ex) {
            ex.printStackTrace();
            String message = "Unknown reason";
            if (token == null) {
                message = "Authentication SHA1 hash was not found.";
            } else if (token.length() < 27) {
                message = "Hash is not of the correct format (should be SHA1).";
            }
//            NOT NEEDED
//            if (uri==null || uri.getPathSegments()==null || uri.getPathSegments().size()<2 ) {
//                message = "Invalid URL";
//            }
            throw new AuthorizationException(message);
        }
    }

    protected static void verifyTime(String date, String command) throws PreconditionException, StaleRequestException {
        if (date == null || date.length() < 10) {
            throw new PreconditionException(MESSAGE_DATE_ERROR);
        }

        long time = 0;

        try {
            time = Long.parseLong(date);
        } catch (NumberFormatException e) {
            DateAdapter da = new DateAdapter();
            try {
                Date d = da.unmarshal(date);
                time = d.getTime();
            } catch (Exception ex) {
                throw new PreconditionException(MESSAGE_DATE_ERROR);
            }
        }

        if (time == 0) {
            throw new PreconditionException(MESSAGE_DATE_ERROR + " - " + date);
        }

        long currentTime = System.currentTimeMillis();
        int diff = ("recon".equals(command)) ? 1000000 : 300000;

        if (time > currentTime + diff || time < currentTime - diff) {
            throw new StaleRequestException("Current time: " + currentTime + ", request's time: " + time);
        }
    }

    //todo: replace with exception mapping
    public static <T> T checkResourceFound(final T resource) {
        if (resource == null) {
            throw new ResourceNotFoundException("resource not found");
        }
        return resource;
    }

}