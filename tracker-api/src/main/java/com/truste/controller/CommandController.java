package com.truste.controller;


import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/command")
@Api(tags = {"command"})
public class CommandController extends BaseController {

    @RequestMapping(value = "/{command}/{value}",
            method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public String hi(@RequestHeader(value = "Api-Key", required = false) String apiKey,
            @RequestHeader(value = "Date", required = false) String date,
            @RequestHeader(value = "MAC-SHA1", required = false) String token,
            @PathVariable("command") String command,
            @PathVariable("value") String value,
            @RequestParam(value = "test", required = false) String test,
            HttpServletRequest request) {
//        checkSecurityAccess(apiKey, date, token, request);
        return "hello";
    }

}
