package com.truste;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "reconEntityManagerFactory",
        transactionManagerRef = "reconTransactionManager",
        basePackages = {"com.truste.dao"})
public class ReconDatasourceConfig {

    @Primary
    @Bean(name = "reconDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.recon")
    public DataSource reconDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "reconEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
            @Qualifier("reconDataSource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("com.truste.entity")
                .persistenceUnit("recon")
                .build();
    }

    @Primary
    @Bean(name = "reconTransactionManager")
    public PlatformTransactionManager reconTransactionManager(@Qualifier("reconEntityManagerFactory")
            EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

}