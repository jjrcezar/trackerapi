package com.truste.common;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DateAdapter extends XmlAdapter<String, Date> {

    private List<SimpleDateFormat> formats = Arrays.asList(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ"),
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSX"),
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSXX"),
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSXXX"),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX"),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXX"),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
            new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z"),
            new SimpleDateFormat("E, d-MMM-yy HH:mm:ss z"),
            new SimpleDateFormat("EEE MMM d HH:mm:ss z yyyy"));


    @Override
    public String marshal(Date v) throws Exception {
        return formats.get(0).format(v);
    }

    @Override
    public Date unmarshal(String date) throws Exception {
        if(date==null||date.length()<10) return null;
        Date time = null;
        try{
            long t = Long.parseLong(date);
            if(t>0) time = new Date(t);
        }catch(NumberFormatException e){
            for(SimpleDateFormat f : formats){
                try{
                    time = f.parse(date);
                    if(time != null) break;
                }catch(Exception e1){}
            }
        }
        return time;
    }
}
