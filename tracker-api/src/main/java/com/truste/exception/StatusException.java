package com.truste.exception;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StatusException {

    public final int code;
    public final String message;
    public final String title;

    public StatusException(int code, String title, Exception ex) {
        this.code = code;
        this.title = title;
        this.message = ex.getLocalizedMessage();
    }
}
