package com.truste.exception;

/**
 * For HTTP 412 Errors
 */
public class PreconditionException extends RuntimeException {
    public PreconditionException() {
        super();
    }

    public PreconditionException(String message, Throwable cause) {
        super(message, cause);
    }

    public PreconditionException(String message) {
        super(message);
    }

    public PreconditionException(Throwable cause) {
        super(cause);
    }

}
