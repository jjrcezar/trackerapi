package com.truste.exception;

/**
 * For HTTP 410 Errors
 */
public class StaleRequestException extends RuntimeException {
    public StaleRequestException() {
        super();
    }

    public StaleRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public StaleRequestException(String message) {
        super(message);
    }

    public StaleRequestException(Throwable cause) {
        super(cause);
    }

}
