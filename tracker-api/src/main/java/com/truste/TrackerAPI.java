package com.truste;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories("com.truste.dao")
@ComponentScan(basePackages = "com.truste")
public class TrackerAPI extends SpringBootServletInitializer {

    private static final Class<TrackerAPI> trackerApiClass = TrackerAPI.class;
    private static final Logger log = LoggerFactory.getLogger(trackerApiClass);

    public static void main(String[] args) {
        SpringApplication.run(trackerApiClass, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(trackerApiClass);
    }
}
