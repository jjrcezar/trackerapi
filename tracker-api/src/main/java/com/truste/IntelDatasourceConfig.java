package com.truste;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;


@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "intelEntityManagerFactory",
        transactionManagerRef = "intelTransactionManager",
        basePackages = {"com.truste.dao"})
public class IntelDatasourceConfig {

    @Bean(name = "intelDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.intel")
    public DataSource intelDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "intelEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder builder,
            @Qualifier("intelDataSource") DataSource dataSource) {
        return builder
                .dataSource(dataSource)
                .packages("com.truste.entity")
                .persistenceUnit("intel")
                .build();
    }

    @Bean(name = "intelTransactionManager")
    public PlatformTransactionManager intelTransactionManager(@Qualifier("intelEntityManagerFactory")
            EntityManagerFactory entityManagerFactory) {
        return new JpaTransactionManager(entityManagerFactory);
    }

}