package com.truste.service;


import com.truste.dao.ApiKeyRepository;
import com.truste.exception.DatabaseException;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

@Service
public class SecurityService {

    @Autowired
    ApiKeyRepository apiKeyRepository;

    @Cacheable(value = "privateKey")
    public String getPrivateKey(String apiKey, String command) throws DatabaseException {
        String privateKey = null;
        try {
            privateKey = this.apiKeyRepository.findValidApiKey(apiKey, command);
        } catch (Exception ex) {
            throw new DatabaseException(ex.getMessage());
        }
        return privateKey;
    }

    @Cacheable(value = "securityToken")
    public String getSecurityToken(String apiKey, String command, String servletPath, String toHash) throws
            DatabaseException, NoSuchAlgorithmException, InvalidKeyException {
        String securityToken = null;
        try {
            String privateKey = this.getPrivateKey(apiKey, command);
            if (null == privateKey) {
                return null;
            }

            //TODO call respository

            SecretKey secretKey = new SecretKeySpec(Base64.decodeBase64(privateKey.getBytes(Charset.forName("ISO-8859-1")
            )), "HmacSHA1");
            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(secretKey);
            securityToken = new String(Base64.encodeBase64(mac.doFinal(toHash.getBytes(Charset.forName("ISO-8859-1")))));
        } catch (DatabaseException ex) {
            throw ex;
        } catch (NoSuchAlgorithmException ex) {
            throw ex;
        } catch (InvalidKeyException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new DatabaseException(ex.getMessage());
        }

        return securityToken;
    }
}
