package com.truste.dao;

import com.truste.entity.ApiKey;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiKeyRepository extends CrudRepository<ApiKey, Long> {

    @Query("SELECT ak.private_key FROM ApiKey AS ak JOIN ak.commandWhitelists AS cw WHERE ak.isactive AND ak.expiration > CURRENT_DATE  AND ak.api_key_id = :apiKeyId AND cw.api_command_id = :apiCommandId")
    String findValidApiKey(@Param("apiKeyId") String apiKeyId, @Param("apiCommandId")String apiCommandId);
}
