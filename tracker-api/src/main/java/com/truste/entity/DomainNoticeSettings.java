package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "domain_notice_settings", schema = "eu")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainNoticeSettings implements Serializable {
    @Id
    private String domain;

    @Column
    private String frame_width;

    @Column
    private String frame_height;

    @Column
    private String icon_seal;

    public DomainNoticeSettings() {
    }

    public DomainNoticeSettings(String domain, String frame_width, String frame_height, String icon_seal) {
        this.domain = domain;
        this.frame_width = frame_width;
        this.frame_height = frame_height;
        this.icon_seal = icon_seal;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getFrame_width() {
        return frame_width;
    }

    public void setFrame_width(String frame_width) {
        this.frame_width = frame_width;
    }

    public String getFrame_height() {
        return frame_height;
    }

    public void setFrame_height(String frame_height) {
        this.frame_height = frame_height;
    }

    public String getIcon_seal() {
        return icon_seal;
    }

    public void setIcon_seal(String icon_seal) {
        this.icon_seal = icon_seal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DomainNoticeSettings that = (DomainNoticeSettings) o;

        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (frame_width != null ? !frame_width.equals(that.frame_width) : that.frame_width != null) return false;
        if (frame_height != null ? !frame_height.equals(that.frame_height) : that.frame_height != null) return false;
        return icon_seal != null ? icon_seal.equals(that.icon_seal) : that.icon_seal == null;
    }

    @Override
    public int hashCode() {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (frame_width != null ? frame_width.hashCode() : 0);
        result = 31 * result + (frame_height != null ? frame_height.hashCode() : 0);
        result = 31 * result + (icon_seal != null ? icon_seal.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DomainNoticeSettings{" +
                "domain='" + domain + '\'' +
                ", frame_width='" + frame_width + '\'' +
                ", frame_height='" + frame_height + '\'' +
                ", icon_seal='" + icon_seal + '\'' +
                '}';
    }
}
