package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "api_key", schema = "api")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ApiKey implements Serializable {

    private static final long serialVersionUID = -9130098801370682361L;

    @Id
    private String api_key_id;

    @Column
    private String coverage;

    @Column
    private String coverage_record_id;

    @Column
    private Date created;

    @Column
    private Date expiration;

    @Column
    private String private_key;

    @Column
    private Boolean isactive;

    @Column
    private String name;

    @OneToMany(mappedBy = "apiKey")
    private Set<ApiEvent> apiEvents = new HashSet<ApiEvent>();

    @OneToMany(mappedBy = "apiKey")
    private Set<CommandWhitelist> commandWhitelists = new HashSet<CommandWhitelist>();

    @OneToMany(mappedBy = "apiKey")
    private Set<IpWhitelist> ipWhitelists = new HashSet<IpWhitelist>();

    public ApiKey() {
    }

    public ApiKey(String api_key_id, String coverage, String coverage_record_id, Date created, Date expiration,
            String private_key, Boolean isactive, String name) {
        this.api_key_id = api_key_id;
        this.coverage = coverage;
        this.coverage_record_id = coverage_record_id;
        this.created = created;
        this.expiration = expiration;
        this.private_key = private_key;
        this.isactive = isactive;
        this.name = name;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getApi_key_id() {
        return api_key_id;
    }

    public void setApi_key_id(String api_key_id) {
        this.api_key_id = api_key_id;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getCoverage_record_id() {
        return coverage_record_id;
    }

    public void setCoverage_record_id(String coverage_record_id) {
        this.coverage_record_id = coverage_record_id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getExpiration() {
        return expiration;
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    public String getPrivate_key() {
        return private_key;
    }

    public void setPrivate_key(String private_key) {
        this.private_key = private_key;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ApiEvent> getApiEvents() {
        return apiEvents;
    }

    public void setApiEvents(Set<ApiEvent> apiEvents) {
        this.apiEvents = apiEvents;
    }

    public Set<CommandWhitelist> getCommandWhitelists() {
        return commandWhitelists;
    }

    public void setCommandWhitelists(Set<CommandWhitelist> commandWhitelists) {
        this.commandWhitelists = commandWhitelists;
    }

    public Set<IpWhitelist> getIpWhitelists() {
        return ipWhitelists;
    }

    public void setIpWhitelists(Set<IpWhitelist> ipWhitelists) {
        this.ipWhitelists = ipWhitelists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiKey apiKey = (ApiKey) o;

        if (api_key_id != null ? !api_key_id.equals(apiKey.api_key_id) : apiKey.api_key_id != null) return false;
        if (coverage != null ? !coverage.equals(apiKey.coverage) : apiKey.coverage != null) return false;
        if (coverage_record_id != null ? !coverage_record_id.equals(apiKey.coverage_record_id) : apiKey
                .coverage_record_id != null)
            return false;
        if (created != null ? !created.equals(apiKey.created) : apiKey.created != null) return false;
        if (expiration != null ? !expiration.equals(apiKey.expiration) : apiKey.expiration != null) return false;
        if (private_key != null ? !private_key.equals(apiKey.private_key) : apiKey.private_key != null) return false;
        if (isactive != null ? !isactive.equals(apiKey.isactive) : apiKey.isactive != null) return false;
        return name != null ? name.equals(apiKey.name) : apiKey.name == null;
    }

    @Override
    public int hashCode() {
        int result = api_key_id != null ? api_key_id.hashCode() : 0;
        result = 31 * result + (coverage != null ? coverage.hashCode() : 0);
        result = 31 * result + (coverage_record_id != null ? coverage_record_id.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (expiration != null ? expiration.hashCode() : 0);
        result = 31 * result + (private_key != null ? private_key.hashCode() : 0);
        result = 31 * result + (isactive != null ? isactive.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ApiKey{" +
                "api_key_id='" + api_key_id + '\'' +
                ", coverage='" + coverage + '\'' +
                ", coverage_record_id='" + coverage_record_id + '\'' +
                ", created=" + created +
                ", expiration=" + expiration +
                ", private_key='" + private_key + '\'' +
                ", isactive=" + isactive +
                ", name='" + name + '\'' +
                '}';
    }
}
