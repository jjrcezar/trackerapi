package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "organization", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Organization implements Serializable {
    @Column
    private String name;

    @Id
    private String organization_id;

    @Column
    private String displayname;

    @Column
    private String description;

    @Column
    private String website;

    @Column
    private String website_privacy_policy;

    @Column
    private String oba_risk;

    @Column
    private String organization_type;

    @Column
    private String data1;

    @Column
    private String data2;

    @Column
    private String logo;

    @Column
    private String service_privacy_policy;

    @Column
    private String application;

    @Column
    private Date wpp_evaluated;

    @OneToOne
    @JoinColumn(name = "organization_id", insertable = false, updatable = false)
    private OptOutScope optOutScope;

    @OneToMany(mappedBy = "organization")
    private Set<OrganizationAffiliation> organizationAffiliations = new HashSet<OrganizationAffiliation>();

    public Organization() {
    }

    public Organization(String name, String organization_id, String displayname, String description, String website,
            String website_privacy_policy, String oba_risk, String organization_type, String data1, String data2,
            String logo, String service_privacy_policy, String application, Date wpp_evaluated) {
        this.name = name;
        this.organization_id = organization_id;
        this.displayname = displayname;
        this.description = description;
        this.website = website;
        this.website_privacy_policy = website_privacy_policy;
        this.oba_risk = oba_risk;
        this.organization_type = organization_type;
        this.data1 = data1;
        this.data2 = data2;
        this.logo = logo;
        this.service_privacy_policy = service_privacy_policy;
        this.application = application;
        this.wpp_evaluated = wpp_evaluated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWebsite_privacy_policy() {
        return website_privacy_policy;
    }

    public void setWebsite_privacy_policy(String website_privacy_policy) {
        this.website_privacy_policy = website_privacy_policy;
    }

    public String getOba_risk() {
        return oba_risk;
    }

    public void setOba_risk(String oba_risk) {
        this.oba_risk = oba_risk;
    }

    public String getOrganization_type() {
        return organization_type;
    }

    public void setOrganization_type(String organization_type) {
        this.organization_type = organization_type;
    }

    public String getData1() {
        return data1;
    }

    public void setData1(String data1) {
        this.data1 = data1;
    }

    public String getData2() {
        return data2;
    }

    public void setData2(String data2) {
        this.data2 = data2;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getService_privacy_policy() {
        return service_privacy_policy;
    }

    public void setService_privacy_policy(String service_privacy_policy) {
        this.service_privacy_policy = service_privacy_policy;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public Date getWpp_evaluated() {
        return wpp_evaluated;
    }

    public void setWpp_evaluated(Date wpp_evaluated) {
        this.wpp_evaluated = wpp_evaluated;
    }

    public OptOutScope getOptOutScope() {
        return optOutScope;
    }

    public void setOptOutScope(OptOutScope optOutScope) {
        this.optOutScope = optOutScope;
    }

    public Set<OrganizationAffiliation> getOrganizationAffiliations() {
        return organizationAffiliations;
    }

    public void setOrganizationAffiliations(Set<OrganizationAffiliation> organizationAffiliations) {
        this.organizationAffiliations = organizationAffiliations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Organization that = (Organization) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (organization_id != null ? !organization_id.equals(that.organization_id) : that.organization_id != null)
            return false;
        if (displayname != null ? !displayname.equals(that.displayname) : that.displayname != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (website != null ? !website.equals(that.website) : that.website != null) return false;
        if (website_privacy_policy != null ? !website_privacy_policy.equals(that.website_privacy_policy) : that
                .website_privacy_policy != null)
            return false;
        if (oba_risk != null ? !oba_risk.equals(that.oba_risk) : that.oba_risk != null) return false;
        if (organization_type != null ? !organization_type.equals(that.organization_type) : that.organization_type !=
                null)
            return false;
        if (data1 != null ? !data1.equals(that.data1) : that.data1 != null) return false;
        if (data2 != null ? !data2.equals(that.data2) : that.data2 != null) return false;
        if (logo != null ? !logo.equals(that.logo) : that.logo != null) return false;
        if (service_privacy_policy != null ? !service_privacy_policy.equals(that.service_privacy_policy) : that
                .service_privacy_policy != null)
            return false;
        if (application != null ? !application.equals(that.application) : that.application != null) return false;
        return wpp_evaluated != null ? wpp_evaluated.equals(that.wpp_evaluated) : that.wpp_evaluated == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (organization_id != null ? organization_id.hashCode() : 0);
        result = 31 * result + (displayname != null ? displayname.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (website_privacy_policy != null ? website_privacy_policy.hashCode() : 0);
        result = 31 * result + (oba_risk != null ? oba_risk.hashCode() : 0);
        result = 31 * result + (organization_type != null ? organization_type.hashCode() : 0);
        result = 31 * result + (data1 != null ? data1.hashCode() : 0);
        result = 31 * result + (data2 != null ? data2.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + (service_privacy_policy != null ? service_privacy_policy.hashCode() : 0);
        result = 31 * result + (application != null ? application.hashCode() : 0);
        result = 31 * result + (wpp_evaluated != null ? wpp_evaluated.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Organization{" +
                "name='" + name + '\'' +
                ", organization_id='" + organization_id + '\'' +
                ", displayname='" + displayname + '\'' +
                ", description='" + description + '\'' +
                ", website='" + website + '\'' +
                ", website_privacy_policy='" + website_privacy_policy + '\'' +
                ", oba_risk='" + oba_risk + '\'' +
                ", organization_type='" + organization_type + '\'' +
                ", data1='" + data1 + '\'' +
                ", data2='" + data2 + '\'' +
                ", logo='" + logo + '\'' +
                ", service_privacy_policy='" + service_privacy_policy + '\'' +
                ", application='" + application + '\'' +
                ", wpp_evaluated=" + wpp_evaluated +
                '}';
    }
}
