package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "campaign", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Campaign implements Serializable {
    @Column
    private String logo;

    @Column
    private String link;

    @Column
    private String text;

    @Id
    private String campaignid;

    @Column
    private String fulldisplayname;

    @Column
    private String description;

    @Column
    private String advertiserid;

    @ManyToOne
    @JoinColumn(name = "advertiserid", insertable = false, updatable = false)
    private Advertiser advertiser;

    public Campaign() {
    }

    public Campaign(String logo, String link, String text, String campaignid, String fulldisplayname, String
            description, String advertiserid) {
        this.logo = logo;
        this.link = link;
        this.text = text;
        this.campaignid = campaignid;
        this.fulldisplayname = fulldisplayname;
        this.description = description;
        this.advertiserid = advertiserid;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCampaignid() {
        return campaignid;
    }

    public void setCampaignid(String campaignid) {
        this.campaignid = campaignid;
    }

    public String getFulldisplayname() {
        return fulldisplayname;
    }

    public void setFulldisplayname(String fulldisplayname) {
        this.fulldisplayname = fulldisplayname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdvertiserid() {
        return advertiserid;
    }

    public void setAdvertiserid(String advertiserid) {
        this.advertiserid = advertiserid;
    }

    public Advertiser getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(Advertiser advertiser) {
        this.advertiser = advertiser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Campaign campaign = (Campaign) o;

        if (logo != null ? !logo.equals(campaign.logo) : campaign.logo != null) return false;
        if (link != null ? !link.equals(campaign.link) : campaign.link != null) return false;
        if (text != null ? !text.equals(campaign.text) : campaign.text != null) return false;
        if (campaignid != null ? !campaignid.equals(campaign.campaignid) : campaign.campaignid != null) return false;
        if (fulldisplayname != null ? !fulldisplayname.equals(campaign.fulldisplayname) : campaign.fulldisplayname !=
                null)
            return false;
        if (description != null ? !description.equals(campaign.description) : campaign.description != null)
            return false;
        return advertiserid != null ? advertiserid.equals(campaign.advertiserid) : campaign.advertiserid == null;
    }

    @Override
    public int hashCode() {
        int result = logo != null ? logo.hashCode() : 0;
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (campaignid != null ? campaignid.hashCode() : 0);
        result = 31 * result + (fulldisplayname != null ? fulldisplayname.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (advertiserid != null ? advertiserid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Campaign{" +
                "logo='" + logo + '\'' +
                ", link='" + link + '\'' +
                ", text='" + text + '\'' +
                ", campaignid='" + campaignid + '\'' +
                ", fulldisplayname='" + fulldisplayname + '\'' +
                ", description='" + description + '\'' +
                ", advertiserid='" + advertiserid + '\'' +
                '}';
    }
}
