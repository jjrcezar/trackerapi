package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "customer_domain", schema = "eu")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerDomain implements Serializable {

    @Id
    private UUID customer_id;

    @Column
    private String dax;

    @Column
    private String scan_signature;

    @Column
    private Date scan_date;

    @Column
    private Date privacy_policy_date;

    @Column
    private Date privacy_policy_last_modified;

    @Id
    private String domain_id;

    @Column
    private String dax_signature;

    @Column
    private String preference_manager_url;

    public CustomerDomain() {
    }

    public CustomerDomain(UUID customer_id, String dax, String scan_signature, Date scan_date, Date
            privacy_policy_date, Date privacy_policy_last_modified, String domain_id, String dax_signature, String
            preference_manager_url) {
        this.customer_id = customer_id;
        this.dax = dax;
        this.scan_signature = scan_signature;
        this.scan_date = scan_date;
        this.privacy_policy_date = privacy_policy_date;
        this.privacy_policy_last_modified = privacy_policy_last_modified;
        this.domain_id = domain_id;
        this.dax_signature = dax_signature;
        this.preference_manager_url = preference_manager_url;
    }

    public UUID getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(UUID customer_id) {
        this.customer_id = customer_id;
    }

    public String getDax() {
        return dax;
    }

    public void setDax(String dax) {
        this.dax = dax;
    }

    public String getScan_signature() {
        return scan_signature;
    }

    public void setScan_signature(String scan_signature) {
        this.scan_signature = scan_signature;
    }

    public Date getScan_date() {
        return scan_date;
    }

    public void setScan_date(Date scan_date) {
        this.scan_date = scan_date;
    }

    public Date getPrivacy_policy_date() {
        return privacy_policy_date;
    }

    public void setPrivacy_policy_date(Date privacy_policy_date) {
        this.privacy_policy_date = privacy_policy_date;
    }

    public Date getPrivacy_policy_last_modified() {
        return privacy_policy_last_modified;
    }

    public void setPrivacy_policy_last_modified(Date privacy_policy_last_modified) {
        this.privacy_policy_last_modified = privacy_policy_last_modified;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public String getDax_signature() {
        return dax_signature;
    }

    public void setDax_signature(String dax_signature) {
        this.dax_signature = dax_signature;
    }

    public String getPreference_manager_url() {
        return preference_manager_url;
    }

    public void setPreference_manager_url(String preference_manager_url) {
        this.preference_manager_url = preference_manager_url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerDomain that = (CustomerDomain) o;

        if (customer_id != null ? !customer_id.equals(that.customer_id) : that.customer_id != null) return false;
        if (dax != null ? !dax.equals(that.dax) : that.dax != null) return false;
        if (scan_signature != null ? !scan_signature.equals(that.scan_signature) : that.scan_signature != null)
            return false;
        if (scan_date != null ? !scan_date.equals(that.scan_date) : that.scan_date != null) return false;
        if (privacy_policy_date != null ? !privacy_policy_date.equals(that.privacy_policy_date) : that
                .privacy_policy_date != null)
            return false;
        if (privacy_policy_last_modified != null ? !privacy_policy_last_modified.equals(that
                .privacy_policy_last_modified) : that.privacy_policy_last_modified != null)
            return false;
        if (domain_id != null ? !domain_id.equals(that.domain_id) : that.domain_id != null) return false;
        if (dax_signature != null ? !dax_signature.equals(that.dax_signature) : that.dax_signature != null)
            return false;
        return preference_manager_url != null ? preference_manager_url.equals(that.preference_manager_url) : that
                .preference_manager_url == null;
    }

    @Override
    public int hashCode() {
        int result = customer_id != null ? customer_id.hashCode() : 0;
        result = 31 * result + (dax != null ? dax.hashCode() : 0);
        result = 31 * result + (scan_signature != null ? scan_signature.hashCode() : 0);
        result = 31 * result + (scan_date != null ? scan_date.hashCode() : 0);
        result = 31 * result + (privacy_policy_date != null ? privacy_policy_date.hashCode() : 0);
        result = 31 * result + (privacy_policy_last_modified != null ? privacy_policy_last_modified.hashCode() : 0);
        result = 31 * result + (domain_id != null ? domain_id.hashCode() : 0);
        result = 31 * result + (dax_signature != null ? dax_signature.hashCode() : 0);
        result = 31 * result + (preference_manager_url != null ? preference_manager_url.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CustomerDomain{" +
                "customer_id=" + customer_id +
                ", dax='" + dax + '\'' +
                ", scan_signature='" + scan_signature + '\'' +
                ", scan_date=" + scan_date +
                ", privacy_policy_date=" + privacy_policy_date +
                ", privacy_policy_last_modified=" + privacy_policy_last_modified +
                ", domain_id='" + domain_id + '\'' +
                ", dax_signature='" + dax_signature + '\'' +
                ", preference_manager_url='" + preference_manager_url + '\'' +
                '}';
    }
}
