package com.truste.entity;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "prefmgr_adaptor", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PrefmgrAdaptor implements Serializable {
    @Id
    private String affiliate_id;

    @Id
    private String prefmgr_id;

    public PrefmgrAdaptor() {
    }

    public PrefmgrAdaptor(String affiliate_id, String prefmgr_id) {
        this.affiliate_id = affiliate_id;
        this.prefmgr_id = prefmgr_id;
    }

    public String getAffiliate_id() {
        return affiliate_id;
    }

    public void setAffiliate_id(String affiliate_id) {
        this.affiliate_id = affiliate_id;
    }

    public String getPrefmgr_id() {
        return prefmgr_id;
    }

    public void setPrefmgr_id(String prefmgr_id) {
        this.prefmgr_id = prefmgr_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrefmgrAdaptor that = (PrefmgrAdaptor) o;

        if (affiliate_id != null ? !affiliate_id.equals(that.affiliate_id) : that.affiliate_id != null) return false;
        return prefmgr_id != null ? prefmgr_id.equals(that.prefmgr_id) : that.prefmgr_id == null;
    }

    @Override
    public int hashCode() {
        int result = affiliate_id != null ? affiliate_id.hashCode() : 0;
        result = 31 * result + (prefmgr_id != null ? prefmgr_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PrefmgrAdaptor{" +
                "affiliate_id='" + affiliate_id + '\'' +
                ", prefmgr_id='" + prefmgr_id + '\'' +
                '}';
    }
}
