package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "organization_log_pref_optout_prefmgr", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationLogPrefOptoutPrefmgr implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String prefmgr_id;

    @Column
    private String organization_id;

    @Column
    private String status;

    @Column
    private Float duration;

    @Column
    private Date entry_date;

    @Column
    private String client_session;

    @Column
    private String client_useragent;

    public OrganizationLogPrefOptoutPrefmgr() {
    }

    public OrganizationLogPrefOptoutPrefmgr(String prefmgr_id, String organization_id, String status, Float duration,
            Date entry_date, String client_session, String client_useragent) {
        this.prefmgr_id = prefmgr_id;
        this.organization_id = organization_id;
        this.status = status;
        this.duration = duration;
        this.entry_date = entry_date;
        this.client_session = client_session;
        this.client_useragent = client_useragent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPrefmgr_id() {
        return prefmgr_id;
    }

    public void setPrefmgr_id(String prefmgr_id) {
        this.prefmgr_id = prefmgr_id;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public Date getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(Date entry_date) {
        this.entry_date = entry_date;
    }

    public String getClient_session() {
        return client_session;
    }

    public void setClient_session(String client_session) {
        this.client_session = client_session;
    }

    public String getClient_useragent() {
        return client_useragent;
    }

    public void setClient_useragent(String client_useragent) {
        this.client_useragent = client_useragent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganizationLogPrefOptoutPrefmgr that = (OrganizationLogPrefOptoutPrefmgr) o;

        if (prefmgr_id != null ? !prefmgr_id.equals(that.prefmgr_id) : that.prefmgr_id != null) return false;
        if (organization_id != null ? !organization_id.equals(that.organization_id) : that.organization_id != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (entry_date != null ? !entry_date.equals(that.entry_date) : that.entry_date != null) return false;
        if (client_session != null ? !client_session.equals(that.client_session) : that.client_session != null)
            return false;
        return client_useragent != null ? client_useragent.equals(that.client_useragent) : that.client_useragent ==
                null;
    }

    @Override
    public int hashCode() {
        int result = prefmgr_id != null ? prefmgr_id.hashCode() : 0;
        result = 31 * result + (organization_id != null ? organization_id.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (entry_date != null ? entry_date.hashCode() : 0);
        result = 31 * result + (client_session != null ? client_session.hashCode() : 0);
        result = 31 * result + (client_useragent != null ? client_useragent.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrganizationLogPrefOptoutPrefmgr{" +
                "prefmgr_id='" + prefmgr_id + '\'' +
                ", organization_id='" + organization_id + '\'' +
                ", status='" + status + '\'' +
                ", duration=" + duration +
                ", entry_date=" + entry_date +
                ", client_session='" + client_session + '\'' +
                ", client_useragent='" + client_useragent + '\'' +
                '}';
    }
}
