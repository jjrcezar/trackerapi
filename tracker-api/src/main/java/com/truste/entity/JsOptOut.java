package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "js_opt_out", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class JsOptOut implements Serializable {
    @Column
    private String url;

    @Column
    private String method;

    @Id
    private String js_ooid;

    @OneToOne
    @JoinColumn(name = "js_ooid", insertable = false, updatable = false)
    private OptOut optOut;

    public JsOptOut() {
    }

    public JsOptOut(String url, String method, String js_ooid) {
        this.url = url;
        this.method = method;
        this.js_ooid = js_ooid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getJs_ooid() {
        return js_ooid;
    }

    public void setJs_ooid(String js_ooid) {
        this.js_ooid = js_ooid;
    }

    public OptOut getOptOut() {
        return optOut;
    }

    public void setOptOut(OptOut optOut) {
        this.optOut = optOut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        JsOptOut jsOptOut = (JsOptOut) o;

        if (url != null ? !url.equals(jsOptOut.url) : jsOptOut.url != null) return false;
        if (method != null ? !method.equals(jsOptOut.method) : jsOptOut.method != null) return false;
        return js_ooid != null ? js_ooid.equals(jsOptOut.js_ooid) : jsOptOut.js_ooid == null;
    }

    @Override
    public int hashCode() {
        int result = url != null ? url.hashCode() : 0;
        result = 31 * result + (method != null ? method.hashCode() : 0);
        result = 31 * result + (js_ooid != null ? js_ooid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "JsOptOut{" +
                "url='" + url + '\'' +
                ", method='" + method + '\'' +
                ", js_ooid='" + js_ooid + '\'' +
                '}';
    }
}
