package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "log_admarker_audit_details", schema = "stat")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LogAdmarkerAuditDetails implements Serializable {

    @Column
    private Long raw_impressions;

    @Column
    private Long raw_clicks;

    @Column
    private Long rolled_impressions;

    @Column
    private Long rolled_clicks;

    @Column
    private Date raw_start_time;

    @Column
    private Date raw_end_time;

    @Column
    private Boolean isconsistent;

    @Id
    private Long log_admarker_audit_detail_id;

    @Column
    private Long log_admarker_audit_id;

    @Column
    private Date rolled_start_time;

    @Column
    private Date rolled_end_time;

    @Column
    private Date coverage_start;

    @Column
    private Date coverage_end;

    @ManyToOne
    @JoinColumn(name = "log_admarker_audit_id", insertable = false, updatable = false)
    private LogAdmarkerAudit logAdmarkerAudit;

    public LogAdmarkerAuditDetails() {
    }

    public LogAdmarkerAuditDetails(Long raw_impressions, Long raw_clicks, Long rolled_impressions, Long
            rolled_clicks, Date raw_start_time, Date raw_end_time, Boolean isconsistent, Long
            log_admarker_audit_detail_id, Long log_admarker_audit_id, Date rolled_start_time, Date rolled_end_time,
            Date coverage_start, Date coverage_end) {
        this.raw_impressions = raw_impressions;
        this.raw_clicks = raw_clicks;
        this.rolled_impressions = rolled_impressions;
        this.rolled_clicks = rolled_clicks;
        this.raw_start_time = raw_start_time;
        this.raw_end_time = raw_end_time;
        this.isconsistent = isconsistent;
        this.log_admarker_audit_detail_id = log_admarker_audit_detail_id;
        this.log_admarker_audit_id = log_admarker_audit_id;
        this.rolled_start_time = rolled_start_time;
        this.rolled_end_time = rolled_end_time;
        this.coverage_start = coverage_start;
        this.coverage_end = coverage_end;
    }

    public Long getRaw_impressions() {
        return raw_impressions;
    }

    public void setRaw_impressions(Long raw_impressions) {
        this.raw_impressions = raw_impressions;
    }

    public Long getRaw_clicks() {
        return raw_clicks;
    }

    public void setRaw_clicks(Long raw_clicks) {
        this.raw_clicks = raw_clicks;
    }

    public Long getRolled_impressions() {
        return rolled_impressions;
    }

    public void setRolled_impressions(Long rolled_impressions) {
        this.rolled_impressions = rolled_impressions;
    }

    public Long getRolled_clicks() {
        return rolled_clicks;
    }

    public void setRolled_clicks(Long rolled_clicks) {
        this.rolled_clicks = rolled_clicks;
    }

    public Date getRaw_start_time() {
        return raw_start_time;
    }

    public void setRaw_start_time(Date raw_start_time) {
        this.raw_start_time = raw_start_time;
    }

    public Date getRaw_end_time() {
        return raw_end_time;
    }

    public void setRaw_end_time(Date raw_end_time) {
        this.raw_end_time = raw_end_time;
    }

    public Boolean getIsconsistent() {
        return isconsistent;
    }

    public void setIsconsistent(Boolean isconsistent) {
        this.isconsistent = isconsistent;
    }

    public Long getLog_admarker_audit_detail_id() {
        return log_admarker_audit_detail_id;
    }

    public void setLog_admarker_audit_detail_id(Long log_admarker_audit_detail_id) {
        this.log_admarker_audit_detail_id = log_admarker_audit_detail_id;
    }

    public Long getLog_admarker_audit_id() {
        return log_admarker_audit_id;
    }

    public void setLog_admarker_audit_id(Long log_admarker_audit_id) {
        this.log_admarker_audit_id = log_admarker_audit_id;
    }

    public Date getRolled_start_time() {
        return rolled_start_time;
    }

    public void setRolled_start_time(Date rolled_start_time) {
        this.rolled_start_time = rolled_start_time;
    }

    public Date getRolled_end_time() {
        return rolled_end_time;
    }

    public void setRolled_end_time(Date rolled_end_time) {
        this.rolled_end_time = rolled_end_time;
    }

    public Date getCoverage_start() {
        return coverage_start;
    }

    public void setCoverage_start(Date coverage_start) {
        this.coverage_start = coverage_start;
    }

    public Date getCoverage_end() {
        return coverage_end;
    }

    public void setCoverage_end(Date coverage_end) {
        this.coverage_end = coverage_end;
    }

    public LogAdmarkerAudit getLogAdmarkerAudit() {
        return logAdmarkerAudit;
    }

    public void setLogAdmarkerAudit(LogAdmarkerAudit logAdmarkerAudit) {
        this.logAdmarkerAudit = logAdmarkerAudit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogAdmarkerAuditDetails that = (LogAdmarkerAuditDetails) o;

        if (raw_impressions != null ? !raw_impressions.equals(that.raw_impressions) : that.raw_impressions != null)
            return false;
        if (raw_clicks != null ? !raw_clicks.equals(that.raw_clicks) : that.raw_clicks != null) return false;
        if (rolled_impressions != null ? !rolled_impressions.equals(that.rolled_impressions) : that
                .rolled_impressions != null)
            return false;
        if (rolled_clicks != null ? !rolled_clicks.equals(that.rolled_clicks) : that.rolled_clicks != null)
            return false;
        if (raw_start_time != null ? !raw_start_time.equals(that.raw_start_time) : that.raw_start_time != null)
            return false;
        if (raw_end_time != null ? !raw_end_time.equals(that.raw_end_time) : that.raw_end_time != null) return false;
        if (isconsistent != null ? !isconsistent.equals(that.isconsistent) : that.isconsistent != null) return false;
        if (log_admarker_audit_detail_id != null ? !log_admarker_audit_detail_id.equals(that
                .log_admarker_audit_detail_id) : that.log_admarker_audit_detail_id != null)
            return false;
        if (log_admarker_audit_id != null ? !log_admarker_audit_id.equals(that.log_admarker_audit_id) : that
                .log_admarker_audit_id != null)
            return false;
        if (rolled_start_time != null ? !rolled_start_time.equals(that.rolled_start_time) : that.rolled_start_time !=
                null)
            return false;
        if (rolled_end_time != null ? !rolled_end_time.equals(that.rolled_end_time) : that.rolled_end_time != null)
            return false;
        if (coverage_start != null ? !coverage_start.equals(that.coverage_start) : that.coverage_start != null)
            return false;
        return coverage_end != null ? coverage_end.equals(that.coverage_end) : that.coverage_end == null;
    }

    @Override
    public int hashCode() {
        int result = raw_impressions != null ? raw_impressions.hashCode() : 0;
        result = 31 * result + (raw_clicks != null ? raw_clicks.hashCode() : 0);
        result = 31 * result + (rolled_impressions != null ? rolled_impressions.hashCode() : 0);
        result = 31 * result + (rolled_clicks != null ? rolled_clicks.hashCode() : 0);
        result = 31 * result + (raw_start_time != null ? raw_start_time.hashCode() : 0);
        result = 31 * result + (raw_end_time != null ? raw_end_time.hashCode() : 0);
        result = 31 * result + (isconsistent != null ? isconsistent.hashCode() : 0);
        result = 31 * result + (log_admarker_audit_detail_id != null ? log_admarker_audit_detail_id.hashCode() : 0);
        result = 31 * result + (log_admarker_audit_id != null ? log_admarker_audit_id.hashCode() : 0);
        result = 31 * result + (rolled_start_time != null ? rolled_start_time.hashCode() : 0);
        result = 31 * result + (rolled_end_time != null ? rolled_end_time.hashCode() : 0);
        result = 31 * result + (coverage_start != null ? coverage_start.hashCode() : 0);
        result = 31 * result + (coverage_end != null ? coverage_end.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LogAdmarkerAuditDetails{" +
                "raw_impressions=" + raw_impressions +
                ", raw_clicks=" + raw_clicks +
                ", rolled_impressions=" + rolled_impressions +
                ", rolled_clicks=" + rolled_clicks +
                ", raw_start_time=" + raw_start_time +
                ", raw_end_time=" + raw_end_time +
                ", isconsistent=" + isconsistent +
                ", log_admarker_audit_detail_id=" + log_admarker_audit_detail_id +
                ", log_admarker_audit_id=" + log_admarker_audit_id +
                ", rolled_start_time=" + rolled_start_time +
                ", rolled_end_time=" + rolled_end_time +
                ", coverage_start=" + coverage_start +
                ", coverage_end=" + coverage_end +
                '}';
    }
}
