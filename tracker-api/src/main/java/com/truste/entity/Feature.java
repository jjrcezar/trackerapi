package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "feature", schema = "eu")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Feature implements Serializable {
    @Id
    private String feature_id;

    @Column
    private String display_name;

    @Column
    private String description;

    @OneToMany(mappedBy = "feature")
    private Set<DomainFeature> domainFeatures = new HashSet<DomainFeature>();

    public Feature() {
    }

    public Feature(String feature_id, String display_name, String description) {
        this.feature_id = feature_id;
        this.display_name = display_name;
        this.description = description;
    }

    public String getFeature_id() {
        return feature_id;
    }

    public void setFeature_id(String feature_id) {
        this.feature_id = feature_id;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<DomainFeature> getDomainFeatures() {
        return domainFeatures;
    }

    public void setDomainFeatures(Set<DomainFeature> domainFeatures) {
        this.domainFeatures = domainFeatures;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Feature feature = (Feature) o;

        if (feature_id != null ? !feature_id.equals(feature.feature_id) : feature.feature_id != null) return false;
        if (display_name != null ? !display_name.equals(feature.display_name) : feature.display_name != null)
            return false;
        return description != null ? description.equals(feature.description) : feature.description == null;
    }

    @Override
    public int hashCode() {
        int result = feature_id != null ? feature_id.hashCode() : 0;
        result = 31 * result + (display_name != null ? display_name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Feature{" +
                "feature_id='" + feature_id + '\'' +
                ", display_name='" + display_name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
