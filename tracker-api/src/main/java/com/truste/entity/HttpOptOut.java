package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "http_opt_out", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class HttpOptOut implements Serializable {
    @Column
    private String url;

    @Column
    private String method;

    @Column
    private String data;

    @Id
    private String http_ooid;

    @OneToOne
    @JoinColumn(name = "http_ooid", insertable = false, updatable = false)
    private OptOut optOut;

    public HttpOptOut() {
    }

    public HttpOptOut(String url, String method, String data, String http_ooid) {
        this.url = url;
        this.method = method;
        this.data = data;
        this.http_ooid = http_ooid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getHttp_ooid() {
        return http_ooid;
    }

    public void setHttp_ooid(String http_ooid) {
        this.http_ooid = http_ooid;
    }

    public OptOut getOptOut() {
        return optOut;
    }

    public void setOptOut(OptOut optOut) {
        this.optOut = optOut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HttpOptOut that = (HttpOptOut) o;

        if (url != null ? !url.equals(that.url) : that.url != null) return false;
        if (method != null ? !method.equals(that.method) : that.method != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        return http_ooid != null ? http_ooid.equals(that.http_ooid) : that.http_ooid == null;
    }

    @Override
    public int hashCode() {
        int result = url != null ? url.hashCode() : 0;
        result = 31 * result + (method != null ? method.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (http_ooid != null ? http_ooid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "HttpOptOut{" +
                "url='" + url + '\'' +
                ", method='" + method + '\'' +
                ", data='" + data + '\'' +
                ", http_ooid='" + http_ooid + '\'' +
                '}';
    }
}
