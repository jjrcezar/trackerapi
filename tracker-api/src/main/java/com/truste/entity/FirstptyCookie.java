package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "firstpty_cookie", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name", "domain_id"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class FirstptyCookie implements Serializable {
    @Column
    private String name;

    @Column
    private String value;

    @Column
    private String domain_id;

    @Id
    private String cookie1_tid;

    @Column
    private String path;

    @OneToOne
    @JoinColumn(name = "cookie1_tid", insertable = false, updatable = false)
    private Tracker tracker;

    public FirstptyCookie() {
    }

    public FirstptyCookie(String name, String value, String domain_id, String cookie1_tid, String path) {
        this.name = name;
        this.value = value;
        this.domain_id = domain_id;
        this.cookie1_tid = cookie1_tid;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public String getCookie1_tid() {
        return cookie1_tid;
    }

    public void setCookie1_tid(String cookie1_tid) {
        this.cookie1_tid = cookie1_tid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FirstptyCookie that = (FirstptyCookie) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (domain_id != null ? !domain_id.equals(that.domain_id) : that.domain_id != null) return false;
        if (cookie1_tid != null ? !cookie1_tid.equals(that.cookie1_tid) : that.cookie1_tid != null) return false;
        return path != null ? path.equals(that.path) : that.path == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (domain_id != null ? domain_id.hashCode() : 0);
        result = 31 * result + (cookie1_tid != null ? cookie1_tid.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FirstptyCookie{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", domain_id='" + domain_id + '\'' +
                ", cookie1_tid='" + cookie1_tid + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
