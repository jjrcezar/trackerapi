package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "advertiser", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Advertiser implements Serializable {
    @Column
    private String partnerid;

    @Column
    private String fulldisplayname;

    @Column
    private String logo;

    @Column
    private String link;

    @Column
    private String text;

    @Column
    private String dataprovidername;

    @Column
    private String dataproviderlink;

    @Column
    private String dataprovidertext;

    @Id
    private String advertiserid;

    @ManyToOne
    @JoinColumn(name = "partnerid", insertable = false, updatable = false)
    private Partner partner;

    @OneToMany(mappedBy = "advertiser")
    private Set<Campaign> campaigns = new HashSet<Campaign>();

    public Advertiser() {
    }

    public Advertiser(String partnerid, String fulldisplayname, String logo, String link, String text, String
            dataprovidername, String dataproviderlink, String dataprovidertext, String advertiserid) {
        this.partnerid = partnerid;
        this.fulldisplayname = fulldisplayname;
        this.logo = logo;
        this.link = link;
        this.text = text;
        this.dataprovidername = dataprovidername;
        this.dataproviderlink = dataproviderlink;
        this.dataprovidertext = dataprovidertext;
        this.advertiserid = advertiserid;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getFulldisplayname() {
        return fulldisplayname;
    }

    public void setFulldisplayname(String fulldisplayname) {
        this.fulldisplayname = fulldisplayname;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDataprovidername() {
        return dataprovidername;
    }

    public void setDataprovidername(String dataprovidername) {
        this.dataprovidername = dataprovidername;
    }

    public String getDataproviderlink() {
        return dataproviderlink;
    }

    public void setDataproviderlink(String dataproviderlink) {
        this.dataproviderlink = dataproviderlink;
    }

    public String getDataprovidertext() {
        return dataprovidertext;
    }

    public void setDataprovidertext(String dataprovidertext) {
        this.dataprovidertext = dataprovidertext;
    }

    public String getAdvertiserid() {
        return advertiserid;
    }

    public void setAdvertiserid(String advertiserid) {
        this.advertiserid = advertiserid;
    }

    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public Set<Campaign> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(Set<Campaign> campaigns) {
        this.campaigns = campaigns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Advertiser that = (Advertiser) o;

        if (partnerid != null ? !partnerid.equals(that.partnerid) : that.partnerid != null) return false;
        if (fulldisplayname != null ? !fulldisplayname.equals(that.fulldisplayname) : that.fulldisplayname != null)
            return false;
        if (logo != null ? !logo.equals(that.logo) : that.logo != null) return false;
        if (link != null ? !link.equals(that.link) : that.link != null) return false;
        if (text != null ? !text.equals(that.text) : that.text != null) return false;
        if (dataprovidername != null ? !dataprovidername.equals(that.dataprovidername) : that.dataprovidername != null)
            return false;
        if (dataproviderlink != null ? !dataproviderlink.equals(that.dataproviderlink) : that.dataproviderlink != null)
            return false;
        if (dataprovidertext != null ? !dataprovidertext.equals(that.dataprovidertext) : that.dataprovidertext != null)
            return false;
        return advertiserid != null ? advertiserid.equals(that.advertiserid) : that.advertiserid == null;
    }

    @Override
    public int hashCode() {
        int result = partnerid != null ? partnerid.hashCode() : 0;
        result = 31 * result + (fulldisplayname != null ? fulldisplayname.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (dataprovidername != null ? dataprovidername.hashCode() : 0);
        result = 31 * result + (dataproviderlink != null ? dataproviderlink.hashCode() : 0);
        result = 31 * result + (dataprovidertext != null ? dataprovidertext.hashCode() : 0);
        result = 31 * result + (advertiserid != null ? advertiserid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Advertiser{" +
                "partnerid='" + partnerid + '\'' +
                ", fulldisplayname='" + fulldisplayname + '\'' +
                ", logo='" + logo + '\'' +
                ", link='" + link + '\'' +
                ", text='" + text + '\'' +
                ", dataprovidername='" + dataprovidername + '\'' +
                ", dataproviderlink='" + dataproviderlink + '\'' +
                ", dataprovidertext='" + dataprovidertext + '\'' +
                ", advertiserid='" + advertiserid + '\'' +
                '}';
    }
}
