package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "thirdpty_cookie", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"domain_id", "name", "value"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ThirdptyCookie implements Serializable {
    @Column
    private String domain_id;

    @Column
    private String name;

    @Column
    private String value;

    @Id
    private String cookie3_tid;

    @Column
    private String path;

    @ManyToOne
    @JoinColumn(name = "domain_id", insertable = false, updatable = false)
    private Domain domain;

    @OneToOne
    @JoinColumn(name = "cookie3_tid", insertable = false, updatable = false)
    private Tracker tracker;

    public ThirdptyCookie() {
    }

    public ThirdptyCookie(String domain_id, String name, String value, String cookie3_tid, String path) {
        this.domain_id = domain_id;
        this.name = name;
        this.value = value;
        this.cookie3_tid = cookie3_tid;
        this.path = path;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCookie3_tid() {
        return cookie3_tid;
    }

    public void setCookie3_tid(String cookie3_tid) {
        this.cookie3_tid = cookie3_tid;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ThirdptyCookie that = (ThirdptyCookie) o;

        if (domain_id != null ? !domain_id.equals(that.domain_id) : that.domain_id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (cookie3_tid != null ? !cookie3_tid.equals(that.cookie3_tid) : that.cookie3_tid != null) return false;
        return path != null ? path.equals(that.path) : that.path == null;
    }

    @Override
    public int hashCode() {
        int result = domain_id != null ? domain_id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (cookie3_tid != null ? cookie3_tid.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ThirdptyCookie{" +
                "domain_id='" + domain_id + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", cookie3_tid='" + cookie3_tid + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
