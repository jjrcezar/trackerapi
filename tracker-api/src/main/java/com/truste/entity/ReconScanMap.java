package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "recon_scan_map", schema = "recon")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconScanMap implements Serializable {
    @Id
    private String scan_domain;

    @Id
    private String recon_table;

    @Id
    private Long recon_table_id;

    @ManyToOne
    @JoinColumn(name = "scan_domain", insertable = false, updatable = false)
    private ReconScan reconScan;

    public ReconScanMap() {
    }

    public ReconScanMap(String scan_domain, String recon_table, Long recon_table_id) {
        this.scan_domain = scan_domain;
        this.recon_table = recon_table;
        this.recon_table_id = recon_table_id;
    }

    public String getScan_domain() {
        return scan_domain;
    }

    public void setScan_domain(String scan_domain) {
        this.scan_domain = scan_domain;
    }

    public String getRecon_table() {
        return recon_table;
    }

    public void setRecon_table(String recon_table) {
        this.recon_table = recon_table;
    }

    public Long getRecon_table_id() {
        return recon_table_id;
    }

    public void setRecon_table_id(Long recon_table_id) {
        this.recon_table_id = recon_table_id;
    }

    public ReconScan getReconScan() {
        return reconScan;
    }

    public void setReconScan(ReconScan reconScan) {
        this.reconScan = reconScan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconScanMap that = (ReconScanMap) o;

        if (scan_domain != null ? !scan_domain.equals(that.scan_domain) : that.scan_domain != null) return false;
        if (recon_table != null ? !recon_table.equals(that.recon_table) : that.recon_table != null) return false;
        return recon_table_id != null ? recon_table_id.equals(that.recon_table_id) : that.recon_table_id == null;
    }

    @Override
    public int hashCode() {
        int result = scan_domain != null ? scan_domain.hashCode() : 0;
        result = 31 * result + (recon_table != null ? recon_table.hashCode() : 0);
        result = 31 * result + (recon_table_id != null ? recon_table_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconScanMap{" +
                "scan_domain='" + scan_domain + '\'' +
                ", recon_table='" + recon_table + '\'' +
                ", recon_table_id=" + recon_table_id +
                '}';
    }
}
