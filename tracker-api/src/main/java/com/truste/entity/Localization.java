package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "localization", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Localization implements Serializable {
    @Id
    private String locale;

    @Id
    private String key;

    @Column
    private String translation;

    public Localization() {
    }

    public Localization(String locale, String key, String translation) {
        this.locale = locale;
        this.key = key;
        this.translation = translation;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Localization that = (Localization) o;

        if (locale != null ? !locale.equals(that.locale) : that.locale != null) return false;
        if (key != null ? !key.equals(that.key) : that.key != null) return false;
        return translation != null ? translation.equals(that.translation) : that.translation == null;
    }

    @Override
    public int hashCode() {
        int result = locale != null ? locale.hashCode() : 0;
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + (translation != null ? translation.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Localization{" +
                "locale='" + locale + '\'' +
                ", key='" + key + '\'' +
                ", translation='" + translation + '\'' +
                '}';
    }
}
