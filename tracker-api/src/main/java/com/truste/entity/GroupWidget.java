package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "group_widget", schema = "portal", uniqueConstraints = {
        @UniqueConstraint(columnNames = "display_name")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupWidget implements Serializable {

    @Id
    private String group_id;

    @Column
    private String display_name;

    @Column
    private String description;

    @OneToMany(mappedBy = "groupWidget")
    private Set<UserWidget> userWidgets = new HashSet<UserWidget>();

    public GroupWidget() {
    }

    public GroupWidget(String group_id, String display_name, String description) {
        this.group_id = group_id;
        this.display_name = display_name;
        this.description = description;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<UserWidget> getUserWidgets() {
        return userWidgets;
    }

    public void setUserWidgets(Set<UserWidget> userWidgets) {
        this.userWidgets = userWidgets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupWidget that = (GroupWidget) o;

        if (group_id != null ? !group_id.equals(that.group_id) : that.group_id != null) return false;
        if (display_name != null ? !display_name.equals(that.display_name) : that.display_name != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = group_id != null ? group_id.hashCode() : 0;
        result = 31 * result + (display_name != null ? display_name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "GroupWidget{" +
                "group_id='" + group_id + '\'' +
                ", display_name='" + display_name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
