package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "interstitial_link", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class InterstitialLink implements Serializable {
    @Id
    private String link_id;

    @Column
    private String name;

    @Column
    private String description;

    public InterstitialLink() {
    }

    public InterstitialLink(String link_id, String name, String description) {
        this.link_id = link_id;
        this.name = name;
        this.description = description;
    }

    public String getLink_id() {
        return link_id;
    }

    public void setLink_id(String link_id) {
        this.link_id = link_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InterstitialLink that = (InterstitialLink) o;

        if (link_id != null ? !link_id.equals(that.link_id) : that.link_id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = link_id != null ? link_id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "InterstitialLink{" +
                "link_id='" + link_id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
