package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "domain", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {
    @Column
    private String type;

    @Column
    private String name;

    @Id
    private String domain_id;

    @OneToMany(mappedBy = "domain")
    private Set<DomainAffiliation> domainAffiliations = new HashSet<DomainAffiliation>();

    @OneToOne
    @JoinColumn(name = "domain_id", insertable = false, updatable = false)
    private OptOutScope optOutScope;

    @OneToMany(mappedBy = "domain")
    private Set<ThirdptyCookie> thirdptyCookies = new HashSet<ThirdptyCookie>();

    public Domain() {
    }

    public Domain(String type, String name, String domain_id) {
        this.type = type;
        this.name = name;
        this.domain_id = domain_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public Set<DomainAffiliation> getDomainAffiliations() {
        return domainAffiliations;
    }

    public void setDomainAffiliations(Set<DomainAffiliation> domainAffiliations) {
        this.domainAffiliations = domainAffiliations;
    }

    public OptOutScope getOptOutScope() {
        return optOutScope;
    }

    public void setOptOutScope(OptOutScope optOutScope) {
        this.optOutScope = optOutScope;
    }

    public Set<ThirdptyCookie> getThirdptyCookies() {
        return thirdptyCookies;
    }

    public void setThirdptyCookies(Set<ThirdptyCookie> thirdptyCookies) {
        this.thirdptyCookies = thirdptyCookies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Domain domain = (Domain) o;

        if (type != null ? !type.equals(domain.type) : domain.type != null) return false;
        if (name != null ? !name.equals(domain.name) : domain.name != null) return false;
        return domain_id != null ? domain_id.equals(domain.domain_id) : domain.domain_id == null;
    }

    @Override
    public int hashCode() {
        int result = type != null ? type.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (domain_id != null ? domain_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Domain{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", domain_id='" + domain_id + '\'' +
                '}';
    }
}
