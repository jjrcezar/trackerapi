package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "recon_etag", schema = "recon", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"domain", "name", "page_url"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconEtag implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String domain;

    @Column
    private String name;

    @Column
    private String value;

    @Column
    private String page_url;

    @Column
    private String loader_url;

    @Column
    private String collectedby;

    @Column
    private Date collected;

    @Column
    private Date created;

    @Column
    private Date updated;

    @Column
    private Boolean evaluated;

    @Column
    private String use;

    public ReconEtag() {
    }

    public ReconEtag(Long id, String domain, String name, String value, String page_url, String loader_url, String
            collectedby, Date collected, Date created, Date updated, Boolean evaluated, String use) {
        this.id = id;
        this.domain = domain;
        this.name = name;
        this.value = value;
        this.page_url = page_url;
        this.loader_url = loader_url;
        this.collectedby = collectedby;
        this.collected = collected;
        this.created = created;
        this.updated = updated;
        this.evaluated = evaluated;
        this.use = use;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPage_url() {
        return page_url;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public String getLoader_url() {
        return loader_url;
    }

    public void setLoader_url(String loader_url) {
        this.loader_url = loader_url;
    }

    public String getCollectedby() {
        return collectedby;
    }

    public void setCollectedby(String collectedby) {
        this.collectedby = collectedby;
    }

    public Date getCollected() {
        return collected;
    }

    public void setCollected(Date collected) {
        this.collected = collected;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Boolean getEvaluated() {
        return evaluated;
    }

    public void setEvaluated(Boolean evaluated) {
        this.evaluated = evaluated;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconEtag reconEtag = (ReconEtag) o;

        if (id != null ? !id.equals(reconEtag.id) : reconEtag.id != null) return false;
        if (domain != null ? !domain.equals(reconEtag.domain) : reconEtag.domain != null) return false;
        if (name != null ? !name.equals(reconEtag.name) : reconEtag.name != null) return false;
        if (value != null ? !value.equals(reconEtag.value) : reconEtag.value != null) return false;
        if (page_url != null ? !page_url.equals(reconEtag.page_url) : reconEtag.page_url != null) return false;
        if (loader_url != null ? !loader_url.equals(reconEtag.loader_url) : reconEtag.loader_url != null) return false;
        if (collectedby != null ? !collectedby.equals(reconEtag.collectedby) : reconEtag.collectedby != null)
            return false;
        if (collected != null ? !collected.equals(reconEtag.collected) : reconEtag.collected != null) return false;
        if (created != null ? !created.equals(reconEtag.created) : reconEtag.created != null) return false;
        if (updated != null ? !updated.equals(reconEtag.updated) : reconEtag.updated != null) return false;
        if (evaluated != null ? !evaluated.equals(reconEtag.evaluated) : reconEtag.evaluated != null) return false;
        return use != null ? use.equals(reconEtag.use) : reconEtag.use == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (page_url != null ? page_url.hashCode() : 0);
        result = 31 * result + (loader_url != null ? loader_url.hashCode() : 0);
        result = 31 * result + (collectedby != null ? collectedby.hashCode() : 0);
        result = 31 * result + (collected != null ? collected.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (evaluated != null ? evaluated.hashCode() : 0);
        result = 31 * result + (use != null ? use.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconEtag{" +
                "id=" + id +
                ", domain='" + domain + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", page_url='" + page_url + '\'' +
                ", loader_url='" + loader_url + '\'' +
                ", collectedby='" + collectedby + '\'' +
                ", collected=" + collected +
                ", created=" + created +
                ", updated=" + updated +
                ", evaluated=" + evaluated +
                ", use='" + use + '\'' +
                '}';
    }
}
