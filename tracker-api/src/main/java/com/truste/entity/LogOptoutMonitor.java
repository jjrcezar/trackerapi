package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "log_optout_monitor", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LogOptoutMonitor implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date optout_time;

    @Column
    private String organization;

    @Column
    private String optout_url;

    @Column
    private String domain;

    @Column
    private String cookie_name;

    @Column
    private Integer success;

    @Column
    private Integer failure;

    @Column
    private Float duration;

    @Column
    private Float pageloading_duration;

    @Column
    private Integer has_completed;

    @Column
    private String cookie_value;

    @Column
    private Date expiration_date;

    public LogOptoutMonitor() {
    }

    public LogOptoutMonitor(Date optout_time, String organization, String optout_url, String domain, String
            cookie_name, Integer success, Integer failure, Float duration, Float pageloading_duration, Integer
            has_completed, String cookie_value, Date expiration_date) {
        this.optout_time = optout_time;
        this.organization = organization;
        this.optout_url = optout_url;
        this.domain = domain;
        this.cookie_name = cookie_name;
        this.success = success;
        this.failure = failure;
        this.duration = duration;
        this.pageloading_duration = pageloading_duration;
        this.has_completed = has_completed;
        this.cookie_value = cookie_value;
        this.expiration_date = expiration_date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getOptout_time() {
        return optout_time;
    }

    public void setOptout_time(Date optout_time) {
        this.optout_time = optout_time;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getOptout_url() {
        return optout_url;
    }

    public void setOptout_url(String optout_url) {
        this.optout_url = optout_url;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCookie_name() {
        return cookie_name;
    }

    public void setCookie_name(String cookie_name) {
        this.cookie_name = cookie_name;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public Integer getFailure() {
        return failure;
    }

    public void setFailure(Integer failure) {
        this.failure = failure;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public Float getPageloading_duration() {
        return pageloading_duration;
    }

    public void setPageloading_duration(Float pageloading_duration) {
        this.pageloading_duration = pageloading_duration;
    }

    public Integer getHas_completed() {
        return has_completed;
    }

    public void setHas_completed(Integer has_completed) {
        this.has_completed = has_completed;
    }

    public String getCookie_value() {
        return cookie_value;
    }

    public void setCookie_value(String cookie_value) {
        this.cookie_value = cookie_value;
    }

    public Date getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogOptoutMonitor that = (LogOptoutMonitor) o;

        if (optout_time != null ? !optout_time.equals(that.optout_time) : that.optout_time != null) return false;
        if (organization != null ? !organization.equals(that.organization) : that.organization != null) return false;
        if (optout_url != null ? !optout_url.equals(that.optout_url) : that.optout_url != null) return false;
        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (cookie_name != null ? !cookie_name.equals(that.cookie_name) : that.cookie_name != null) return false;
        if (success != null ? !success.equals(that.success) : that.success != null) return false;
        if (failure != null ? !failure.equals(that.failure) : that.failure != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (pageloading_duration != null ? !pageloading_duration.equals(that.pageloading_duration) : that
                .pageloading_duration != null)
            return false;
        if (has_completed != null ? !has_completed.equals(that.has_completed) : that.has_completed != null)
            return false;
        if (cookie_value != null ? !cookie_value.equals(that.cookie_value) : that.cookie_value != null) return false;
        return expiration_date != null ? expiration_date.equals(that.expiration_date) : that.expiration_date == null;
    }

    @Override
    public int hashCode() {
        int result = optout_time != null ? optout_time.hashCode() : 0;
        result = 31 * result + (organization != null ? organization.hashCode() : 0);
        result = 31 * result + (optout_url != null ? optout_url.hashCode() : 0);
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (cookie_name != null ? cookie_name.hashCode() : 0);
        result = 31 * result + (success != null ? success.hashCode() : 0);
        result = 31 * result + (failure != null ? failure.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (pageloading_duration != null ? pageloading_duration.hashCode() : 0);
        result = 31 * result + (has_completed != null ? has_completed.hashCode() : 0);
        result = 31 * result + (cookie_value != null ? cookie_value.hashCode() : 0);
        result = 31 * result + (expiration_date != null ? expiration_date.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LogOptoutMonitor{" +
                "optout_time=" + optout_time +
                ", organization='" + organization + '\'' +
                ", optout_url='" + optout_url + '\'' +
                ", domain='" + domain + '\'' +
                ", cookie_name='" + cookie_name + '\'' +
                ", success=" + success +
                ", failure=" + failure +
                ", duration=" + duration +
                ", pageloading_duration=" + pageloading_duration +
                ", has_completed=" + has_completed +
                ", cookie_value='" + cookie_value + '\'' +
                ", expiration_date=" + expiration_date +
                '}';
    }
}
