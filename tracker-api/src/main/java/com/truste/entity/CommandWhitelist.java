package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "command_whitelist", schema = "api")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CommandWhitelist implements Serializable {

    private static final long serialVersionUID = -5729551286254658902L;

    @Id
    private String api_key_id;

    @Id
    private String api_command_id;

    @ManyToOne
    @JoinColumn(name = "api_command_id", insertable = false, updatable = false)
    private ApiCommand apiCommand;

    @ManyToOne
    @JoinColumn(name = "api_key_id", insertable = false, updatable = false)
    private ApiKey apiKey;

    public CommandWhitelist() {
    }

    public CommandWhitelist(String api_key_id, String api_command_id) {
        this.api_key_id = api_key_id;
        this.api_command_id = api_command_id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getApi_key_id() {
        return api_key_id;
    }

    public void setApi_key_id(String api_key_id) {
        this.api_key_id = api_key_id;
    }

    public String getApi_command_id() {
        return api_command_id;
    }

    public void setApi_command_id(String api_command_id) {
        this.api_command_id = api_command_id;
    }

    public ApiCommand getApiCommand() {
        return apiCommand;
    }

    public void setApiCommand(ApiCommand apiCommand) {
        this.apiCommand = apiCommand;
    }

    public ApiKey getApiKey() {
        return apiKey;
    }

    public void setApiKey(ApiKey apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommandWhitelist that = (CommandWhitelist) o;

        if (api_key_id != null ? !api_key_id.equals(that.api_key_id) : that.api_key_id != null) return false;
        return api_command_id != null ? api_command_id.equals(that.api_command_id) : that.api_command_id == null;
    }

    @Override
    public int hashCode() {
        int result = api_key_id != null ? api_key_id.hashCode() : 0;
        result = 31 * result + (api_command_id != null ? api_command_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CommandWhitelist{" +
                "api_key_id='" + api_key_id + '\'' +
                ", api_command_id='" + api_command_id + '\'' +
                '}';
    }
}
