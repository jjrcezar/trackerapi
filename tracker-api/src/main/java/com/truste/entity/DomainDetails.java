package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "domain_details", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainDetails implements Serializable {

    @Id
    private String domain_id;

    @Column
    private String tpl_status;

    @Column
    private Boolean is_tracker;

    @Column
    private Boolean truste;

    @Column
    private Boolean uses_flash_cookies;

    @Column
    private Integer oba_risk;

    @Column
    private String optout_url;

    @Column
    private Date optout_url_evaluated;

    public DomainDetails() {
    }

    public DomainDetails(String domain_id, String tpl_status, Boolean is_tracker, Boolean truste, Boolean
            uses_flash_cookies, Integer oba_risk, String optout_url, Date optout_url_evaluated) {
        this.domain_id = domain_id;
        this.tpl_status = tpl_status;
        this.is_tracker = is_tracker;
        this.truste = truste;
        this.uses_flash_cookies = uses_flash_cookies;
        this.oba_risk = oba_risk;
        this.optout_url = optout_url;
        this.optout_url_evaluated = optout_url_evaluated;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public String getTpl_status() {
        return tpl_status;
    }

    public void setTpl_status(String tpl_status) {
        this.tpl_status = tpl_status;
    }

    public Boolean getIs_tracker() {
        return is_tracker;
    }

    public void setIs_tracker(Boolean is_tracker) {
        this.is_tracker = is_tracker;
    }

    public Boolean getTruste() {
        return truste;
    }

    public void setTruste(Boolean truste) {
        this.truste = truste;
    }

    public Boolean getUses_flash_cookies() {
        return uses_flash_cookies;
    }

    public void setUses_flash_cookies(Boolean uses_flash_cookies) {
        this.uses_flash_cookies = uses_flash_cookies;
    }

    public Integer getOba_risk() {
        return oba_risk;
    }

    public void setOba_risk(Integer oba_risk) {
        this.oba_risk = oba_risk;
    }

    public String getOptout_url() {
        return optout_url;
    }

    public void setOptout_url(String optout_url) {
        this.optout_url = optout_url;
    }

    public Date getOptout_url_evaluated() {
        return optout_url_evaluated;
    }

    public void setOptout_url_evaluated(Date optout_url_evaluated) {
        this.optout_url_evaluated = optout_url_evaluated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DomainDetails that = (DomainDetails) o;

        if (domain_id != null ? !domain_id.equals(that.domain_id) : that.domain_id != null) return false;
        if (tpl_status != null ? !tpl_status.equals(that.tpl_status) : that.tpl_status != null) return false;
        if (is_tracker != null ? !is_tracker.equals(that.is_tracker) : that.is_tracker != null) return false;
        if (truste != null ? !truste.equals(that.truste) : that.truste != null) return false;
        if (uses_flash_cookies != null ? !uses_flash_cookies.equals(that.uses_flash_cookies) : that
                .uses_flash_cookies != null)
            return false;
        if (oba_risk != null ? !oba_risk.equals(that.oba_risk) : that.oba_risk != null) return false;
        if (optout_url != null ? !optout_url.equals(that.optout_url) : that.optout_url != null) return false;
        return optout_url_evaluated != null ? optout_url_evaluated.equals(that.optout_url_evaluated) : that
                .optout_url_evaluated == null;
    }

    @Override
    public int hashCode() {
        int result = domain_id != null ? domain_id.hashCode() : 0;
        result = 31 * result + (tpl_status != null ? tpl_status.hashCode() : 0);
        result = 31 * result + (is_tracker != null ? is_tracker.hashCode() : 0);
        result = 31 * result + (truste != null ? truste.hashCode() : 0);
        result = 31 * result + (uses_flash_cookies != null ? uses_flash_cookies.hashCode() : 0);
        result = 31 * result + (oba_risk != null ? oba_risk.hashCode() : 0);
        result = 31 * result + (optout_url != null ? optout_url.hashCode() : 0);
        result = 31 * result + (optout_url_evaluated != null ? optout_url_evaluated.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DomainDetails{" +
                "domain_id='" + domain_id + '\'' +
                ", tpl_status='" + tpl_status + '\'' +
                ", is_tracker=" + is_tracker +
                ", truste=" + truste +
                ", uses_flash_cookies=" + uses_flash_cookies +
                ", oba_risk=" + oba_risk +
                ", optout_url='" + optout_url + '\'' +
                ", optout_url_evaluated=" + optout_url_evaluated +
                '}';
    }
}
