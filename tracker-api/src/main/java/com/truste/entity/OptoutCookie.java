package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "optout_cookie", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name", "domain_id"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OptoutCookie implements Serializable {
    @Id
    private String optout_ooid;

    @Column
    private String name;

    @Column
    private String value;

    @Column
    private String path;

    @Column
    private String domain_id;

    public OptoutCookie() {
    }

    public OptoutCookie(String optout_ooid, String name, String value, String path, String domain_id) {
        this.optout_ooid = optout_ooid;
        this.name = name;
        this.value = value;
        this.path = path;
        this.domain_id = domain_id;
    }

    public String getOptout_ooid() {
        return optout_ooid;
    }

    public void setOptout_ooid(String optout_ooid) {
        this.optout_ooid = optout_ooid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OptoutCookie that = (OptoutCookie) o;

        if (optout_ooid != null ? !optout_ooid.equals(that.optout_ooid) : that.optout_ooid != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (path != null ? !path.equals(that.path) : that.path != null) return false;
        return domain_id != null ? domain_id.equals(that.domain_id) : that.domain_id == null;
    }

    @Override
    public int hashCode() {
        int result = optout_ooid != null ? optout_ooid.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (domain_id != null ? domain_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OptoutCookie{" +
                "optout_ooid='" + optout_ooid + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", path='" + path + '\'' +
                ", domain_id='" + domain_id + '\'' +
                '}';
    }
}
