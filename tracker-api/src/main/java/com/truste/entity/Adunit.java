package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "adunit", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Adunit implements Serializable {

    @Id
    private String size;

    @Column
    private String plc;

    @Column
    private String iplc;

    @Column
    private String ox;

    @Column
    private String oy;

    @Column
    private String bg;

    @Column
    private String op;

    @Column
    private String zi;

    @Column
    private String icon;

    @Column
    private String iconcam;

    @Column
    private String icontext;

    @Column
    private String optouttext;

    @Column
    private String optoutlink;

    @Column
    private String placeholdertext;

    @Column
    private String placeholderlink;

    @Column
    private String cam;

    @Column
    private Date modified;

    @Id
    private String level;

    @Id
    private String eid;

    @OneToMany(mappedBy = "adunit")
    private Set<Interstitial> interstitials = new HashSet<Interstitial>();

    public Adunit() {
    }

    public Adunit(String size, String plc, String iplc, String ox, String oy, String bg, String op, String zi, String
            icon, String iconcam, String icontext, String optouttext, String optoutlink, String placeholdertext,
            String placeholderlink, String cam, Date modified, String level, String eid) {
        this.size = size;
        this.plc = plc;
        this.iplc = iplc;
        this.ox = ox;
        this.oy = oy;
        this.bg = bg;
        this.op = op;
        this.zi = zi;
        this.icon = icon;
        this.iconcam = iconcam;
        this.icontext = icontext;
        this.optouttext = optouttext;
        this.optoutlink = optoutlink;
        this.placeholdertext = placeholdertext;
        this.placeholderlink = placeholderlink;
        this.cam = cam;
        this.modified = modified;
        this.level = level;
        this.eid = eid;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPlc() {
        return plc;
    }

    public void setPlc(String plc) {
        this.plc = plc;
    }

    public String getIplc() {
        return iplc;
    }

    public void setIplc(String iplc) {
        this.iplc = iplc;
    }

    public String getOx() {
        return ox;
    }

    public void setOx(String ox) {
        this.ox = ox;
    }

    public String getOy() {
        return oy;
    }

    public void setOy(String oy) {
        this.oy = oy;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getZi() {
        return zi;
    }

    public void setZi(String zi) {
        this.zi = zi;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconcam() {
        return iconcam;
    }

    public void setIconcam(String iconcam) {
        this.iconcam = iconcam;
    }

    public String getIcontext() {
        return icontext;
    }

    public void setIcontext(String icontext) {
        this.icontext = icontext;
    }

    public String getOptouttext() {
        return optouttext;
    }

    public void setOptouttext(String optouttext) {
        this.optouttext = optouttext;
    }

    public String getOptoutlink() {
        return optoutlink;
    }

    public void setOptoutlink(String optoutlink) {
        this.optoutlink = optoutlink;
    }

    public String getPlaceholdertext() {
        return placeholdertext;
    }

    public void setPlaceholdertext(String placeholdertext) {
        this.placeholdertext = placeholdertext;
    }

    public String getPlaceholderlink() {
        return placeholderlink;
    }

    public void setPlaceholderlink(String placeholderlink) {
        this.placeholderlink = placeholderlink;
    }

    public String getCam() {
        return cam;
    }

    public void setCam(String cam) {
        this.cam = cam;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public Set<Interstitial> getInterstitials() {
        return interstitials;
    }

    public void setInterstitials(Set<Interstitial> interstitials) {
        this.interstitials = interstitials;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Adunit adunit = (Adunit) o;

        if (size != null ? !size.equals(adunit.size) : adunit.size != null) return false;
        if (plc != null ? !plc.equals(adunit.plc) : adunit.plc != null) return false;
        if (iplc != null ? !iplc.equals(adunit.iplc) : adunit.iplc != null) return false;
        if (ox != null ? !ox.equals(adunit.ox) : adunit.ox != null) return false;
        if (oy != null ? !oy.equals(adunit.oy) : adunit.oy != null) return false;
        if (bg != null ? !bg.equals(adunit.bg) : adunit.bg != null) return false;
        if (op != null ? !op.equals(adunit.op) : adunit.op != null) return false;
        if (zi != null ? !zi.equals(adunit.zi) : adunit.zi != null) return false;
        if (icon != null ? !icon.equals(adunit.icon) : adunit.icon != null) return false;
        if (iconcam != null ? !iconcam.equals(adunit.iconcam) : adunit.iconcam != null) return false;
        if (icontext != null ? !icontext.equals(adunit.icontext) : adunit.icontext != null) return false;
        if (optouttext != null ? !optouttext.equals(adunit.optouttext) : adunit.optouttext != null) return false;
        if (optoutlink != null ? !optoutlink.equals(adunit.optoutlink) : adunit.optoutlink != null) return false;
        if (placeholdertext != null ? !placeholdertext.equals(adunit.placeholdertext) : adunit.placeholdertext != null)
            return false;
        if (placeholderlink != null ? !placeholderlink.equals(adunit.placeholderlink) : adunit.placeholderlink != null)
            return false;
        if (cam != null ? !cam.equals(adunit.cam) : adunit.cam != null) return false;
        if (modified != null ? !modified.equals(adunit.modified) : adunit.modified != null) return false;
        if (level != null ? !level.equals(adunit.level) : adunit.level != null) return false;
        return eid != null ? eid.equals(adunit.eid) : adunit.eid == null;
    }

    @Override
    public int hashCode() {
        int result = size != null ? size.hashCode() : 0;
        result = 31 * result + (plc != null ? plc.hashCode() : 0);
        result = 31 * result + (iplc != null ? iplc.hashCode() : 0);
        result = 31 * result + (ox != null ? ox.hashCode() : 0);
        result = 31 * result + (oy != null ? oy.hashCode() : 0);
        result = 31 * result + (bg != null ? bg.hashCode() : 0);
        result = 31 * result + (op != null ? op.hashCode() : 0);
        result = 31 * result + (zi != null ? zi.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (iconcam != null ? iconcam.hashCode() : 0);
        result = 31 * result + (icontext != null ? icontext.hashCode() : 0);
        result = 31 * result + (optouttext != null ? optouttext.hashCode() : 0);
        result = 31 * result + (optoutlink != null ? optoutlink.hashCode() : 0);
        result = 31 * result + (placeholdertext != null ? placeholdertext.hashCode() : 0);
        result = 31 * result + (placeholderlink != null ? placeholderlink.hashCode() : 0);
        result = 31 * result + (cam != null ? cam.hashCode() : 0);
        result = 31 * result + (modified != null ? modified.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        result = 31 * result + (eid != null ? eid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Adunit{" +
                "size='" + size + '\'' +
                ", plc='" + plc + '\'' +
                ", iplc='" + iplc + '\'' +
                ", ox='" + ox + '\'' +
                ", oy='" + oy + '\'' +
                ", bg='" + bg + '\'' +
                ", op='" + op + '\'' +
                ", zi='" + zi + '\'' +
                ", icon='" + icon + '\'' +
                ", iconcam='" + iconcam + '\'' +
                ", icontext='" + icontext + '\'' +
                ", optouttext='" + optouttext + '\'' +
                ", optoutlink='" + optoutlink + '\'' +
                ", placeholdertext='" + placeholdertext + '\'' +
                ", placeholderlink='" + placeholderlink + '\'' +
                ", cam='" + cam + '\'' +
                ", modified=" + modified +
                ", level='" + level + '\'' +
                ", eid='" + eid + '\'' +
                '}';
    }
}
