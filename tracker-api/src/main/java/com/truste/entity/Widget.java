package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "widget", schema = "portal", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Widget implements Serializable {
    @Id
    private String widget_id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String search_key;

    @Column
    private Boolean is_active;

    @Column
    private String icon;

    @Column
    private String url;

    @Column
    private Boolean is_iframe;

    @Column
    private Integer priority;

    @Column
    private Boolean new_window;

    @OneToMany(mappedBy = "widget")
    private Set<RoleWidget> roleWidgets = new HashSet<RoleWidget>();

    @OneToMany(mappedBy = "widget")
    private Set<UserWidget> userWidgets = new HashSet<UserWidget>();

    public Widget() {
    }

    public Widget(String widget_id, String name, String description, String search_key, Boolean is_active, String
            icon, String url, Boolean is_iframe, Integer priority, Boolean new_window) {
        this.widget_id = widget_id;
        this.name = name;
        this.description = description;
        this.search_key = search_key;
        this.is_active = is_active;
        this.icon = icon;
        this.url = url;
        this.is_iframe = is_iframe;
        this.priority = priority;
        this.new_window = new_window;
    }

    public String getWidget_id() {
        return widget_id;
    }

    public void setWidget_id(String widget_id) {
        this.widget_id = widget_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSearch_key() {
        return search_key;
    }

    public void setSearch_key(String search_key) {
        this.search_key = search_key;
    }

    public Boolean getIs_active() {
        return is_active;
    }

    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getIs_iframe() {
        return is_iframe;
    }

    public void setIs_iframe(Boolean is_iframe) {
        this.is_iframe = is_iframe;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getNew_window() {
        return new_window;
    }

    public void setNew_window(Boolean new_window) {
        this.new_window = new_window;
    }

    public Set<RoleWidget> getRoleWidgets() {
        return roleWidgets;
    }

    public void setRoleWidgets(Set<RoleWidget> roleWidgets) {
        this.roleWidgets = roleWidgets;
    }

    public Set<UserWidget> getUserWidgets() {
        return userWidgets;
    }

    public void setUserWidgets(Set<UserWidget> userWidgets) {
        this.userWidgets = userWidgets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Widget widget = (Widget) o;

        if (widget_id != null ? !widget_id.equals(widget.widget_id) : widget.widget_id != null) return false;
        if (name != null ? !name.equals(widget.name) : widget.name != null) return false;
        if (description != null ? !description.equals(widget.description) : widget.description != null) return false;
        if (search_key != null ? !search_key.equals(widget.search_key) : widget.search_key != null) return false;
        if (is_active != null ? !is_active.equals(widget.is_active) : widget.is_active != null) return false;
        if (icon != null ? !icon.equals(widget.icon) : widget.icon != null) return false;
        if (url != null ? !url.equals(widget.url) : widget.url != null) return false;
        if (is_iframe != null ? !is_iframe.equals(widget.is_iframe) : widget.is_iframe != null) return false;
        if (priority != null ? !priority.equals(widget.priority) : widget.priority != null) return false;
        return new_window != null ? new_window.equals(widget.new_window) : widget.new_window == null;
    }

    @Override
    public int hashCode() {
        int result = widget_id != null ? widget_id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (search_key != null ? search_key.hashCode() : 0);
        result = 31 * result + (is_active != null ? is_active.hashCode() : 0);
        result = 31 * result + (icon != null ? icon.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (is_iframe != null ? is_iframe.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (new_window != null ? new_window.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Widget{" +
                "widget_id='" + widget_id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", search_key='" + search_key + '\'' +
                ", is_active=" + is_active +
                ", icon='" + icon + '\'' +
                ", url='" + url + '\'' +
                ", is_iframe=" + is_iframe +
                ", priority=" + priority +
                ", new_window=" + new_window +
                '}';
    }
}
