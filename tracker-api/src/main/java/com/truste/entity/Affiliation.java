package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "affiliation", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Affiliation implements Serializable {

    @Id
    private String affiliation_id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String logo;

    @Column
    private String type;

    @Column
    private String geo_region;

    @OneToMany(mappedBy = "affiliation")
    private Set<OrganizationAffiliation> organizationAffiliations = new HashSet<OrganizationAffiliation>();

    public Affiliation() {
    }

    public Affiliation(String affiliation_id, String name, String description, String logo, String type, String
            geo_region) {
        this.affiliation_id = affiliation_id;
        this.name = name;
        this.description = description;
        this.logo = logo;
        this.type = type;
        this.geo_region = geo_region;
    }

    public String getAffiliation_id() {
        return affiliation_id;
    }

    public void setAffiliation_id(String affiliation_id) {
        this.affiliation_id = affiliation_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGeo_region() {
        return geo_region;
    }

    public void setGeo_region(String geo_region) {
        this.geo_region = geo_region;
    }

    public Set<OrganizationAffiliation> getOrganizationAffiliations() {
        return organizationAffiliations;
    }

    public void setOrganizationAffiliations(Set<OrganizationAffiliation> organizationAffiliations) {
        this.organizationAffiliations = organizationAffiliations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Affiliation that = (Affiliation) o;

        if (affiliation_id != null ? !affiliation_id.equals(that.affiliation_id) : that.affiliation_id != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (logo != null ? !logo.equals(that.logo) : that.logo != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        return geo_region != null ? geo_region.equals(that.geo_region) : that.geo_region == null;
    }

    @Override
    public int hashCode() {
        int result = affiliation_id != null ? affiliation_id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (geo_region != null ? geo_region.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Affiliation{" +
                "affiliation_id='" + affiliation_id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", logo='" + logo + '\'' +
                ", type='" + type + '\'' +
                ", geo_region='" + geo_region + '\'' +
                '}';
    }
}
