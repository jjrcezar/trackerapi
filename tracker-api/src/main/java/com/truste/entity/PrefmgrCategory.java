package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "prefmgr_category", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PrefmgrCategory implements Serializable {
    @Id
    private String category_id;

    @Column
    private String display_text;

    @Column
    private String description;

    @Column
    private Integer priority;

    public PrefmgrCategory() {
    }

    public PrefmgrCategory(String category_id, String display_text, String description, Integer priority) {
        this.category_id = category_id;
        this.display_text = display_text;
        this.description = description;
        this.priority = priority;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDisplay_text() {
        return display_text;
    }

    public void setDisplay_text(String display_text) {
        this.display_text = display_text;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrefmgrCategory that = (PrefmgrCategory) o;

        if (category_id != null ? !category_id.equals(that.category_id) : that.category_id != null) return false;
        if (display_text != null ? !display_text.equals(that.display_text) : that.display_text != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return priority != null ? priority.equals(that.priority) : that.priority == null;
    }

    @Override
    public int hashCode() {
        int result = category_id != null ? category_id.hashCode() : 0;
        result = 31 * result + (display_text != null ? display_text.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PrefmgrCategory{" +
                "category_id='" + category_id + '\'' +
                ", display_text='" + display_text + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                '}';
    }
}
