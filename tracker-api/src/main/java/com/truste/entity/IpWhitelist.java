package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "ip_whitelist", schema = "api")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class IpWhitelist implements Serializable {

    private static final long serialVersionUID = -5180329551305646824L;

    @Id
    private String api_key_id;

    @Id
    private String ip;

    @Column
    private Boolean isactive;

    @ManyToOne
    @JoinColumn(name = "api_key_id", insertable = false, updatable = false)
    private ApiKey apiKey;

    public IpWhitelist() {
    }

    public IpWhitelist(String api_key_id, String ip, Boolean isactive) {
        this.api_key_id = api_key_id;
        this.ip = ip;
        this.isactive = isactive;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getApi_key_id() {
        return api_key_id;
    }

    public void setApi_key_id(String api_key_id) {
        this.api_key_id = api_key_id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public ApiKey getApiKey() {
        return apiKey;
    }

    public void setApiKey(ApiKey apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IpWhitelist that = (IpWhitelist) o;

        if (api_key_id != null ? !api_key_id.equals(that.api_key_id) : that.api_key_id != null) return false;
        if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
        return isactive != null ? isactive.equals(that.isactive) : that.isactive == null;
    }

    @Override
    public int hashCode() {
        int result = api_key_id != null ? api_key_id.hashCode() : 0;
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (isactive != null ? isactive.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "IpWhitelist{" +
                "api_key_id='" + api_key_id + '\'' +
                ", ip='" + ip + '\'' +
                ", isactive=" + isactive +
                '}';
    }
}