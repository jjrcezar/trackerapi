package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "domain_seenas", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = "placeholder_domain")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainSeenas implements Serializable {
    @Id
    private String source_domain_id;

    @Id
    private String placeholder_domain;

    public DomainSeenas() {
    }

    public DomainSeenas(String source_domain_id, String placeholder_domain) {
        this.source_domain_id = source_domain_id;
        this.placeholder_domain = placeholder_domain;
    }

    public String getSource_domain_id() {
        return source_domain_id;
    }

    public void setSource_domain_id(String source_domain_id) {
        this.source_domain_id = source_domain_id;
    }

    public String getPlaceholder_domain() {
        return placeholder_domain;
    }

    public void setPlaceholder_domain(String placeholder_domain) {
        this.placeholder_domain = placeholder_domain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DomainSeenas that = (DomainSeenas) o;

        if (source_domain_id != null ? !source_domain_id.equals(that.source_domain_id) : that.source_domain_id != null)
            return false;
        return placeholder_domain != null ? placeholder_domain.equals(that.placeholder_domain) : that
                .placeholder_domain == null;
    }

    @Override
    public int hashCode() {
        int result = source_domain_id != null ? source_domain_id.hashCode() : 0;
        result = 31 * result + (placeholder_domain != null ? placeholder_domain.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DomainSeenas{" +
                "source_domain_id='" + source_domain_id + '\'' +
                ", placeholder_domain='" + placeholder_domain + '\'' +
                '}';
    }
}
