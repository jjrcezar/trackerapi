package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ta_markup", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TaMarkup implements Serializable {
    @Id
    private String name;

    @Column
    private String data;

    @Column
    private Date modified;

    public TaMarkup() {
    }

    public TaMarkup(String name, String data, Date modified) {
        this.name = name;
        this.data = data;
        this.modified = modified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaMarkup taMarkup = (TaMarkup) o;

        if (name != null ? !name.equals(taMarkup.name) : taMarkup.name != null) return false;
        if (data != null ? !data.equals(taMarkup.data) : taMarkup.data != null) return false;
        return modified != null ? modified.equals(taMarkup.modified) : taMarkup.modified == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (modified != null ? modified.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TaMarkup{" +
                "name='" + name + '\'' +
                ", data='" + data + '\'' +
                ", modified=" + modified +
                '}';
    }
}
