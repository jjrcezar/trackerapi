package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "user_widget", schema = "portal")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserWidget implements Serializable {
    @Id
    private String user_id;

    @Id
    private String group_id;

    @Id
    private String widget_id;

    @ManyToOne
    @JoinColumn(name = "group_id", insertable = false, updatable = false)
    private GroupWidget groupWidget;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    @ManyToOne
    @JoinColumn(name = "widget_id", insertable = false, updatable = false)
    private Widget widget;

    public UserWidget() {
    }

    public UserWidget(String user_id, String group_id, String widget_id) {
        this.user_id = user_id;
        this.group_id = group_id;
        this.widget_id = widget_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getWidget_id() {
        return widget_id;
    }

    public void setWidget_id(String widget_id) {
        this.widget_id = widget_id;
    }

    public GroupWidget getGroupWidget() {
        return groupWidget;
    }

    public void setGroupWidget(GroupWidget groupWidget) {
        this.groupWidget = groupWidget;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Widget getWidget() {
        return widget;
    }

    public void setWidget(Widget widget) {
        this.widget = widget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserWidget that = (UserWidget) o;

        if (user_id != null ? !user_id.equals(that.user_id) : that.user_id != null) return false;
        if (group_id != null ? !group_id.equals(that.group_id) : that.group_id != null) return false;
        return widget_id != null ? widget_id.equals(that.widget_id) : that.widget_id == null;
    }

    @Override
    public int hashCode() {
        int result = user_id != null ? user_id.hashCode() : 0;
        result = 31 * result + (group_id != null ? group_id.hashCode() : 0);
        result = 31 * result + (widget_id != null ? widget_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserWidget{" +
                "user_id='" + user_id + '\'' +
                ", group_id='" + group_id + '\'' +
                ", widget_id='" + widget_id + '\'' +
                '}';
    }
}
