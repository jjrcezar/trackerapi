package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "url", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Url implements Serializable {
    @Id
    private UUID url_id;

    @Column
    private String url;

    public Url() {
    }

    public Url(UUID url_id, String url) {
        this.url_id = url_id;
        this.url = url;
    }

    public UUID getUrl_id() {
        return url_id;
    }

    public void setUrl_id(UUID url_id) {
        this.url_id = url_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Url url1 = (Url) o;

        if (url_id != null ? !url_id.equals(url1.url_id) : url1.url_id != null) return false;
        return url != null ? url.equals(url1.url) : url1.url == null;
    }

    @Override
    public int hashCode() {
        int result = url_id != null ? url_id.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Url{" +
                "url_id=" + url_id +
                ", url='" + url + '\'' +
                '}';
    }
}
