package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "url_tracker", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UrlTracker implements Serializable {
    @Id
    private UUID url_id;

    @Column
    private String tracker_id;


    public UrlTracker() {
    }

    public UrlTracker(UUID url_id, String tracker_id) {
        this.url_id = url_id;
        this.tracker_id = tracker_id;
    }

    public UUID getUrl_id() {
        return url_id;
    }

    public void setUrl_id(UUID url_id) {
        this.url_id = url_id;
    }

    public String getTracker_id() {
        return tracker_id;
    }

    public void setTracker_id(String tracker_id) {
        this.tracker_id = tracker_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UrlTracker that = (UrlTracker) o;

        if (url_id != null ? !url_id.equals(that.url_id) : that.url_id != null) return false;
        return tracker_id != null ? tracker_id.equals(that.tracker_id) : that.tracker_id == null;
    }

    @Override
    public int hashCode() {
        int result = url_id != null ? url_id.hashCode() : 0;
        result = 31 * result + (tracker_id != null ? tracker_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UrlTracker{" +
                "url_id=" + url_id +
                ", tracker_id='" + tracker_id + '\'' +
                '}';
    }
}
