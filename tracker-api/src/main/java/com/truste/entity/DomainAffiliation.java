package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "domain_affiliation", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainAffiliation implements Serializable {
    @Id
    private String domain_id;

    @Id
    private String affiliation_id;

    @ManyToOne
    @JoinColumn(name = "domain_id", insertable = false, updatable = false)
    private Domain domain;

    public DomainAffiliation() {
    }

    public DomainAffiliation(String domain_id, String affiliation_id) {
        this.domain_id = domain_id;
        this.affiliation_id = affiliation_id;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public String getAffiliation_id() {
        return affiliation_id;
    }

    public void setAffiliation_id(String affiliation_id) {
        this.affiliation_id = affiliation_id;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DomainAffiliation that = (DomainAffiliation) o;

        if (domain_id != null ? !domain_id.equals(that.domain_id) : that.domain_id != null) return false;
        return affiliation_id != null ? affiliation_id.equals(that.affiliation_id) : that.affiliation_id == null;
    }

    @Override
    public int hashCode() {
        int result = domain_id != null ? domain_id.hashCode() : 0;
        result = 31 * result + (affiliation_id != null ? affiliation_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DomainAffiliation{" +
                "domain_id='" + domain_id + '\'' +
                ", affiliation_id='" + affiliation_id + '\'' +
                '}';
    }
}
