package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "tplentry", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Tplentry implements Serializable {
    @Id
    private Integer tplentry_id;

    @Column
    private String tpl_name;

    @Column
    private Boolean is_string_rule;

    @Column
    private Boolean is_block_rule;

    @Column
    private String scope_type;

    @Column
    private String scope_type_id;

    @Column
    private String note;

    @ManyToOne
    @JoinColumn(name = "tpl_name", insertable = false, updatable = false)
    private Tplist tplist;

    public Tplentry() {
    }

    public Tplentry(Integer tplentry_id, String tpl_name, Boolean is_string_rule, Boolean is_block_rule, String
            scope_type, String scope_type_id, String note) {
        this.tplentry_id = tplentry_id;
        this.tpl_name = tpl_name;
        this.is_string_rule = is_string_rule;
        this.is_block_rule = is_block_rule;
        this.scope_type = scope_type;
        this.scope_type_id = scope_type_id;
        this.note = note;
    }

    public Integer getTplentry_id() {
        return tplentry_id;
    }

    public void setTplentry_id(Integer tplentry_id) {
        this.tplentry_id = tplentry_id;
    }

    public String getTpl_name() {
        return tpl_name;
    }

    public void setTpl_name(String tpl_name) {
        this.tpl_name = tpl_name;
    }

    public Boolean getIs_string_rule() {
        return is_string_rule;
    }

    public void setIs_string_rule(Boolean is_string_rule) {
        this.is_string_rule = is_string_rule;
    }

    public Boolean getIs_block_rule() {
        return is_block_rule;
    }

    public void setIs_block_rule(Boolean is_block_rule) {
        this.is_block_rule = is_block_rule;
    }

    public String getScope_type() {
        return scope_type;
    }

    public void setScope_type(String scope_type) {
        this.scope_type = scope_type;
    }

    public String getScope_type_id() {
        return scope_type_id;
    }

    public void setScope_type_id(String scope_type_id) {
        this.scope_type_id = scope_type_id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Tplist getTplist() {
        return tplist;
    }

    public void setTplist(Tplist tplist) {
        this.tplist = tplist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tplentry tplentry = (Tplentry) o;

        if (tplentry_id != null ? !tplentry_id.equals(tplentry.tplentry_id) : tplentry.tplentry_id != null)
            return false;
        if (tpl_name != null ? !tpl_name.equals(tplentry.tpl_name) : tplentry.tpl_name != null) return false;
        if (is_string_rule != null ? !is_string_rule.equals(tplentry.is_string_rule) : tplentry.is_string_rule != null)
            return false;
        if (is_block_rule != null ? !is_block_rule.equals(tplentry.is_block_rule) : tplentry.is_block_rule != null)
            return false;
        if (scope_type != null ? !scope_type.equals(tplentry.scope_type) : tplentry.scope_type != null) return false;
        if (scope_type_id != null ? !scope_type_id.equals(tplentry.scope_type_id) : tplentry.scope_type_id != null)
            return false;
        return note != null ? note.equals(tplentry.note) : tplentry.note == null;
    }

    @Override
    public int hashCode() {
        int result = tplentry_id != null ? tplentry_id.hashCode() : 0;
        result = 31 * result + (tpl_name != null ? tpl_name.hashCode() : 0);
        result = 31 * result + (is_string_rule != null ? is_string_rule.hashCode() : 0);
        result = 31 * result + (is_block_rule != null ? is_block_rule.hashCode() : 0);
        result = 31 * result + (scope_type != null ? scope_type.hashCode() : 0);
        result = 31 * result + (scope_type_id != null ? scope_type_id.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tplentry{" +
                "tplentry_id=" + tplentry_id +
                ", tpl_name='" + tpl_name + '\'' +
                ", is_string_rule=" + is_string_rule +
                ", is_block_rule=" + is_block_rule +
                ", scope_type='" + scope_type + '\'' +
                ", scope_type_id='" + scope_type_id + '\'' +
                ", note='" + note + '\'' +
                '}';
    }
}
