package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "log_notice", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LogNotice implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date start_time;

    @Column
    private Date end_time;

    @Column
    private String domain;

    @Column
    private String locale;

    @Column
    private String language;

    @Column
    private Long impressions;

    @Column
    private Long clicks;

    @Column
    private Long consents;

    @Column
    private Long returns;

    public LogNotice() {
    }

    public LogNotice(Date start_time, Date end_time, String domain, String locale, String language, Long impressions,
            Long clicks, Long consents, Long returns) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.domain = domain;
        this.locale = locale;
        this.language = language;
        this.impressions = impressions;
        this.clicks = clicks;
        this.consents = consents;
        this.returns = returns;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Long getImpressions() {
        return impressions;
    }

    public void setImpressions(Long impressions) {
        this.impressions = impressions;
    }

    public Long getClicks() {
        return clicks;
    }

    public void setClicks(Long clicks) {
        this.clicks = clicks;
    }

    public Long getConsents() {
        return consents;
    }

    public void setConsents(Long consents) {
        this.consents = consents;
    }

    public Long getReturns() {
        return returns;
    }

    public void setReturns(Long returns) {
        this.returns = returns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogNotice logNotice = (LogNotice) o;

        if (start_time != null ? !start_time.equals(logNotice.start_time) : logNotice.start_time != null) return false;
        if (end_time != null ? !end_time.equals(logNotice.end_time) : logNotice.end_time != null) return false;
        if (domain != null ? !domain.equals(logNotice.domain) : logNotice.domain != null) return false;
        if (locale != null ? !locale.equals(logNotice.locale) : logNotice.locale != null) return false;
        if (language != null ? !language.equals(logNotice.language) : logNotice.language != null) return false;
        if (impressions != null ? !impressions.equals(logNotice.impressions) : logNotice.impressions != null)
            return false;
        if (clicks != null ? !clicks.equals(logNotice.clicks) : logNotice.clicks != null) return false;
        if (consents != null ? !consents.equals(logNotice.consents) : logNotice.consents != null) return false;
        return returns != null ? returns.equals(logNotice.returns) : logNotice.returns == null;
    }

    @Override
    public int hashCode() {
        int result = start_time != null ? start_time.hashCode() : 0;
        result = 31 * result + (end_time != null ? end_time.hashCode() : 0);
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (locale != null ? locale.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (impressions != null ? impressions.hashCode() : 0);
        result = 31 * result + (clicks != null ? clicks.hashCode() : 0);
        result = 31 * result + (consents != null ? consents.hashCode() : 0);
        result = 31 * result + (returns != null ? returns.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LogNotice{" +
                "start_time=" + start_time +
                ", end_time=" + end_time +
                ", domain='" + domain + '\'' +
                ", locale='" + locale + '\'' +
                ", language='" + language + '\'' +
                ", impressions=" + impressions +
                ", clicks=" + clicks +
                ", consents=" + consents +
                ", returns=" + returns +
                '}';
    }
}
