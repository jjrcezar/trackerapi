package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "recon_iframe", schema = "recon", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"page_url", "loader_url", "loaded_url", "domain", "name"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconIframe implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String page_url;

    @Column
    private String collectedby;

    @Column
    private String loader_url;

    @Column
    private String loaded_url;

    @Column
    private Date collected;

    @Column
    private Boolean evaluated;

    @Column
    private Date created;

    @Column
    private String note;

    @Column
    private String name;

    @Column
    private String value;

    @Column
    private String domain;

    @Column
    private Date updated;

    @Column
    private String use;

    public ReconIframe() {
    }

    public ReconIframe(Long id, String page_url, String collectedby, String loader_url, String loaded_url, Date
            collected, Boolean evaluated, Date created, String note, String name, String value, String domain, Date
            updated, String use) {
        this.id = id;
        this.page_url = page_url;
        this.collectedby = collectedby;
        this.loader_url = loader_url;
        this.loaded_url = loaded_url;
        this.collected = collected;
        this.evaluated = evaluated;
        this.created = created;
        this.note = note;
        this.name = name;
        this.value = value;
        this.domain = domain;
        this.updated = updated;
        this.use = use;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPage_url() {
        return page_url;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public String getCollectedby() {
        return collectedby;
    }

    public void setCollectedby(String collectedby) {
        this.collectedby = collectedby;
    }

    public String getLoader_url() {
        return loader_url;
    }

    public void setLoader_url(String loader_url) {
        this.loader_url = loader_url;
    }

    public String getLoaded_url() {
        return loaded_url;
    }

    public void setLoaded_url(String loaded_url) {
        this.loaded_url = loaded_url;
    }

    public Date getCollected() {
        return collected;
    }

    public void setCollected(Date collected) {
        this.collected = collected;
    }

    public Boolean getEvaluated() {
        return evaluated;
    }

    public void setEvaluated(Boolean evaluated) {
        this.evaluated = evaluated;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconIframe that = (ReconIframe) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (page_url != null ? !page_url.equals(that.page_url) : that.page_url != null) return false;
        if (collectedby != null ? !collectedby.equals(that.collectedby) : that.collectedby != null) return false;
        if (loader_url != null ? !loader_url.equals(that.loader_url) : that.loader_url != null) return false;
        if (loaded_url != null ? !loaded_url.equals(that.loaded_url) : that.loaded_url != null) return false;
        if (collected != null ? !collected.equals(that.collected) : that.collected != null) return false;
        if (evaluated != null ? !evaluated.equals(that.evaluated) : that.evaluated != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (note != null ? !note.equals(that.note) : that.note != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;
        return use != null ? use.equals(that.use) : that.use == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (page_url != null ? page_url.hashCode() : 0);
        result = 31 * result + (collectedby != null ? collectedby.hashCode() : 0);
        result = 31 * result + (loader_url != null ? loader_url.hashCode() : 0);
        result = 31 * result + (loaded_url != null ? loaded_url.hashCode() : 0);
        result = 31 * result + (collected != null ? collected.hashCode() : 0);
        result = 31 * result + (evaluated != null ? evaluated.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (use != null ? use.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconIframe{" +
                "id=" + id +
                ", page_url='" + page_url + '\'' +
                ", collectedby='" + collectedby + '\'' +
                ", loader_url='" + loader_url + '\'' +
                ", loaded_url='" + loaded_url + '\'' +
                ", collected=" + collected +
                ", evaluated=" + evaluated +
                ", created=" + created +
                ", note='" + note + '\'' +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", domain='" + domain + '\'' +
                ", updated=" + updated +
                ", use='" + use + '\'' +
                '}';
    }
}
