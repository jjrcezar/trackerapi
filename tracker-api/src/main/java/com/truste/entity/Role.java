package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "role", schema = "portal", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Role implements Serializable {
    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String coverage;

    @Column
    private String search_key;

    @Column
    private String parent_role_id;

    @Id
    private String role_id;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private Role parentRole;

    @OneToMany(mappedBy = "parentRole")
    private Set<Role> childRoles = new HashSet<Role>();

    @OneToMany(mappedBy = "role")
    private Set<RoleWidget> roleWidgets = new HashSet<RoleWidget>();

    @OneToMany(mappedBy = "role")
    private Set<UserRole> userRoles = new HashSet<UserRole>();

    public Role() {
    }

    public Role(String name, String description, String coverage, String search_key, String parent_role_id, String
            role_id) {
        this.name = name;
        this.description = description;
        this.coverage = coverage;
        this.search_key = search_key;
        this.parent_role_id = parent_role_id;
        this.role_id = role_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCoverage() {
        return coverage;
    }

    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }

    public String getSearch_key() {
        return search_key;
    }

    public void setSearch_key(String search_key) {
        this.search_key = search_key;
    }

    public String getParent_role_id() {
        return parent_role_id;
    }

    public void setParent_role_id(String parent_role_id) {
        this.parent_role_id = parent_role_id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public Role getParentRole() {
        return parentRole;
    }

    public void setParentRole(Role parentRole) {
        this.parentRole = parentRole;
    }

    public Set<Role> getChildRoles() {
        return childRoles;
    }

    public void setChildRoles(Set<Role> childRoles) {
        this.childRoles = childRoles;
    }

    public Set<RoleWidget> getRoleWidgets() {
        return roleWidgets;
    }

    public void setRoleWidgets(Set<RoleWidget> roleWidgets) {
        this.roleWidgets = roleWidgets;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (name != null ? !name.equals(role.name) : role.name != null) return false;
        if (description != null ? !description.equals(role.description) : role.description != null) return false;
        if (coverage != null ? !coverage.equals(role.coverage) : role.coverage != null) return false;
        if (search_key != null ? !search_key.equals(role.search_key) : role.search_key != null) return false;
        if (parent_role_id != null ? !parent_role_id.equals(role.parent_role_id) : role.parent_role_id != null)
            return false;
        return role_id != null ? role_id.equals(role.role_id) : role.role_id == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (coverage != null ? coverage.hashCode() : 0);
        result = 31 * result + (search_key != null ? search_key.hashCode() : 0);
        result = 31 * result + (parent_role_id != null ? parent_role_id.hashCode() : 0);
        result = 31 * result + (role_id != null ? role_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", coverage='" + coverage + '\'' +
                ", search_key='" + search_key + '\'' +
                ", parent_role_id='" + parent_role_id + '\'' +
                ", role_id='" + role_id + '\'' +
                '}';
    }
}
