package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "log_admarker_audit", schema = "stat")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LogAdmarkerAudit implements Serializable {
    @Id
    private Long log_admarker_audit_id;

    @Column
    private Date coverage_start;

    @Column
    private Date coverage_end;

    @Column
    private Boolean isconsistent;

    @Column
    private String periodic_interval;

    @Column
    private Integer failed;

    @Column
    private Integer passed;

    @OneToMany(mappedBy = "logAdmarkerAudit")
    private Set<LogAdmarkerAuditDetails> logAdmarkerAuditDetails = new HashSet<LogAdmarkerAuditDetails>();

    public LogAdmarkerAudit() {
    }

    public LogAdmarkerAudit(Long log_admarker_audit_id, Date coverage_start, Date coverage_end, Boolean isconsistent,
            String periodic_interval, Integer failed, Integer passed) {
        this.log_admarker_audit_id = log_admarker_audit_id;
        this.coverage_start = coverage_start;
        this.coverage_end = coverage_end;
        this.isconsistent = isconsistent;
        this.periodic_interval = periodic_interval;
        this.failed = failed;
        this.passed = passed;
    }

    public Long getLog_admarker_audit_id() {
        return log_admarker_audit_id;
    }

    public void setLog_admarker_audit_id(Long log_admarker_audit_id) {
        this.log_admarker_audit_id = log_admarker_audit_id;
    }

    public Date getCoverage_start() {
        return coverage_start;
    }

    public void setCoverage_start(Date coverage_start) {
        this.coverage_start = coverage_start;
    }

    public Date getCoverage_end() {
        return coverage_end;
    }

    public void setCoverage_end(Date coverage_end) {
        this.coverage_end = coverage_end;
    }

    public Boolean getIsconsistent() {
        return isconsistent;
    }

    public void setIsconsistent(Boolean isconsistent) {
        this.isconsistent = isconsistent;
    }

    public String getPeriodic_interval() {
        return periodic_interval;
    }

    public void setPeriodic_interval(String periodic_interval) {
        this.periodic_interval = periodic_interval;
    }

    public Integer getFailed() {
        return failed;
    }

    public void setFailed(Integer failed) {
        this.failed = failed;
    }

    public Integer getPassed() {
        return passed;
    }

    public void setPassed(Integer passed) {
        this.passed = passed;
    }

    public Set<LogAdmarkerAuditDetails> getLogAdmarkerAuditDetails() {
        return logAdmarkerAuditDetails;
    }

    public void setLogAdmarkerAuditDetails(Set<LogAdmarkerAuditDetails> logAdmarkerAuditDetails) {
        this.logAdmarkerAuditDetails = logAdmarkerAuditDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogAdmarkerAudit that = (LogAdmarkerAudit) o;

        if (log_admarker_audit_id != null ? !log_admarker_audit_id.equals(that.log_admarker_audit_id) : that
                .log_admarker_audit_id != null)
            return false;
        if (coverage_start != null ? !coverage_start.equals(that.coverage_start) : that.coverage_start != null)
            return false;
        if (coverage_end != null ? !coverage_end.equals(that.coverage_end) : that.coverage_end != null) return false;
        if (isconsistent != null ? !isconsistent.equals(that.isconsistent) : that.isconsistent != null) return false;
        if (periodic_interval != null ? !periodic_interval.equals(that.periodic_interval) : that.periodic_interval !=
                null)
            return false;
        if (failed != null ? !failed.equals(that.failed) : that.failed != null) return false;
        return passed != null ? passed.equals(that.passed) : that.passed == null;
    }

    @Override
    public int hashCode() {
        int result = log_admarker_audit_id != null ? log_admarker_audit_id.hashCode() : 0;
        result = 31 * result + (coverage_start != null ? coverage_start.hashCode() : 0);
        result = 31 * result + (coverage_end != null ? coverage_end.hashCode() : 0);
        result = 31 * result + (isconsistent != null ? isconsistent.hashCode() : 0);
        result = 31 * result + (periodic_interval != null ? periodic_interval.hashCode() : 0);
        result = 31 * result + (failed != null ? failed.hashCode() : 0);
        result = 31 * result + (passed != null ? passed.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LogAdmarkerAudit{" +
                "log_admarker_audit_id=" + log_admarker_audit_id +
                ", coverage_start=" + coverage_start +
                ", coverage_end=" + coverage_end +
                ", isconsistent=" + isconsistent +
                ", periodic_interval='" + periodic_interval + '\'' +
                ", failed=" + failed +
                ", passed=" + passed +
                '}';
    }
}
