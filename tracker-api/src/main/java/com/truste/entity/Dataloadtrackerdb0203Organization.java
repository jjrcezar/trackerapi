package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "dataloadtrackerdb0203_organization", schema = "import")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Dataloadtrackerdb0203Organization implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private String parent;

    @Column
    private String aka;

    @Column
    private String category;

    @Column
    private String description;

    @Column
    private String website;

    @Column
    private String website_privacy_policy;

    @Column
    private String obarisk;

    @Column
    private String usesflashcookies;

    public Dataloadtrackerdb0203Organization() {
    }

    public Dataloadtrackerdb0203Organization(String name, String parent, String aka, String category, String
            description, String website, String website_privacy_policy, String obarisk, String usesflashcookies) {
        this.name = name;
        this.parent = parent;
        this.aka = aka;
        this.category = category;
        this.description = description;
        this.website = website;
        this.website_privacy_policy = website_privacy_policy;
        this.obarisk = obarisk;
        this.usesflashcookies = usesflashcookies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getAka() {
        return aka;
    }

    public void setAka(String aka) {
        this.aka = aka;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWebsite_privacy_policy() {
        return website_privacy_policy;
    }

    public void setWebsite_privacy_policy(String website_privacy_policy) {
        this.website_privacy_policy = website_privacy_policy;
    }

    public String getObarisk() {
        return obarisk;
    }

    public void setObarisk(String obarisk) {
        this.obarisk = obarisk;
    }

    public String getUsesflashcookies() {
        return usesflashcookies;
    }

    public void setUsesflashcookies(String usesflashcookies) {
        this.usesflashcookies = usesflashcookies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dataloadtrackerdb0203Organization that = (Dataloadtrackerdb0203Organization) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (parent != null ? !parent.equals(that.parent) : that.parent != null) return false;
        if (aka != null ? !aka.equals(that.aka) : that.aka != null) return false;
        if (category != null ? !category.equals(that.category) : that.category != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (website != null ? !website.equals(that.website) : that.website != null) return false;
        if (website_privacy_policy != null ? !website_privacy_policy.equals(that.website_privacy_policy) : that
                .website_privacy_policy != null)
            return false;
        if (obarisk != null ? !obarisk.equals(that.obarisk) : that.obarisk != null) return false;
        return usesflashcookies != null ? usesflashcookies.equals(that.usesflashcookies) : that.usesflashcookies ==
                null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (parent != null ? parent.hashCode() : 0);
        result = 31 * result + (aka != null ? aka.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (website_privacy_policy != null ? website_privacy_policy.hashCode() : 0);
        result = 31 * result + (obarisk != null ? obarisk.hashCode() : 0);
        result = 31 * result + (usesflashcookies != null ? usesflashcookies.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Dataloadtrackerdb0203Organization{" +
                "name='" + name + '\'' +
                ", parent='" + parent + '\'' +
                ", aka='" + aka + '\'' +
                ", category='" + category + '\'' +
                ", description='" + description + '\'' +
                ", website='" + website + '\'' +
                ", website_privacy_policy='" + website_privacy_policy + '\'' +
                ", obarisk='" + obarisk + '\'' +
                ", usesflashcookies='" + usesflashcookies + '\'' +
                '}';
    }
}
