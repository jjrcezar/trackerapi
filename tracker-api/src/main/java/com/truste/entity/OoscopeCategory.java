package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "ooscope_category", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OoscopeCategory implements Serializable {
    @Id
    private String category_id;

    @Column
    private Boolean isprimary;

    @Id
    private String ooscope_id;

    @ManyToOne
    @JoinColumn(name = "category_id", insertable = false, updatable = false)
    private Category category;

    @ManyToOne
    @JoinColumn(name = "ooscope_id", insertable = false, updatable = false)
    private OptOutScope optOutScope;

    public OoscopeCategory() {
    }

    public OoscopeCategory(String category_id, Boolean isprimary, String ooscope_id) {
        this.category_id = category_id;
        this.isprimary = isprimary;
        this.ooscope_id = ooscope_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public Boolean getIsprimary() {
        return isprimary;
    }

    public void setIsprimary(Boolean isprimary) {
        this.isprimary = isprimary;
    }

    public String getOoscope_id() {
        return ooscope_id;
    }

    public void setOoscope_id(String ooscope_id) {
        this.ooscope_id = ooscope_id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public OptOutScope getOptOutScope() {
        return optOutScope;
    }

    public void setOptOutScope(OptOutScope optOutScope) {
        this.optOutScope = optOutScope;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OoscopeCategory that = (OoscopeCategory) o;

        if (category_id != null ? !category_id.equals(that.category_id) : that.category_id != null) return false;
        if (isprimary != null ? !isprimary.equals(that.isprimary) : that.isprimary != null) return false;
        return ooscope_id != null ? ooscope_id.equals(that.ooscope_id) : that.ooscope_id == null;
    }

    @Override
    public int hashCode() {
        int result = category_id != null ? category_id.hashCode() : 0;
        result = 31 * result + (isprimary != null ? isprimary.hashCode() : 0);
        result = 31 * result + (ooscope_id != null ? ooscope_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OoscopeCategory{" +
                "category_id='" + category_id + '\'' +
                ", isprimary=" + isprimary +
                ", ooscope_id='" + ooscope_id + '\'' +
                '}';
    }
}
