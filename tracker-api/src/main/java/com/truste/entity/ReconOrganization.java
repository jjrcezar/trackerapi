package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "recon_organization", schema = "recon")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconOrganization implements Serializable {
    @Id
    private Long id;

    @Column
    private String name;

    @Column
    private String displayname;

    @Column
    private String description;

    @Column
    private String website;

    @Column
    private String website_privacy_policy;

    @Column
    private String services_privacy_policy;

    @Column
    private String affiliations;

    @Column
    private String domains;

    @Column
    private String primary_category;

    @Column
    private String categories;

    @Column
    private String domain_categories;

    @Column
    private String information;

    @Column
    private String collectedby;

    @Column
    private Date created;

    @Column
    private Date updated;

    @Column
    private String updatedby;

    @Column
    private String notes;

    @Column
    private String preferences_url;

    @Column
    private String opt_in_url;

    @Column
    private Boolean evaluated;

    public ReconOrganization() {

    }

    public ReconOrganization(Long id, String name, String displayname, String description, String website, String
            website_privacy_policy, String services_privacy_policy, String affiliations, String domains, String
            primary_category, String categories, String domain_categories, String information, String collectedby,
            Date created, Date updated, String updatedby, String notes, String preferences_url, String opt_in_url,
            Boolean evaluated) {
        this.id = id;
        this.name = name;
        this.displayname = displayname;
        this.description = description;
        this.website = website;
        this.website_privacy_policy = website_privacy_policy;
        this.services_privacy_policy = services_privacy_policy;
        this.affiliations = affiliations;
        this.domains = domains;
        this.primary_category = primary_category;
        this.categories = categories;
        this.domain_categories = domain_categories;
        this.information = information;
        this.collectedby = collectedby;
        this.created = created;
        this.updated = updated;
        this.updatedby = updatedby;
        this.notes = notes;
        this.preferences_url = preferences_url;
        this.opt_in_url = opt_in_url;
        this.evaluated = evaluated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWebsite_privacy_policy() {
        return website_privacy_policy;
    }

    public void setWebsite_privacy_policy(String website_privacy_policy) {
        this.website_privacy_policy = website_privacy_policy;
    }

    public String getServices_privacy_policy() {
        return services_privacy_policy;
    }

    public void setServices_privacy_policy(String services_privacy_policy) {
        this.services_privacy_policy = services_privacy_policy;
    }

    public String getAffiliations() {
        return affiliations;
    }

    public void setAffiliations(String affiliations) {
        this.affiliations = affiliations;
    }

    public String getDomains() {
        return domains;
    }

    public void setDomains(String domains) {
        this.domains = domains;
    }

    public String getPrimary_category() {
        return primary_category;
    }

    public void setPrimary_category(String primary_category) {
        this.primary_category = primary_category;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getDomain_categories() {
        return domain_categories;
    }

    public void setDomain_categories(String domain_categories) {
        this.domain_categories = domain_categories;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getCollectedby() {
        return collectedby;
    }

    public void setCollectedby(String collectedby) {
        this.collectedby = collectedby;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPreferences_url() {
        return preferences_url;
    }

    public void setPreferences_url(String preferences_url) {
        this.preferences_url = preferences_url;
    }

    public String getOpt_in_url() {
        return opt_in_url;
    }

    public void setOpt_in_url(String opt_in_url) {
        this.opt_in_url = opt_in_url;
    }

    public Boolean getEvaluated() {
        return evaluated;
    }

    public void setEvaluated(Boolean evaluated) {
        this.evaluated = evaluated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconOrganization that = (ReconOrganization) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (displayname != null ? !displayname.equals(that.displayname) : that.displayname != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (website != null ? !website.equals(that.website) : that.website != null) return false;
        if (website_privacy_policy != null ? !website_privacy_policy.equals(that.website_privacy_policy) : that
                .website_privacy_policy != null)
            return false;
        if (services_privacy_policy != null ? !services_privacy_policy.equals(that.services_privacy_policy) : that
                .services_privacy_policy != null)
            return false;
        if (affiliations != null ? !affiliations.equals(that.affiliations) : that.affiliations != null) return false;
        if (domains != null ? !domains.equals(that.domains) : that.domains != null) return false;
        if (primary_category != null ? !primary_category.equals(that.primary_category) : that.primary_category != null)
            return false;
        if (categories != null ? !categories.equals(that.categories) : that.categories != null) return false;
        if (domain_categories != null ? !domain_categories.equals(that.domain_categories) : that.domain_categories !=
                null)
            return false;
        if (information != null ? !information.equals(that.information) : that.information != null) return false;
        if (collectedby != null ? !collectedby.equals(that.collectedby) : that.collectedby != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;
        if (updatedby != null ? !updatedby.equals(that.updatedby) : that.updatedby != null) return false;
        if (notes != null ? !notes.equals(that.notes) : that.notes != null) return false;
        if (preferences_url != null ? !preferences_url.equals(that.preferences_url) : that.preferences_url != null)
            return false;
        if (opt_in_url != null ? !opt_in_url.equals(that.opt_in_url) : that.opt_in_url != null) return false;
        return evaluated != null ? evaluated.equals(that.evaluated) : that.evaluated == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (displayname != null ? displayname.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (website != null ? website.hashCode() : 0);
        result = 31 * result + (website_privacy_policy != null ? website_privacy_policy.hashCode() : 0);
        result = 31 * result + (services_privacy_policy != null ? services_privacy_policy.hashCode() : 0);
        result = 31 * result + (affiliations != null ? affiliations.hashCode() : 0);
        result = 31 * result + (domains != null ? domains.hashCode() : 0);
        result = 31 * result + (primary_category != null ? primary_category.hashCode() : 0);
        result = 31 * result + (categories != null ? categories.hashCode() : 0);
        result = 31 * result + (domain_categories != null ? domain_categories.hashCode() : 0);
        result = 31 * result + (information != null ? information.hashCode() : 0);
        result = 31 * result + (collectedby != null ? collectedby.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (updatedby != null ? updatedby.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (preferences_url != null ? preferences_url.hashCode() : 0);
        result = 31 * result + (opt_in_url != null ? opt_in_url.hashCode() : 0);
        result = 31 * result + (evaluated != null ? evaluated.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconOrganization{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", displayname='" + displayname + '\'' +
                ", description='" + description + '\'' +
                ", website='" + website + '\'' +
                ", website_privacy_policy='" + website_privacy_policy + '\'' +
                ", services_privacy_policy='" + services_privacy_policy + '\'' +
                ", affiliations='" + affiliations + '\'' +
                ", domains='" + domains + '\'' +
                ", primary_category='" + primary_category + '\'' +
                ", categories='" + categories + '\'' +
                ", domain_categories='" + domain_categories + '\'' +
                ", information='" + information + '\'' +
                ", collectedby='" + collectedby + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", updatedby='" + updatedby + '\'' +
                ", notes='" + notes + '\'' +
                ", preferences_url='" + preferences_url + '\'' +
                ", opt_in_url='" + opt_in_url + '\'' +
                ", evaluated=" + evaluated +
                '}';
    }
}
