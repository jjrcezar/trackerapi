package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "recon_optout", schema = "recon", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"method", "data_for_method", "uri_for_method"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconOptout implements Serializable {
    @Column
    private Boolean evaluated;

    @Id
    private Long id;

    @Column
    private String name;

    @Column
    private String value;

    @Column
    private String domain;

    @Column
    private Date collected;

    @Column
    private String page_url;

    @Column
    private String collectedby;

    @Column
    private Date created;

    @Column
    private Date updated;

    @Column
    private String method;

    @Column
    private String data_for_method;

    @Column
    private String uri_for_method;

    @Column
    private String use;

    public ReconOptout() {
    }

    public ReconOptout(Boolean evaluated, Long id, String name, String value, String domain, Date collected, String
            page_url, String collectedby, Date created, Date updated, String method, String data_for_method, String
            uri_for_method, String use) {
        this.evaluated = evaluated;
        this.id = id;
        this.name = name;
        this.value = value;
        this.domain = domain;
        this.collected = collected;
        this.page_url = page_url;
        this.collectedby = collectedby;
        this.created = created;
        this.updated = updated;
        this.method = method;
        this.data_for_method = data_for_method;
        this.uri_for_method = uri_for_method;
        this.use = use;
    }

    public Boolean getEvaluated() {
        return evaluated;
    }

    public void setEvaluated(Boolean evaluated) {
        this.evaluated = evaluated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Date getCollected() {
        return collected;
    }

    public void setCollected(Date collected) {
        this.collected = collected;
    }

    public String getPage_url() {
        return page_url;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public String getCollectedby() {
        return collectedby;
    }

    public void setCollectedby(String collectedby) {
        this.collectedby = collectedby;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getData_for_method() {
        return data_for_method;
    }

    public void setData_for_method(String data_for_method) {
        this.data_for_method = data_for_method;
    }

    public String getUri_for_method() {
        return uri_for_method;
    }

    public void setUri_for_method(String uri_for_method) {
        this.uri_for_method = uri_for_method;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconOptout that = (ReconOptout) o;

        if (evaluated != null ? !evaluated.equals(that.evaluated) : that.evaluated != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (collected != null ? !collected.equals(that.collected) : that.collected != null) return false;
        if (page_url != null ? !page_url.equals(that.page_url) : that.page_url != null) return false;
        if (collectedby != null ? !collectedby.equals(that.collectedby) : that.collectedby != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;
        if (method != null ? !method.equals(that.method) : that.method != null) return false;
        if (data_for_method != null ? !data_for_method.equals(that.data_for_method) : that.data_for_method != null)
            return false;
        if (uri_for_method != null ? !uri_for_method.equals(that.uri_for_method) : that.uri_for_method != null)
            return false;
        return use != null ? use.equals(that.use) : that.use == null;
    }

    @Override
    public int hashCode() {
        int result = evaluated != null ? evaluated.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (collected != null ? collected.hashCode() : 0);
        result = 31 * result + (page_url != null ? page_url.hashCode() : 0);
        result = 31 * result + (collectedby != null ? collectedby.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (method != null ? method.hashCode() : 0);
        result = 31 * result + (data_for_method != null ? data_for_method.hashCode() : 0);
        result = 31 * result + (uri_for_method != null ? uri_for_method.hashCode() : 0);
        result = 31 * result + (use != null ? use.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconOptout{" +
                "evaluated=" + evaluated +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", domain='" + domain + '\'' +
                ", collected=" + collected +
                ", page_url='" + page_url + '\'' +
                ", collectedby='" + collectedby + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", method='" + method + '\'' +
                ", data_for_method='" + data_for_method + '\'' +
                ", uri_for_method='" + uri_for_method + '\'' +
                ", use='" + use + '\'' +
                '}';
    }
}
