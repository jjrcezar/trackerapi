package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tplist", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = "publish_url")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Tplist implements Serializable {
    @Id
    private String tpl_name;

    @Column
    private String publish_url;

    @Column
    private String manager;

    @Column
    private String description;

    @Column
    private String header_comment;

    @Column
    private Integer refresh;

    @Column
    private Date last_publish;

    @Column
    private Date last_update;

    @OneToMany(mappedBy = "tplist")
    private Set<Tplentry> tplentries = new HashSet<Tplentry>();

    public Tplist() {
    }

    public Tplist(String tpl_name, String publish_url, String manager, String description, String header_comment,
            Integer refresh, Date last_publish, Date last_update) {
        this.tpl_name = tpl_name;
        this.publish_url = publish_url;
        this.manager = manager;
        this.description = description;
        this.header_comment = header_comment;
        this.refresh = refresh;
        this.last_publish = last_publish;
        this.last_update = last_update;
    }

    public String getTpl_name() {
        return tpl_name;
    }

    public void setTpl_name(String tpl_name) {
        this.tpl_name = tpl_name;
    }

    public String getPublish_url() {
        return publish_url;
    }

    public void setPublish_url(String publish_url) {
        this.publish_url = publish_url;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHeader_comment() {
        return header_comment;
    }

    public void setHeader_comment(String header_comment) {
        this.header_comment = header_comment;
    }

    public Integer getRefresh() {
        return refresh;
    }

    public void setRefresh(Integer refresh) {
        this.refresh = refresh;
    }

    public Date getLast_publish() {
        return last_publish;
    }

    public void setLast_publish(Date last_publish) {
        this.last_publish = last_publish;
    }

    public Date getLast_update() {
        return last_update;
    }

    public void setLast_update(Date last_update) {
        this.last_update = last_update;
    }

    public Set<Tplentry> getTplentries() {
        return tplentries;
    }

    public void setTplentries(Set<Tplentry> tplentries) {
        this.tplentries = tplentries;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tplist tplist = (Tplist) o;

        if (tpl_name != null ? !tpl_name.equals(tplist.tpl_name) : tplist.tpl_name != null) return false;
        if (publish_url != null ? !publish_url.equals(tplist.publish_url) : tplist.publish_url != null) return false;
        if (manager != null ? !manager.equals(tplist.manager) : tplist.manager != null) return false;
        if (description != null ? !description.equals(tplist.description) : tplist.description != null) return false;
        if (header_comment != null ? !header_comment.equals(tplist.header_comment) : tplist.header_comment != null)
            return false;
        if (refresh != null ? !refresh.equals(tplist.refresh) : tplist.refresh != null) return false;
        if (last_publish != null ? !last_publish.equals(tplist.last_publish) : tplist.last_publish != null)
            return false;
        return last_update != null ? last_update.equals(tplist.last_update) : tplist.last_update == null;
    }

    @Override
    public int hashCode() {
        int result = tpl_name != null ? tpl_name.hashCode() : 0;
        result = 31 * result + (publish_url != null ? publish_url.hashCode() : 0);
        result = 31 * result + (manager != null ? manager.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (header_comment != null ? header_comment.hashCode() : 0);
        result = 31 * result + (refresh != null ? refresh.hashCode() : 0);
        result = 31 * result + (last_publish != null ? last_publish.hashCode() : 0);
        result = 31 * result + (last_update != null ? last_update.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tplist{" +
                "tpl_name='" + tpl_name + '\'' +
                ", publish_url='" + publish_url + '\'' +
                ", manager='" + manager + '\'' +
                ", description='" + description + '\'' +
                ", header_comment='" + header_comment + '\'' +
                ", refresh=" + refresh +
                ", last_publish=" + last_publish +
                ", last_update=" + last_update +
                '}';
    }
}
