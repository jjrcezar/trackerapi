package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "email_opt_out", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EmailOptOut implements Serializable {
    @Column
    private String email;

    @Column
    private String content;

    @Column
    private String subject;

    @Id
    private String email_ooid;

    @OneToOne
    @JoinColumn(name = "email_ooid", insertable = false, updatable = false)
    private OptOut optOut;

    public EmailOptOut() {
    }

    public EmailOptOut(String email, String content, String subject, String email_ooid) {
        this.email = email;
        this.content = content;
        this.subject = subject;
        this.email_ooid = email_ooid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getEmail_ooid() {
        return email_ooid;
    }

    public void setEmail_ooid(String email_ooid) {
        this.email_ooid = email_ooid;
    }

    public OptOut getOptOut() {
        return optOut;
    }

    public void setOptOut(OptOut optOut) {
        this.optOut = optOut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EmailOptOut that = (EmailOptOut) o;

        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (subject != null ? !subject.equals(that.subject) : that.subject != null) return false;
        return email_ooid != null ? email_ooid.equals(that.email_ooid) : that.email_ooid == null;
    }

    @Override
    public int hashCode() {
        int result = email != null ? email.hashCode() : 0;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (subject != null ? subject.hashCode() : 0);
        result = 31 * result + (email_ooid != null ? email_ooid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EmailOptOut{" +
                "email='" + email + '\'' +
                ", content='" + content + '\'' +
                ", subject='" + subject + '\'' +
                ", email_ooid='" + email_ooid + '\'' +
                '}';
    }
}
