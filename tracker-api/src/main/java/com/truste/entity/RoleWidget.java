package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "role_widget", schema = "portal")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RoleWidget implements Serializable {
    @Id
    private String role_id;

    @Id
    private String widget_id;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private Role role;

    @ManyToOne
    @JoinColumn(name = "widget_id", insertable = false, updatable = false)
    private Widget widget;

    public RoleWidget() {
    }

    public RoleWidget(String role_id, String widget_id) {
        this.role_id = role_id;
        this.widget_id = widget_id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getWidget_id() {
        return widget_id;
    }

    public void setWidget_id(String widget_id) {
        this.widget_id = widget_id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Widget getWidget() {
        return widget;
    }

    public void setWidget(Widget widget) {
        this.widget = widget;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleWidget that = (RoleWidget) o;

        if (role_id != null ? !role_id.equals(that.role_id) : that.role_id != null) return false;
        return widget_id != null ? widget_id.equals(that.widget_id) : that.widget_id == null;
    }

    @Override
    public int hashCode() {
        int result = role_id != null ? role_id.hashCode() : 0;
        result = 31 * result + (widget_id != null ? widget_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RoleWidget{" +
                "role_id='" + role_id + '\'' +
                ", widget_id='" + widget_id + '\'' +
                '}';
    }
}
