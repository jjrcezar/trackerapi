package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ta_markup_history", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TaMarkupHistory implements Serializable {
    @Id
    private String name;

    @Column
    private String data;

    @Id
    private Date date;

    @Column
    private String action;

    @Column
    private String comment;

    @Column
    private String user_id;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    public TaMarkupHistory() {
    }

    public TaMarkupHistory(String name, String data, Date date, String action, String comment, String user_id) {
        this.name = name;
        this.data = data;
        this.date = date;
        this.action = action;
        this.comment = comment;
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaMarkupHistory that = (TaMarkupHistory) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (action != null ? !action.equals(that.action) : that.action != null) return false;
        if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
        return user_id != null ? user_id.equals(that.user_id) : that.user_id == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        result = 31 * result + (user_id != null ? user_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TaMarkupHistory{" +
                "name='" + name + '\'' +
                ", data='" + data + '\'' +
                ", date=" + date +
                ", action='" + action + '\'' +
                ", comment='" + comment + '\'' +
                ", user_id='" + user_id + '\'' +
                '}';
    }
}
