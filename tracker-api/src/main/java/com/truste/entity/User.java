package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user", schema = "portal", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User implements Serializable {
    @Column
    private String name;

    @Column
    private String password;

    @Column
    private String firstname;

    @Column
    private String lastname;

    @Column
    private String email;

    @Id
    private String user_id;

    @OneToMany(mappedBy = "user")
    private Set<ActiveLogin> activeLogins = new HashSet<ActiveLogin>();

    @OneToMany(mappedBy = "user")
    private Set<TaMarkupHistory> taMarkupHistories = new HashSet<TaMarkupHistory>();

    @OneToMany(mappedBy = "user")
    private Set<UserRole> userRoles = new HashSet<UserRole>();

    @OneToMany(mappedBy = "user")
    private Set<UserWidget> userWidgets = new HashSet<UserWidget>();

    public User() {
    }

    public User(String name, String password, String firstname, String lastname, String email, String user_id) {

        this.name = name;
        this.password = password;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.user_id = user_id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Set<ActiveLogin> getActiveLogins() {
        return activeLogins;
    }

    public void setActiveLogins(Set<ActiveLogin> activeLogins) {
        this.activeLogins = activeLogins;
    }

    public Set<TaMarkupHistory> getTaMarkupHistories() {
        return taMarkupHistories;
    }

    public void setTaMarkupHistories(Set<TaMarkupHistory> taMarkupHistories) {
        this.taMarkupHistories = taMarkupHistories;
    }

    public Set<UserRole> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(Set<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    public Set<UserWidget> getUserWidgets() {
        return userWidgets;
    }

    public void setUserWidgets(Set<UserWidget> userWidgets) {
        this.userWidgets = userWidgets;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (name != null ? !name.equals(user.name) : user.name != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (firstname != null ? !firstname.equals(user.firstname) : user.firstname != null) return false;
        if (lastname != null ? !lastname.equals(user.lastname) : user.lastname != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        return user_id != null ? user_id.equals(user.user_id) : user.user_id == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (firstname != null ? firstname.hashCode() : 0);
        result = 31 * result + (lastname != null ? lastname.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (user_id != null ? user_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", user_id='" + user_id + '\'' +
                '}';
    }
}
