package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "user_role", schema = "portal")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class UserRole implements Serializable {
    @Id
    private String user_id;

    @Id
    private String role_id;

    @Column
    private String record_id;

    @ManyToOne
    @JoinColumn(name = "role_id", insertable = false, updatable = false)
    private Role role;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    public UserRole() {
    }

    public UserRole(String user_id, String role_id, String record_id) {
        this.user_id = user_id;
        this.role_id = role_id;
        this.record_id = record_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRole userRole = (UserRole) o;

        if (user_id != null ? !user_id.equals(userRole.user_id) : userRole.user_id != null) return false;
        if (role_id != null ? !role_id.equals(userRole.role_id) : userRole.role_id != null) return false;
        return record_id != null ? record_id.equals(userRole.record_id) : userRole.record_id == null;
    }

    @Override
    public int hashCode() {
        int result = user_id != null ? user_id.hashCode() : 0;
        result = 31 * result + (role_id != null ? role_id.hashCode() : 0);
        result = 31 * result + (record_id != null ? record_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "user_id='" + user_id + '\'' +
                ", role_id='" + role_id + '\'' +
                ", record_id='" + record_id + '\'' +
                '}';
    }
}
