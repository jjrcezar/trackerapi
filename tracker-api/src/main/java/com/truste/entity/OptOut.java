package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "opt_out", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OptOut implements Serializable {
    @Column
    private String ootechnique;

    @Column
    private String ootechnique_ooid;

    @Id
    private String ooid;

    @Column
    private String name;

    @OneToOne
    @JoinColumn(name = "ooid", insertable = false, updatable = false)
    private EmailOptOut emailOptOut;

    @OneToOne
    @JoinColumn(name = "ooid", insertable = false, updatable = false)
    private HttpOptOut httpOptOut;

    @OneToOne
    @JoinColumn(name = "ooid", insertable = false, updatable = false)
    private JsOptOut jsOptOut;

    public OptOut() {
    }

    public OptOut(String ootechnique, String ootechnique_ooid, String ooid, String name) {
        this.ootechnique = ootechnique;
        this.ootechnique_ooid = ootechnique_ooid;
        this.ooid = ooid;
        this.name = name;
    }

    public String getOotechnique() {
        return ootechnique;
    }

    public void setOotechnique(String ootechnique) {
        this.ootechnique = ootechnique;
    }

    public String getOotechnique_ooid() {
        return ootechnique_ooid;
    }

    public void setOotechnique_ooid(String ootechnique_ooid) {
        this.ootechnique_ooid = ootechnique_ooid;
    }

    public String getOoid() {
        return ooid;
    }

    public void setOoid(String ooid) {
        this.ooid = ooid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public EmailOptOut getEmailOptOut() {
        return emailOptOut;
    }

    public void setEmailOptOut(EmailOptOut emailOptOut) {
        this.emailOptOut = emailOptOut;
    }

    public HttpOptOut getHttpOptOut() {
        return httpOptOut;
    }

    public void setHttpOptOut(HttpOptOut httpOptOut) {
        this.httpOptOut = httpOptOut;
    }

    public JsOptOut getJsOptOut() {
        return jsOptOut;
    }

    public void setJsOptOut(JsOptOut jsOptOut) {
        this.jsOptOut = jsOptOut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OptOut optOut = (OptOut) o;

        if (ootechnique != null ? !ootechnique.equals(optOut.ootechnique) : optOut.ootechnique != null) return false;
        if (ootechnique_ooid != null ? !ootechnique_ooid.equals(optOut.ootechnique_ooid) : optOut.ootechnique_ooid !=
                null)
            return false;
        if (ooid != null ? !ooid.equals(optOut.ooid) : optOut.ooid != null) return false;
        return name != null ? name.equals(optOut.name) : optOut.name == null;
    }

    @Override
    public int hashCode() {
        int result = ootechnique != null ? ootechnique.hashCode() : 0;
        result = 31 * result + (ootechnique_ooid != null ? ootechnique_ooid.hashCode() : 0);
        result = 31 * result + (ooid != null ? ooid.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OptOut{" +
                "ootechnique='" + ootechnique + '\'' +
                ", ootechnique_ooid='" + ootechnique_ooid + '\'' +
                ", ooid='" + ooid + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
