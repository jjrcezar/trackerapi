package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "ta_asset", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TaAsset implements Serializable {
    @Id
    private String name;

    @Column
    private Long blob_data;

    @Column
    private String contenttype;

    public TaAsset() {
    }

    public TaAsset(String name, Long blob_data, String contenttype) {
        this.name = name;
        this.blob_data = blob_data;
        this.contenttype = contenttype;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBlob_data() {
        return blob_data;
    }

    public void setBlob_data(Long blob_data) {
        this.blob_data = blob_data;
    }

    public String getContenttype() {
        return contenttype;
    }

    public void setContenttype(String contenttype) {
        this.contenttype = contenttype;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaAsset taAsset = (TaAsset) o;

        if (name != null ? !name.equals(taAsset.name) : taAsset.name != null) return false;
        if (blob_data != null ? !blob_data.equals(taAsset.blob_data) : taAsset.blob_data != null) return false;
        return contenttype != null ? contenttype.equals(taAsset.contenttype) : taAsset.contenttype == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (blob_data != null ? blob_data.hashCode() : 0);
        result = 31 * result + (contenttype != null ? contenttype.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TaAsset{" +
                "name='" + name + '\'' +
                ", blob_data=" + blob_data +
                ", contenttype='" + contenttype + '\'' +
                '}';
    }
}
