package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "opt_out_scope", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"scope_type", "scope_type_id"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OptOutScope implements Serializable {
    @Column
    private String scope_type;

    @Column
    private String scope_type_id;

    @Id
    private String ooscope_id;

    @Column
    private String parent_scope_id;

    @OneToOne
    @JoinColumn(name = "ooscope_id", insertable = false, updatable = false)
    private Organization organization;

    @OneToOne
    @JoinColumn(name = "ooscope_id", insertable = false, updatable = false)
    private Domain domain;

    @OneToOne
    @JoinColumn(name = "ooscope_id", insertable = false, updatable = false)
    private Tracker tracker;

    @OneToMany(mappedBy = "optOutScope")
    private Set<OoscopeCategory> ooscopeCategories = new HashSet<OoscopeCategory>();

    @OneToMany(mappedBy = "optOutScope")
    private Set<VendorMappingOptOut> vendorMappingOptOuts = new HashSet<VendorMappingOptOut>();

    public OptOutScope() {
    }

    public OptOutScope(String scope_type, String scope_type_id, String ooscope_id, String parent_scope_id) {
        this.scope_type = scope_type;
        this.scope_type_id = scope_type_id;
        this.ooscope_id = ooscope_id;
        this.parent_scope_id = parent_scope_id;
    }

    public String getScope_type() {
        return scope_type;
    }

    public void setScope_type(String scope_type) {
        this.scope_type = scope_type;
    }

    public String getScope_type_id() {
        return scope_type_id;
    }

    public void setScope_type_id(String scope_type_id) {
        this.scope_type_id = scope_type_id;
    }

    public String getOoscope_id() {
        return ooscope_id;
    }

    public void setOoscope_id(String ooscope_id) {
        this.ooscope_id = ooscope_id;
    }

    public String getParent_scope_id() {
        return parent_scope_id;
    }

    public void setParent_scope_id(String parent_scope_id) {
        this.parent_scope_id = parent_scope_id;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    public Set<OoscopeCategory> getOoscopeCategories() {
        return ooscopeCategories;
    }

    public void setOoscopeCategories(Set<OoscopeCategory> ooscopeCategories) {
        this.ooscopeCategories = ooscopeCategories;
    }

    public Set<VendorMappingOptOut> getVendorMappingOptOuts() {
        return vendorMappingOptOuts;
    }

    public void setVendorMappingOptOuts(Set<VendorMappingOptOut> vendorMappingOptOuts) {
        this.vendorMappingOptOuts = vendorMappingOptOuts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OptOutScope that = (OptOutScope) o;

        if (scope_type != null ? !scope_type.equals(that.scope_type) : that.scope_type != null) return false;
        if (scope_type_id != null ? !scope_type_id.equals(that.scope_type_id) : that.scope_type_id != null)
            return false;
        if (ooscope_id != null ? !ooscope_id.equals(that.ooscope_id) : that.ooscope_id != null) return false;
        return parent_scope_id != null ? parent_scope_id.equals(that.parent_scope_id) : that.parent_scope_id == null;
    }

    @Override
    public int hashCode() {
        int result = scope_type != null ? scope_type.hashCode() : 0;
        result = 31 * result + (scope_type_id != null ? scope_type_id.hashCode() : 0);
        result = 31 * result + (ooscope_id != null ? ooscope_id.hashCode() : 0);
        result = 31 * result + (parent_scope_id != null ? parent_scope_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OptOutScope{" +
                "scope_type='" + scope_type + '\'' +
                ", scope_type_id='" + scope_type_id + '\'' +
                ", ooscope_id='" + ooscope_id + '\'' +
                ", parent_scope_id='" + parent_scope_id + '\'' +
                '}';
    }
}
