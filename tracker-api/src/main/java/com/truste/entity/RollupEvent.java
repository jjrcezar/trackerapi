package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "rollup_event", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class RollupEvent implements Serializable {
    @Column
    private Date started;

    @Column
    private Date ended;

    @Column
    private Boolean success;

    @Column
    private BigDecimal original_count;

    @Column
    private Integer rolledup_count;

    @Column
    private Integer hour_rollup_calls;

    @Column
    private Date date_range_start;

    @Column
    private Date date_range_end;

    @Column
    private BigDecimal total_impressions;

    @Column
    private Integer total_clicks;

    @Column
    private Date min_start;

    @Column
    private Date max_end;

    @Id
    private Integer rollup_event_id;

    @Column
    private Integer duration;

    public RollupEvent() {
    }

    public RollupEvent(Date started, Date ended, Boolean success, BigDecimal original_count, Integer rolledup_count,
            Integer hour_rollup_calls, Date date_range_start, Date date_range_end, BigDecimal total_impressions,
            Integer total_clicks, Date min_start, Date max_end, Integer rollup_event_id, Integer duration) {
        this.started = started;
        this.ended = ended;
        this.success = success;
        this.original_count = original_count;
        this.rolledup_count = rolledup_count;
        this.hour_rollup_calls = hour_rollup_calls;
        this.date_range_start = date_range_start;
        this.date_range_end = date_range_end;
        this.total_impressions = total_impressions;
        this.total_clicks = total_clicks;
        this.min_start = min_start;
        this.max_end = max_end;
        this.rollup_event_id = rollup_event_id;
        this.duration = duration;
    }

    public Date getStarted() {
        return started;
    }

    public void setStarted(Date started) {
        this.started = started;
    }

    public Date getEnded() {
        return ended;
    }

    public void setEnded(Date ended) {
        this.ended = ended;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public BigDecimal getOriginal_count() {
        return original_count;
    }

    public void setOriginal_count(BigDecimal original_count) {
        this.original_count = original_count;
    }

    public Integer getRolledup_count() {
        return rolledup_count;
    }

    public void setRolledup_count(Integer rolledup_count) {
        this.rolledup_count = rolledup_count;
    }

    public Integer getHour_rollup_calls() {
        return hour_rollup_calls;
    }

    public void setHour_rollup_calls(Integer hour_rollup_calls) {
        this.hour_rollup_calls = hour_rollup_calls;
    }

    public Date getDate_range_start() {
        return date_range_start;
    }

    public void setDate_range_start(Date date_range_start) {
        this.date_range_start = date_range_start;
    }

    public Date getDate_range_end() {
        return date_range_end;
    }

    public void setDate_range_end(Date date_range_end) {
        this.date_range_end = date_range_end;
    }

    public BigDecimal getTotal_impressions() {
        return total_impressions;
    }

    public void setTotal_impressions(BigDecimal total_impressions) {
        this.total_impressions = total_impressions;
    }

    public Integer getTotal_clicks() {
        return total_clicks;
    }

    public void setTotal_clicks(Integer total_clicks) {
        this.total_clicks = total_clicks;
    }

    public Date getMin_start() {
        return min_start;
    }

    public void setMin_start(Date min_start) {
        this.min_start = min_start;
    }

    public Date getMax_end() {
        return max_end;
    }

    public void setMax_end(Date max_end) {
        this.max_end = max_end;
    }

    public Integer getRollup_event_id() {
        return rollup_event_id;
    }

    public void setRollup_event_id(Integer rollup_event_id) {
        this.rollup_event_id = rollup_event_id;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RollupEvent that = (RollupEvent) o;

        if (started != null ? !started.equals(that.started) : that.started != null) return false;
        if (ended != null ? !ended.equals(that.ended) : that.ended != null) return false;
        if (success != null ? !success.equals(that.success) : that.success != null) return false;
        if (original_count != null ? !original_count.equals(that.original_count) : that.original_count != null)
            return false;
        if (rolledup_count != null ? !rolledup_count.equals(that.rolledup_count) : that.rolledup_count != null)
            return false;
        if (hour_rollup_calls != null ? !hour_rollup_calls.equals(that.hour_rollup_calls) : that.hour_rollup_calls !=
                null)
            return false;
        if (date_range_start != null ? !date_range_start.equals(that.date_range_start) : that.date_range_start != null)
            return false;
        if (date_range_end != null ? !date_range_end.equals(that.date_range_end) : that.date_range_end != null)
            return false;
        if (total_impressions != null ? !total_impressions.equals(that.total_impressions) : that.total_impressions !=
                null)
            return false;
        if (total_clicks != null ? !total_clicks.equals(that.total_clicks) : that.total_clicks != null) return false;
        if (min_start != null ? !min_start.equals(that.min_start) : that.min_start != null) return false;
        if (max_end != null ? !max_end.equals(that.max_end) : that.max_end != null) return false;
        if (rollup_event_id != null ? !rollup_event_id.equals(that.rollup_event_id) : that.rollup_event_id != null)
            return false;
        return duration != null ? duration.equals(that.duration) : that.duration == null;
    }

    @Override
    public int hashCode() {
        int result = started != null ? started.hashCode() : 0;
        result = 31 * result + (ended != null ? ended.hashCode() : 0);
        result = 31 * result + (success != null ? success.hashCode() : 0);
        result = 31 * result + (original_count != null ? original_count.hashCode() : 0);
        result = 31 * result + (rolledup_count != null ? rolledup_count.hashCode() : 0);
        result = 31 * result + (hour_rollup_calls != null ? hour_rollup_calls.hashCode() : 0);
        result = 31 * result + (date_range_start != null ? date_range_start.hashCode() : 0);
        result = 31 * result + (date_range_end != null ? date_range_end.hashCode() : 0);
        result = 31 * result + (total_impressions != null ? total_impressions.hashCode() : 0);
        result = 31 * result + (total_clicks != null ? total_clicks.hashCode() : 0);
        result = 31 * result + (min_start != null ? min_start.hashCode() : 0);
        result = 31 * result + (max_end != null ? max_end.hashCode() : 0);
        result = 31 * result + (rollup_event_id != null ? rollup_event_id.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RollupEvent{" +
                "started=" + started +
                ", ended=" + ended +
                ", success=" + success +
                ", original_count=" + original_count +
                ", rolledup_count=" + rolledup_count +
                ", hour_rollup_calls=" + hour_rollup_calls +
                ", date_range_start=" + date_range_start +
                ", date_range_end=" + date_range_end +
                ", total_impressions=" + total_impressions +
                ", total_clicks=" + total_clicks +
                ", min_start=" + min_start +
                ", max_end=" + max_end +
                ", rollup_event_id=" + rollup_event_id +
                ", duration=" + duration +
                '}';
    }
}
