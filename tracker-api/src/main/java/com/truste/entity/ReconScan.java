package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "recon_scan", schema = "recon")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconScan implements Serializable {
    @Id
    private String domain;

    @Column
    private Date last_access_date;

    @Column
    private Date last_update;

    @Column
    private String md5;

    @OneToMany(mappedBy = "reconScan")
    private Set<ReconScanMap> reconScanMaps = new HashSet<ReconScanMap>();

    public ReconScan() {
    }

    public ReconScan(String domain, Date last_access_date, Date last_update, String md5) {
        this.domain = domain;
        this.last_access_date = last_access_date;
        this.last_update = last_update;
        this.md5 = md5;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Date getLast_access_date() {
        return last_access_date;
    }

    public void setLast_access_date(Date last_access_date) {
        this.last_access_date = last_access_date;
    }

    public Date getLast_update() {
        return last_update;
    }

    public void setLast_update(Date last_update) {
        this.last_update = last_update;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public Set<ReconScanMap> getReconScanMaps() {
        return reconScanMaps;
    }

    public void setReconScanMaps(Set<ReconScanMap> reconScanMaps) {
        this.reconScanMaps = reconScanMaps;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconScan reconScan = (ReconScan) o;

        if (domain != null ? !domain.equals(reconScan.domain) : reconScan.domain != null) return false;
        if (last_access_date != null ? !last_access_date.equals(reconScan.last_access_date) : reconScan
                .last_access_date != null)
            return false;
        if (last_update != null ? !last_update.equals(reconScan.last_update) : reconScan.last_update != null)
            return false;
        return md5 != null ? md5.equals(reconScan.md5) : reconScan.md5 == null;
    }

    @Override
    public int hashCode() {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (last_access_date != null ? last_access_date.hashCode() : 0);
        result = 31 * result + (last_update != null ? last_update.hashCode() : 0);
        result = 31 * result + (md5 != null ? md5.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconScan{" +
                "domain='" + domain + '\'' +
                ", last_access_date=" + last_access_date +
                ", last_update=" + last_update +
                ", md5='" + md5 + '\'' +
                '}';
    }
}
