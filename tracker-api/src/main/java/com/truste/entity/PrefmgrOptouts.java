package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "prefmgr_optouts", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PrefmgrOptouts implements Serializable {
    @Id
    private String ooid;

    @Id
    private String prefmgr_id;

    @Column
    private String category_override;

    public PrefmgrOptouts() {
    }

    public PrefmgrOptouts(String ooid, String prefmgr_id, String category_override) {
        this.ooid = ooid;
        this.prefmgr_id = prefmgr_id;
        this.category_override = category_override;
    }

    public String getOoid() {
        return ooid;
    }

    public void setOoid(String ooid) {
        this.ooid = ooid;
    }

    public String getPrefmgr_id() {
        return prefmgr_id;
    }

    public void setPrefmgr_id(String prefmgr_id) {
        this.prefmgr_id = prefmgr_id;
    }

    public String getCategory_override() {
        return category_override;
    }

    public void setCategory_override(String category_override) {
        this.category_override = category_override;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrefmgrOptouts that = (PrefmgrOptouts) o;

        if (ooid != null ? !ooid.equals(that.ooid) : that.ooid != null) return false;
        if (prefmgr_id != null ? !prefmgr_id.equals(that.prefmgr_id) : that.prefmgr_id != null) return false;
        return category_override != null ? category_override.equals(that.category_override) : that.category_override
                == null;
    }

    @Override
    public int hashCode() {
        int result = ooid != null ? ooid.hashCode() : 0;
        result = 31 * result + (prefmgr_id != null ? prefmgr_id.hashCode() : 0);
        result = 31 * result + (category_override != null ? category_override.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PrefmgrOptouts{" +
                "ooid='" + ooid + '\'' +
                ", prefmgr_id='" + prefmgr_id + '\'' +
                ", category_override='" + category_override + '\'' +
                '}';
    }
}
