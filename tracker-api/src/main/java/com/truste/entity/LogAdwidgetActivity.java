package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "log_adwidget_activity", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LogAdwidgetActivity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String unique_id;

    @Column
    private String action;

    @Column
    private Date start_time;

    @Column
    private String partner_id;

    @Column
    private String advertiser_id;

    @Column
    private String campaign_id;

    @Column
    private String ad_size;

    @Column
    private String widget;

    @Column
    private String click_data1;

    @Column
    private String click_data2;

    @Column
    private Date end_time;

    public LogAdwidgetActivity() {
    }

    public LogAdwidgetActivity(String unique_id, String action, Date start_time, String partner_id, String
            advertiser_id, String campaign_id, String ad_size, String widget, String click_data1, String click_data2,
            Date end_time) {
        this.unique_id = unique_id;
        this.action = action;
        this.start_time = start_time;
        this.partner_id = partner_id;
        this.advertiser_id = advertiser_id;
        this.campaign_id = campaign_id;
        this.ad_size = ad_size;
        this.widget = widget;
        this.click_data1 = click_data1;
        this.click_data2 = click_data2;
        this.end_time = end_time;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getAdvertiser_id() {
        return advertiser_id;
    }

    public void setAdvertiser_id(String advertiser_id) {
        this.advertiser_id = advertiser_id;
    }

    public String getCampaign_id() {
        return campaign_id;
    }

    public void setCampaign_id(String campaign_id) {
        this.campaign_id = campaign_id;
    }

    public String getAd_size() {
        return ad_size;
    }

    public void setAd_size(String ad_size) {
        this.ad_size = ad_size;
    }

    public String getWidget() {
        return widget;
    }

    public void setWidget(String widget) {
        this.widget = widget;
    }

    public String getClick_data1() {
        return click_data1;
    }

    public void setClick_data1(String click_data1) {
        this.click_data1 = click_data1;
    }

    public String getClick_data2() {
        return click_data2;
    }

    public void setClick_data2(String click_data2) {
        this.click_data2 = click_data2;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogAdwidgetActivity that = (LogAdwidgetActivity) o;

        if (unique_id != null ? !unique_id.equals(that.unique_id) : that.unique_id != null) return false;
        if (action != null ? !action.equals(that.action) : that.action != null) return false;
        if (start_time != null ? !start_time.equals(that.start_time) : that.start_time != null) return false;
        if (partner_id != null ? !partner_id.equals(that.partner_id) : that.partner_id != null) return false;
        if (advertiser_id != null ? !advertiser_id.equals(that.advertiser_id) : that.advertiser_id != null)
            return false;
        if (campaign_id != null ? !campaign_id.equals(that.campaign_id) : that.campaign_id != null) return false;
        if (ad_size != null ? !ad_size.equals(that.ad_size) : that.ad_size != null) return false;
        if (widget != null ? !widget.equals(that.widget) : that.widget != null) return false;
        if (click_data1 != null ? !click_data1.equals(that.click_data1) : that.click_data1 != null) return false;
        if (click_data2 != null ? !click_data2.equals(that.click_data2) : that.click_data2 != null) return false;
        return end_time != null ? end_time.equals(that.end_time) : that.end_time == null;
    }

    @Override
    public int hashCode() {
        int result = unique_id != null ? unique_id.hashCode() : 0;
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (start_time != null ? start_time.hashCode() : 0);
        result = 31 * result + (partner_id != null ? partner_id.hashCode() : 0);
        result = 31 * result + (advertiser_id != null ? advertiser_id.hashCode() : 0);
        result = 31 * result + (campaign_id != null ? campaign_id.hashCode() : 0);
        result = 31 * result + (ad_size != null ? ad_size.hashCode() : 0);
        result = 31 * result + (widget != null ? widget.hashCode() : 0);
        result = 31 * result + (click_data1 != null ? click_data1.hashCode() : 0);
        result = 31 * result + (click_data2 != null ? click_data2.hashCode() : 0);
        result = 31 * result + (end_time != null ? end_time.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LogAdwidgetActivity{" +
                "unique_id='" + unique_id + '\'' +
                ", action='" + action + '\'' +
                ", start_time=" + start_time +
                ", partner_id='" + partner_id + '\'' +
                ", advertiser_id='" + advertiser_id + '\'' +
                ", campaign_id='" + campaign_id + '\'' +
                ", ad_size='" + ad_size + '\'' +
                ", widget='" + widget + '\'' +
                ", click_data1='" + click_data1 + '\'' +
                ", click_data2='" + click_data2 + '\'' +
                ", end_time=" + end_time +
                '}';
    }
}
