package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "partner", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Partner implements Serializable {
    @Column
    private String name;

    @Id
    private String partnerid;

    @Column
    private String fulldisplayname;

    @Column
    private String logo;

    @Column
    private String link;

    @Column
    private String text;

    @OneToMany(mappedBy = "partner")
    private Set<Advertiser> advertisers = new HashSet<Advertiser>();

    public Partner() {
    }

    public Partner(String name, String partnerid, String fulldisplayname, String logo, String link, String text) {
        this.name = name;
        this.partnerid = partnerid;
        this.fulldisplayname = fulldisplayname;
        this.logo = logo;
        this.link = link;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPartnerid() {
        return partnerid;
    }

    public void setPartnerid(String partnerid) {
        this.partnerid = partnerid;
    }

    public String getFulldisplayname() {
        return fulldisplayname;
    }

    public void setFulldisplayname(String fulldisplayname) {
        this.fulldisplayname = fulldisplayname;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Set<Advertiser> getAdvertisers() {
        return advertisers;
    }

    public void setAdvertisers(Set<Advertiser> advertisers) {
        this.advertisers = advertisers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Partner partner = (Partner) o;

        if (name != null ? !name.equals(partner.name) : partner.name != null) return false;
        if (partnerid != null ? !partnerid.equals(partner.partnerid) : partner.partnerid != null) return false;
        if (fulldisplayname != null ? !fulldisplayname.equals(partner.fulldisplayname) : partner.fulldisplayname !=
                null)
            return false;
        if (logo != null ? !logo.equals(partner.logo) : partner.logo != null) return false;
        if (link != null ? !link.equals(partner.link) : partner.link != null) return false;
        return text != null ? text.equals(partner.text) : partner.text == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (partnerid != null ? partnerid.hashCode() : 0);
        result = 31 * result + (fulldisplayname != null ? fulldisplayname.hashCode() : 0);
        result = 31 * result + (logo != null ? logo.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Partner{" +
                "name='" + name + '\'' +
                ", partnerid='" + partnerid + '\'' +
                ", fulldisplayname='" + fulldisplayname + '\'' +
                ", logo='" + logo + '\'' +
                ", link='" + link + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
