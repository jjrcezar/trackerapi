package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "prefmgr_contents", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PrefmgrContents implements Serializable {
    @Id
    private String preflayout;

    @Id
    private String preftype;

    @Id
    private String key;

    @Column
    private String contents;

    public PrefmgrContents() {
    }

    public PrefmgrContents(String preflayout, String preftype, String key, String contents) {
        this.preflayout = preflayout;
        this.preftype = preftype;
        this.key = key;
        this.contents = contents;
    }

    public String getPreflayout() {
        return preflayout;
    }

    public void setPreflayout(String preflayout) {
        this.preflayout = preflayout;
    }

    public String getPreftype() {
        return preftype;
    }

    public void setPreftype(String preftype) {
        this.preftype = preftype;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrefmgrContents that = (PrefmgrContents) o;

        if (preflayout != null ? !preflayout.equals(that.preflayout) : that.preflayout != null) return false;
        if (preftype != null ? !preftype.equals(that.preftype) : that.preftype != null) return false;
        if (key != null ? !key.equals(that.key) : that.key != null) return false;
        return contents != null ? contents.equals(that.contents) : that.contents == null;
    }

    @Override
    public int hashCode() {
        int result = preflayout != null ? preflayout.hashCode() : 0;
        result = 31 * result + (preftype != null ? preftype.hashCode() : 0);
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + (contents != null ? contents.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PrefmgrContents{" +
                "preflayout='" + preflayout + '\'' +
                ", preftype='" + preftype + '\'' +
                ", key='" + key + '\'' +
                ", contents='" + contents + '\'' +
                '}';
    }
}
