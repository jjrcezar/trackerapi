package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "locale_groups", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LocaleGroups implements Serializable {

    @Id
    private String group_id;

    @Column
    private String name;

    @Column
    private String locales;

    @Column
    private String default_locale;

    public LocaleGroups() {
    }

    public LocaleGroups(String group_id, String name, String locales, String default_locale) {
        this.group_id = group_id;
        this.name = name;
        this.locales = locales;
        this.default_locale = default_locale;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocales() {
        return locales;
    }

    public void setLocales(String locales) {
        this.locales = locales;
    }

    public String getDefault_locale() {
        return default_locale;
    }

    public void setDefault_locale(String default_locale) {
        this.default_locale = default_locale;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LocaleGroups that = (LocaleGroups) o;

        if (group_id != null ? !group_id.equals(that.group_id) : that.group_id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (locales != null ? !locales.equals(that.locales) : that.locales != null) return false;
        return default_locale != null ? default_locale.equals(that.default_locale) : that.default_locale == null;
    }

    @Override
    public int hashCode() {
        int result = group_id != null ? group_id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (locales != null ? locales.hashCode() : 0);
        result = 31 * result + (default_locale != null ? default_locale.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LocaleGroups{" +
                "group_id='" + group_id + '\'' +
                ", name='" + name + '\'' +
                ", locales='" + locales + '\'' +
                ", default_locale='" + default_locale + '\'' +
                '}';
    }
}
