package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "html5_storage", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"domain_id", "name"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Html5Storage implements Serializable {
    @Column
    private String domain_id;

    @Column
    private String name;

    @Id
    private String html5_tid;

    @Column
    private String value;

    @OneToOne
    @JoinColumn(name = "html5_tid", insertable = false, updatable = false)
    private Tracker tracker;

    public Html5Storage() {
    }

    public Html5Storage(String domain_id, String name, String html5_tid, String value) {
        this.domain_id = domain_id;
        this.name = name;
        this.html5_tid = html5_tid;
        this.value = value;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtml5_tid() {
        return html5_tid;
    }

    public void setHtml5_tid(String html5_tid) {
        this.html5_tid = html5_tid;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Tracker getTracker() {
        return tracker;
    }

    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Html5Storage that = (Html5Storage) o;

        if (domain_id != null ? !domain_id.equals(that.domain_id) : that.domain_id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (html5_tid != null ? !html5_tid.equals(that.html5_tid) : that.html5_tid != null) return false;
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = domain_id != null ? domain_id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (html5_tid != null ? html5_tid.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Html5Storage{" +
                "domain_id='" + domain_id + '\'' +
                ", name='" + name + '\'' +
                ", html5_tid='" + html5_tid + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
