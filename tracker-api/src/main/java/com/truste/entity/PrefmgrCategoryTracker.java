package com.truste.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "prefmgr_category_tracker", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PrefmgrCategoryTracker implements Serializable {
    @Id
    private String category_id;

    @Id
    private String tscope_id;

    @Id
    private String tracker_id;

    public PrefmgrCategoryTracker() {
    }

    public PrefmgrCategoryTracker(String category_id, String tscope_id, String tracker_id) {
        this.category_id = category_id;
        this.tscope_id = tscope_id;
        this.tracker_id = tracker_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTscope_id() {
        return tscope_id;
    }

    public void setTscope_id(String tscope_id) {
        this.tscope_id = tscope_id;
    }

    public String getTracker_id() {
        return tracker_id;
    }

    public void setTracker_id(String tracker_id) {
        this.tracker_id = tracker_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrefmgrCategoryTracker that = (PrefmgrCategoryTracker) o;

        if (category_id != null ? !category_id.equals(that.category_id) : that.category_id != null) return false;
        if (tscope_id != null ? !tscope_id.equals(that.tscope_id) : that.tscope_id != null) return false;
        return tracker_id != null ? tracker_id.equals(that.tracker_id) : that.tracker_id == null;
    }

    @Override
    public int hashCode() {
        int result = category_id != null ? category_id.hashCode() : 0;
        result = 31 * result + (tscope_id != null ? tscope_id.hashCode() : 0);
        result = 31 * result + (tracker_id != null ? tracker_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PrefmgrCategoryTracker{" +
                "category_id='" + category_id + '\'' +
                ", tscope_id='" + tscope_id + '\'' +
                ", tracker_id='" + tracker_id + '\'' +
                '}';
    }
}
