package com.truste.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "t_applicability", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TApplicability implements Serializable {
    @Id
    private String tid;

    @Id
    private String tscope_id;

    public TApplicability() {
    }

    public TApplicability(String tid, String tscope_id) {
        this.tid = tid;
        this.tscope_id = tscope_id;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getTscope_id() {
        return tscope_id;
    }

    public void setTscope_id(String tscope_id) {
        this.tscope_id = tscope_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TApplicability that = (TApplicability) o;

        if (tid != null ? !tid.equals(that.tid) : that.tid != null) return false;
        return tscope_id != null ? tscope_id.equals(that.tscope_id) : that.tscope_id == null;
    }

    @Override
    public int hashCode() {
        int result = tid != null ? tid.hashCode() : 0;
        result = 31 * result + (tscope_id != null ? tscope_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TApplicability{" +
                "tid='" + tid + '\'' +
                ", tscope_id='" + tscope_id + '\'' +
                '}';
    }
}
