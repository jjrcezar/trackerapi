package com.truste.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "dataloadtrackerdb0203_domain", schema = "import")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Dataloadtrackerdb0203Domain implements Serializable {
    @Id
    private String domain;

    @Id
    private String organization;

    public Dataloadtrackerdb0203Domain() {
    }

    public Dataloadtrackerdb0203Domain(String domain, String organization) {
        this.domain = domain;
        this.organization = organization;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dataloadtrackerdb0203Domain that = (Dataloadtrackerdb0203Domain) o;

        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        return organization != null ? organization.equals(that.organization) : that.organization == null;
    }

    @Override
    public int hashCode() {
        int result = domain != null ? domain.hashCode() : 0;
        result = 31 * result + (organization != null ? organization.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Dataloadtrackerdb0203Domain{" +
                "domain='" + domain + '\'' +
                ", organization='" + organization + '\'' +
                '}';
    }
}
