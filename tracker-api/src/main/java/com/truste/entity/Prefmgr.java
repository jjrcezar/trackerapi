package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "prefmgr", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Prefmgr implements Serializable {
    @Column
    private String name;

    @Column
    private String description;

    @Id
    private String prefmgr_id;

    @Column
    private String preftype;

    @Column
    private String pid;

    @Column
    private String aid;

    @Column
    private String cid;

    @Column
    private String size;

    public Prefmgr() {
    }

    public Prefmgr(String name, String description, String prefmgr_id, String preftype, String pid, String aid,
            String cid, String size) {
        this.name = name;
        this.description = description;
        this.prefmgr_id = prefmgr_id;
        this.preftype = preftype;
        this.pid = pid;
        this.aid = aid;
        this.cid = cid;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrefmgr_id() {
        return prefmgr_id;
    }

    public void setPrefmgr_id(String prefmgr_id) {
        this.prefmgr_id = prefmgr_id;
    }

    public String getPreftype() {
        return preftype;
    }

    public void setPreftype(String preftype) {
        this.preftype = preftype;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Prefmgr prefmgr = (Prefmgr) o;

        if (name != null ? !name.equals(prefmgr.name) : prefmgr.name != null) return false;
        if (description != null ? !description.equals(prefmgr.description) : prefmgr.description != null) return false;
        if (prefmgr_id != null ? !prefmgr_id.equals(prefmgr.prefmgr_id) : prefmgr.prefmgr_id != null) return false;
        if (preftype != null ? !preftype.equals(prefmgr.preftype) : prefmgr.preftype != null) return false;
        if (pid != null ? !pid.equals(prefmgr.pid) : prefmgr.pid != null) return false;
        if (aid != null ? !aid.equals(prefmgr.aid) : prefmgr.aid != null) return false;
        if (cid != null ? !cid.equals(prefmgr.cid) : prefmgr.cid != null) return false;
        return size != null ? size.equals(prefmgr.size) : prefmgr.size == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (prefmgr_id != null ? prefmgr_id.hashCode() : 0);
        result = 31 * result + (preftype != null ? preftype.hashCode() : 0);
        result = 31 * result + (pid != null ? pid.hashCode() : 0);
        result = 31 * result + (aid != null ? aid.hashCode() : 0);
        result = 31 * result + (cid != null ? cid.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Prefmgr{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", prefmgr_id='" + prefmgr_id + '\'' +
                ", preftype='" + preftype + '\'' +
                ", pid='" + pid + '\'' +
                ", aid='" + aid + '\'' +
                ", cid='" + cid + '\'' +
                ", size='" + size + '\'' +
                '}';
    }
}
