package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "interstitial", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Interstitial implements Serializable {
    @Id
    private String eid;

    @Id
    private String size;

    @Column
    private String template;

    @Column
    private String target;

    @Column
    private String isize;

    @Id
    private String level;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "eid", referencedColumnName = "eid", insertable = false, updatable = false),
            @JoinColumn(name = "size", referencedColumnName = "size", insertable = false, updatable = false),
            @JoinColumn(name = "level", referencedColumnName = "level", insertable = false, updatable = false)})
    private Adunit adunit;

    public Interstitial() {
    }

    public Interstitial(String eid, String size, String template, String target, String isize, String level) {
        this.eid = eid;
        this.size = size;
        this.template = template;
        this.target = target;
        this.isize = isize;
        this.level = level;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getIsize() {
        return isize;
    }

    public void setIsize(String isize) {
        this.isize = isize;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Interstitial that = (Interstitial) o;

        if (eid != null ? !eid.equals(that.eid) : that.eid != null) return false;
        if (size != null ? !size.equals(that.size) : that.size != null) return false;
        if (template != null ? !template.equals(that.template) : that.template != null) return false;
        if (target != null ? !target.equals(that.target) : that.target != null) return false;
        if (isize != null ? !isize.equals(that.isize) : that.isize != null) return false;
        return level != null ? level.equals(that.level) : that.level == null;
    }

    @Override
    public int hashCode() {
        int result = eid != null ? eid.hashCode() : 0;
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + (template != null ? template.hashCode() : 0);
        result = 31 * result + (target != null ? target.hashCode() : 0);
        result = 31 * result + (isize != null ? isize.hashCode() : 0);
        result = 31 * result + (level != null ? level.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Interstitial{" +
                "eid='" + eid + '\'' +
                ", size='" + size + '\'' +
                ", template='" + template + '\'' +
                ", target='" + target + '\'' +
                ", isize='" + isize + '\'' +
                ", level='" + level + '\'' +
                '}';
    }
}
