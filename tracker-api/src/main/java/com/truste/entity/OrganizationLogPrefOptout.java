package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "organization_log_pref_optout", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationLogPrefOptout implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String partner_id;

    @Column
    private String advertiser_id;

    @Column
    private String campaign_id;

    @Column
    private String size;

    @Column
    private String organization_id;

    @Column
    private String status;

    @Column
    private Float duration;

    @Column
    private Date entry_date;

    @Column
    private String client_session;

    @Column
    private String client_useragent;

    public OrganizationLogPrefOptout() {
    }

    public OrganizationLogPrefOptout(String partner_id, String advertiser_id, String campaign_id, String size, String
            organization_id, String status, Float duration, Date entry_date, String client_session, String
            client_useragent) {
        this.partner_id = partner_id;
        this.advertiser_id = advertiser_id;
        this.campaign_id = campaign_id;
        this.size = size;
        this.organization_id = organization_id;
        this.status = status;
        this.duration = duration;
        this.entry_date = entry_date;
        this.client_session = client_session;
        this.client_useragent = client_useragent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getAdvertiser_id() {
        return advertiser_id;
    }

    public void setAdvertiser_id(String advertiser_id) {
        this.advertiser_id = advertiser_id;
    }

    public String getCampaign_id() {
        return campaign_id;
    }

    public void setCampaign_id(String campaign_id) {
        this.campaign_id = campaign_id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Float getDuration() {
        return duration;
    }

    public void setDuration(Float duration) {
        this.duration = duration;
    }

    public Date getEntry_date() {
        return entry_date;
    }

    public void setEntry_date(Date entry_date) {
        this.entry_date = entry_date;
    }

    public String getClient_session() {
        return client_session;
    }

    public void setClient_session(String client_session) {
        this.client_session = client_session;
    }

    public String getClient_useragent() {
        return client_useragent;
    }

    public void setClient_useragent(String client_useragent) {
        this.client_useragent = client_useragent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganizationLogPrefOptout that = (OrganizationLogPrefOptout) o;

        if (partner_id != null ? !partner_id.equals(that.partner_id) : that.partner_id != null) return false;
        if (advertiser_id != null ? !advertiser_id.equals(that.advertiser_id) : that.advertiser_id != null)
            return false;
        if (campaign_id != null ? !campaign_id.equals(that.campaign_id) : that.campaign_id != null) return false;
        if (size != null ? !size.equals(that.size) : that.size != null) return false;
        if (organization_id != null ? !organization_id.equals(that.organization_id) : that.organization_id != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
        if (entry_date != null ? !entry_date.equals(that.entry_date) : that.entry_date != null) return false;
        if (client_session != null ? !client_session.equals(that.client_session) : that.client_session != null)
            return false;
        return client_useragent != null ? client_useragent.equals(that.client_useragent) : that.client_useragent ==
                null;
    }

    @Override
    public int hashCode() {
        int result = partner_id != null ? partner_id.hashCode() : 0;
        result = 31 * result + (advertiser_id != null ? advertiser_id.hashCode() : 0);
        result = 31 * result + (campaign_id != null ? campaign_id.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + (organization_id != null ? organization_id.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        result = 31 * result + (entry_date != null ? entry_date.hashCode() : 0);
        result = 31 * result + (client_session != null ? client_session.hashCode() : 0);
        result = 31 * result + (client_useragent != null ? client_useragent.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrganizationLogPrefOptout{" +
                "partner_id='" + partner_id + '\'' +
                ", advertiser_id='" + advertiser_id + '\'' +
                ", campaign_id='" + campaign_id + '\'' +
                ", size='" + size + '\'' +
                ", organization_id='" + organization_id + '\'' +
                ", status='" + status + '\'' +
                ", duration=" + duration +
                ", entry_date=" + entry_date +
                ", client_session='" + client_session + '\'' +
                ", client_useragent='" + client_useragent + '\'' +
                '}';
    }
}
