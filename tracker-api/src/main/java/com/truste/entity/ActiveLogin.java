package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "active_login", schema = "portal")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ActiveLogin implements Serializable {

    @Id
    private String token;

    @Column
    private Date date;

    @Column
    private String ip;

    @Column
    private String user_agent;

    @Id
    private String user_id;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private User user;

    public ActiveLogin() {
    }

    public ActiveLogin(String token, Date date, String ip, String user_agent, String user_id) {
        this.token = token;
        this.date = date;
        this.ip = ip;
        this.user_agent = user_agent;
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUser_agent() {
        return user_agent;
    }

    public void setUser_agent(String user_agent) {
        this.user_agent = user_agent;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActiveLogin that = (ActiveLogin) o;

        if (token != null ? !token.equals(that.token) : that.token != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (ip != null ? !ip.equals(that.ip) : that.ip != null) return false;
        if (user_agent != null ? !user_agent.equals(that.user_agent) : that.user_agent != null) return false;
        return user_id != null ? user_id.equals(that.user_id) : that.user_id == null;
    }

    @Override
    public int hashCode() {
        int result = token != null ? token.hashCode() : 0;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (user_agent != null ? user_agent.hashCode() : 0);
        result = 31 * result + (user_id != null ? user_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ActiveLogin{" +
                "token='" + token + '\'' +
                ", date=" + date +
                ", ip='" + ip + '\'' +
                ", user_agent='" + user_agent + '\'' +
                ", user_id='" + user_id + '\'' +
                '}';
    }
}
