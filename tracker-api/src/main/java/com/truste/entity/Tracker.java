package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "tracker", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Tracker implements Serializable {
    @Column
    private String ttechnique;

    @Column
    private Boolean verified_tracker;

    @Column
    private Boolean evaluated;

    @Id
    private String tracker_id;

    @Column
    private String ttechnique_id;

    @Column
    private String name;

    @OneToOne
    @JoinColumn(name = "ttechnique_id", insertable = false, updatable = false)
    private FirstptyCookie firstptyCookie;

    @OneToOne
    @JoinColumn(name = "ttechnique_id", insertable = false, updatable = false)
    private Html5Storage html5Storage;

    @OneToOne
    @JoinColumn(name = "ttechnique_id", insertable = false, updatable = false)
    private ThirdptyCookie thirdptyCookie;

    @OneToOne
    @JoinColumn(name = "tracker_id", insertable = false, updatable = false)
    private OptOutScope optOutScope;


    public Tracker() {
    }

    public Tracker(String ttechnique, Boolean verified_tracker, Boolean evaluated, String tracker_id, String
            ttechnique_id, String name) {
        this.ttechnique = ttechnique;
        this.verified_tracker = verified_tracker;
        this.evaluated = evaluated;
        this.tracker_id = tracker_id;
        this.ttechnique_id = ttechnique_id;
        this.name = name;
    }

    public String getTtechnique() {
        return ttechnique;
    }

    public void setTtechnique(String ttechnique) {
        this.ttechnique = ttechnique;
    }

    public Boolean getVerified_tracker() {
        return verified_tracker;
    }

    public void setVerified_tracker(Boolean verified_tracker) {
        this.verified_tracker = verified_tracker;
    }

    public Boolean getEvaluated() {
        return evaluated;
    }

    public void setEvaluated(Boolean evaluated) {
        this.evaluated = evaluated;
    }

    public String getTracker_id() {
        return tracker_id;
    }

    public void setTracker_id(String tracker_id) {
        this.tracker_id = tracker_id;
    }

    public String getTtechnique_id() {
        return ttechnique_id;
    }

    public void setTtechnique_id(String ttechnique_id) {
        this.ttechnique_id = ttechnique_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FirstptyCookie getFirstptyCookie() {
        return firstptyCookie;
    }

    public void setFirstptyCookie(FirstptyCookie firstptyCookie) {
        this.firstptyCookie = firstptyCookie;
    }

    public Html5Storage getHtml5Storage() {
        return html5Storage;
    }

    public void setHtml5Storage(Html5Storage html5Storage) {
        this.html5Storage = html5Storage;
    }

    public ThirdptyCookie getThirdptyCookie() {
        return thirdptyCookie;
    }

    public void setThirdptyCookie(ThirdptyCookie thirdptyCookie) {
        this.thirdptyCookie = thirdptyCookie;
    }

    public OptOutScope getOptOutScope() {
        return optOutScope;
    }

    public void setOptOutScope(OptOutScope optOutScope) {
        this.optOutScope = optOutScope;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tracker tracker = (Tracker) o;

        if (ttechnique != null ? !ttechnique.equals(tracker.ttechnique) : tracker.ttechnique != null) return false;
        if (verified_tracker != null ? !verified_tracker.equals(tracker.verified_tracker) : tracker.verified_tracker
                != null)
            return false;
        if (evaluated != null ? !evaluated.equals(tracker.evaluated) : tracker.evaluated != null) return false;
        if (tracker_id != null ? !tracker_id.equals(tracker.tracker_id) : tracker.tracker_id != null) return false;
        if (ttechnique_id != null ? !ttechnique_id.equals(tracker.ttechnique_id) : tracker.ttechnique_id != null)
            return false;
        return name != null ? name.equals(tracker.name) : tracker.name == null;
    }

    @Override
    public int hashCode() {
        int result = ttechnique != null ? ttechnique.hashCode() : 0;
        result = 31 * result + (verified_tracker != null ? verified_tracker.hashCode() : 0);
        result = 31 * result + (evaluated != null ? evaluated.hashCode() : 0);
        result = 31 * result + (tracker_id != null ? tracker_id.hashCode() : 0);
        result = 31 * result + (ttechnique_id != null ? ttechnique_id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tracker{" +
                "ttechnique='" + ttechnique + '\'' +
                ", verified_tracker=" + verified_tracker +
                ", evaluated=" + evaluated +
                ", tracker_id='" + tracker_id + '\'' +
                ", ttechnique_id='" + ttechnique_id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
