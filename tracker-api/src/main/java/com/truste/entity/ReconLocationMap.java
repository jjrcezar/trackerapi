package com.truste.entity;


import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "recon_location_map", schema = "recon", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"recon_table_name", "recon_table_id", "location_url"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconLocationMap implements Serializable {
    @Id
    private String recon_table_name;

    @Id
    private Long recon_table_id;

    @Column
    private String location_domain;

    @Id
    private String location_url;

    @Column
    private String location_chain;

    @Column
    private Date first_found;

    @Column
    private Date last_found;

    public ReconLocationMap() {
    }

    public ReconLocationMap(String recon_table_name, Long recon_table_id, String location_domain, String
            location_url, String location_chain, Date first_found, Date last_found) {
        this.recon_table_name = recon_table_name;
        this.recon_table_id = recon_table_id;
        this.location_domain = location_domain;
        this.location_url = location_url;
        this.location_chain = location_chain;
        this.first_found = first_found;
        this.last_found = last_found;
    }

    public String getRecon_table_name() {
        return recon_table_name;
    }

    public void setRecon_table_name(String recon_table_name) {
        this.recon_table_name = recon_table_name;
    }

    public Long getRecon_table_id() {
        return recon_table_id;
    }

    public void setRecon_table_id(Long recon_table_id) {
        this.recon_table_id = recon_table_id;
    }

    public String getLocation_domain() {
        return location_domain;
    }

    public void setLocation_domain(String location_domain) {
        this.location_domain = location_domain;
    }

    public String getLocation_url() {
        return location_url;
    }

    public void setLocation_url(String location_url) {
        this.location_url = location_url;
    }

    public String getLocation_chain() {
        return location_chain;
    }

    public void setLocation_chain(String location_chain) {
        this.location_chain = location_chain;
    }

    public Date getFirst_found() {
        return first_found;
    }

    public void setFirst_found(Date first_found) {
        this.first_found = first_found;
    }

    public Date getLast_found() {
        return last_found;
    }

    public void setLast_found(Date last_found) {
        this.last_found = last_found;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconLocationMap that = (ReconLocationMap) o;

        if (recon_table_name != null ? !recon_table_name.equals(that.recon_table_name) : that.recon_table_name != null)
            return false;
        if (recon_table_id != null ? !recon_table_id.equals(that.recon_table_id) : that.recon_table_id != null)
            return false;
        if (location_domain != null ? !location_domain.equals(that.location_domain) : that.location_domain != null)
            return false;
        if (location_url != null ? !location_url.equals(that.location_url) : that.location_url != null) return false;
        if (location_chain != null ? !location_chain.equals(that.location_chain) : that.location_chain != null)
            return false;
        if (first_found != null ? !first_found.equals(that.first_found) : that.first_found != null) return false;
        return last_found != null ? last_found.equals(that.last_found) : that.last_found == null;
    }

    @Override
    public int hashCode() {
        int result = recon_table_name != null ? recon_table_name.hashCode() : 0;
        result = 31 * result + (recon_table_id != null ? recon_table_id.hashCode() : 0);
        result = 31 * result + (location_domain != null ? location_domain.hashCode() : 0);
        result = 31 * result + (location_url != null ? location_url.hashCode() : 0);
        result = 31 * result + (location_chain != null ? location_chain.hashCode() : 0);
        result = 31 * result + (first_found != null ? first_found.hashCode() : 0);
        result = 31 * result + (last_found != null ? last_found.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconLocationMap{" +
                "recon_table_name='" + recon_table_name + '\'' +
                ", recon_table_id=" + recon_table_id +
                ", location_domain='" + location_domain + '\'' +
                ", location_url='" + location_url + '\'' +
                ", location_chain='" + location_chain + '\'' +
                ", first_found=" + first_found +
                ", last_found=" + last_found +
                '}';
    }
}
