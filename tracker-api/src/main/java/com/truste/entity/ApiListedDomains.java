package com.truste.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "api_listed_domains", schema = "api")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ApiListedDomains implements Serializable {
    @Id
    private String domain_id;

    @Id
    private String list_name;

    @Column
    private Boolean list_status;

    @Column
    private Date last_changed;

    @Column
    private String comment;

    public ApiListedDomains() {
    }

    public ApiListedDomains(String domain_id, String list_name, Boolean list_status, Date last_changed, String
            comment) {
        this.domain_id = domain_id;
        this.list_name = list_name;
        this.list_status = list_status;
        this.last_changed = last_changed;
        this.comment = comment;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public String getList_name() {
        return list_name;
    }

    public void setList_name(String list_name) {
        this.list_name = list_name;
    }

    public Boolean getList_status() {
        return list_status;
    }

    public void setList_status(Boolean list_status) {
        this.list_status = list_status;
    }

    public Date getLast_changed() {
        return last_changed;
    }

    public void setLast_changed(Date last_changed) {
        this.last_changed = last_changed;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiListedDomains that = (ApiListedDomains) o;

        if (domain_id != null ? !domain_id.equals(that.domain_id) : that.domain_id != null) return false;
        if (list_name != null ? !list_name.equals(that.list_name) : that.list_name != null) return false;
        if (list_status != null ? !list_status.equals(that.list_status) : that.list_status != null) return false;
        if (last_changed != null ? !last_changed.equals(that.last_changed) : that.last_changed != null) return false;
        return comment != null ? comment.equals(that.comment) : that.comment == null;
    }

    @Override
    public int hashCode() {
        int result = domain_id != null ? domain_id.hashCode() : 0;
        result = 31 * result + (list_name != null ? list_name.hashCode() : 0);
        result = 31 * result + (list_status != null ? list_status.hashCode() : 0);
        result = 31 * result + (last_changed != null ? last_changed.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ApiListedDomains{" +
                "domain_id='" + domain_id + '\'' +
                ", list_name='" + list_name + '\'' +
                ", list_status=" + list_status +
                ", last_changed=" + last_changed +
                ", comment='" + comment + '\'' +
                '}';
    }
}
