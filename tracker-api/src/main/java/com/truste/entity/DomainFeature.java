package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "domain_feature", schema = "eu")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainFeature implements Serializable {
    @Id
    private String domain_id;

    @Id
    private String feature_id;

    @ManyToOne
    @JoinColumn(name = "feature_id", insertable = false, updatable = false)
    private Feature feature;

    public DomainFeature() {
    }

    public DomainFeature(String domain_id, String feature_id) {
        this.domain_id = domain_id;
        this.feature_id = feature_id;
    }

    public String getDomain_id() {
        return domain_id;
    }

    public void setDomain_id(String domain_id) {
        this.domain_id = domain_id;
    }

    public String getFeature_id() {
        return feature_id;
    }

    public void setFeature_id(String feature_id) {
        this.feature_id = feature_id;
    }

    public Feature getFeature() {
        return feature;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DomainFeature that = (DomainFeature) o;

        if (domain_id != null ? !domain_id.equals(that.domain_id) : that.domain_id != null) return false;
        return feature_id != null ? feature_id.equals(that.feature_id) : that.feature_id == null;
    }

    @Override
    public int hashCode() {
        int result = domain_id != null ? domain_id.hashCode() : 0;
        result = 31 * result + (feature_id != null ? feature_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DomainFeature{" +
                "domain_id='" + domain_id + '\'' +
                ", feature_id='" + feature_id + '\'' +
                '}';
    }
}
