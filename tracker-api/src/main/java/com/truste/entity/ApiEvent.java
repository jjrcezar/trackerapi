package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "api_event", schema = "api", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"requested_data", "api_command", "api_key_id", "extra_info"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ApiEvent implements Serializable {

    private static final long serialVersionUID = -6525026597509673241L;

    @Id
    private Long id;

    @Column
    private String requested_data;

    @Column
    private String api_command;

    @Column
    private String api_key_id;

    @Column
    private Integer count;

    @Column
    private Date first_time;

    @Column
    private Date last_time;

    @Column
    private String extra_info;

    @ManyToOne
    @JoinColumn(name = "api_key_id", insertable = false, updatable = false)
    private ApiKey apiKey;

    public ApiEvent() {
    }

    public ApiEvent(Long id, String requested_data, String api_command, String api_key_id, Integer count, Date
            first_time, Date last_time, String extra_info) {
        this.id = id;
        this.requested_data = requested_data;
        this.api_command = api_command;
        this.api_key_id = api_key_id;
        this.count = count;
        this.first_time = first_time;
        this.last_time = last_time;
        this.extra_info = extra_info;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequested_data() {
        return requested_data;
    }

    public void setRequested_data(String requested_data) {
        this.requested_data = requested_data;
    }

    public String getApi_command() {
        return api_command;
    }

    public void setApi_command(String api_command) {
        this.api_command = api_command;
    }

    public String getApi_key_id() {
        return api_key_id;
    }

    public void setApi_key_id(String api_key_id) {
        this.api_key_id = api_key_id;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getFirst_time() {
        return first_time;
    }

    public void setFirst_time(Date first_time) {
        this.first_time = first_time;
    }

    public Date getLast_time() {
        return last_time;
    }

    public void setLast_time(Date last_time) {
        this.last_time = last_time;
    }

    public String getExtra_info() {
        return extra_info;
    }

    public void setExtra_info(String extra_info) {
        this.extra_info = extra_info;
    }

    public ApiKey getApiKey() {
        return apiKey;
    }

    public void setApiKey(ApiKey apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiEvent apiEvent = (ApiEvent) o;

        if (id != null ? !id.equals(apiEvent.id) : apiEvent.id != null) return false;
        if (requested_data != null ? !requested_data.equals(apiEvent.requested_data) : apiEvent.requested_data != null)
            return false;
        if (api_command != null ? !api_command.equals(apiEvent.api_command) : apiEvent.api_command != null)
            return false;
        if (api_key_id != null ? !api_key_id.equals(apiEvent.api_key_id) : apiEvent.api_key_id != null) return false;
        if (count != null ? !count.equals(apiEvent.count) : apiEvent.count != null) return false;
        if (first_time != null ? !first_time.equals(apiEvent.first_time) : apiEvent.first_time != null) return false;
        if (last_time != null ? !last_time.equals(apiEvent.last_time) : apiEvent.last_time != null) return false;
        return extra_info != null ? extra_info.equals(apiEvent.extra_info) : apiEvent.extra_info == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (requested_data != null ? requested_data.hashCode() : 0);
        result = 31 * result + (api_command != null ? api_command.hashCode() : 0);
        result = 31 * result + (api_key_id != null ? api_key_id.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        result = 31 * result + (first_time != null ? first_time.hashCode() : 0);
        result = 31 * result + (last_time != null ? last_time.hashCode() : 0);
        result = 31 * result + (extra_info != null ? extra_info.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ApiEvent{" +
                "id=" + id +
                ", requested_data='" + requested_data + '\'' +
                ", api_command='" + api_command + '\'' +
                ", api_key_id='" + api_key_id + '\'' +
                ", count=" + count +
                ", first_time=" + first_time +
                ", last_time=" + last_time +
                ", extra_info='" + extra_info + '\'' +
                '}';
    }
}