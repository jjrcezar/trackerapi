package com.truste.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "oo_applicability", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OoApplicability implements Serializable {
    @Id
    private String ooscope_id;

    @Id
    private String ooid;

    public OoApplicability() {
    }

    public OoApplicability(String ooscope_id, String ooid) {
        this.ooscope_id = ooscope_id;
        this.ooid = ooid;
    }

    public String getOoscope_id() {
        return ooscope_id;
    }

    public void setOoscope_id(String ooscope_id) {
        this.ooscope_id = ooscope_id;
    }

    public String getOoid() {
        return ooid;
    }

    public void setOoid(String ooid) {
        this.ooid = ooid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OoApplicability that = (OoApplicability) o;

        if (ooscope_id != null ? !ooscope_id.equals(that.ooscope_id) : that.ooscope_id != null) return false;
        return ooid != null ? ooid.equals(that.ooid) : that.ooid == null;
    }

    @Override
    public int hashCode() {
        int result = ooscope_id != null ? ooscope_id.hashCode() : 0;
        result = 31 * result + (ooid != null ? ooid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OoApplicability{" +
                "ooscope_id='" + ooscope_id + '\'' +
                ", ooid='" + ooid + '\'' +
                '}';
    }
}
