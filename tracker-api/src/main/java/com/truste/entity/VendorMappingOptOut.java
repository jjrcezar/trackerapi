package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "vendor_mapping_opt_out", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class VendorMappingOptOut implements Serializable {
    @Id
    private String vendor_mapping_ooid;

    @Column
    private String partner_ooscope_id;

    @Column
    private String vendor_ooscope_id;

    @ManyToOne
    @JoinColumns({
//            @JoinColumn(name = "partner_ooscope_id", referencedColumnName = "ooscope_id", insertable = false, updatable = false),
            @JoinColumn(name = "vendor_ooscope_id", referencedColumnName = "ooscope_id", insertable = false, updatable = false)})
    private OptOutScope optOutScope;

    public VendorMappingOptOut() {
    }

    public VendorMappingOptOut(String vendor_mapping_ooid, String partner_ooscope_id, String vendor_ooscope_id) {
        this.vendor_mapping_ooid = vendor_mapping_ooid;
        this.partner_ooscope_id = partner_ooscope_id;
        this.vendor_ooscope_id = vendor_ooscope_id;
    }

    public String getVendor_mapping_ooid() {
        return vendor_mapping_ooid;
    }

    public void setVendor_mapping_ooid(String vendor_mapping_ooid) {
        this.vendor_mapping_ooid = vendor_mapping_ooid;
    }

    public String getPartner_ooscope_id() {
        return partner_ooscope_id;
    }

    public void setPartner_ooscope_id(String partner_ooscope_id) {
        this.partner_ooscope_id = partner_ooscope_id;
    }

    public String getVendor_ooscope_id() {
        return vendor_ooscope_id;
    }

    public void setVendor_ooscope_id(String vendor_ooscope_id) {
        this.vendor_ooscope_id = vendor_ooscope_id;
    }

    public OptOutScope getOptOutScope() {
        return optOutScope;
    }

    public void setOptOutScope(OptOutScope optOutScope) {
        this.optOutScope = optOutScope;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VendorMappingOptOut that = (VendorMappingOptOut) o;

        if (vendor_mapping_ooid != null ? !vendor_mapping_ooid.equals(that.vendor_mapping_ooid) : that
                .vendor_mapping_ooid != null)
            return false;
        if (partner_ooscope_id != null ? !partner_ooscope_id.equals(that.partner_ooscope_id) : that
                .partner_ooscope_id != null)
            return false;
        return vendor_ooscope_id != null ? vendor_ooscope_id.equals(that.vendor_ooscope_id) : that.vendor_ooscope_id
                == null;
    }

    @Override
    public int hashCode() {
        int result = vendor_mapping_ooid != null ? vendor_mapping_ooid.hashCode() : 0;
        result = 31 * result + (partner_ooscope_id != null ? partner_ooscope_id.hashCode() : 0);
        result = 31 * result + (vendor_ooscope_id != null ? vendor_ooscope_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "VendorMappingOptOut{" +
                "vendor_mapping_ooid='" + vendor_mapping_ooid + '\'' +
                ", partner_ooscope_id='" + partner_ooscope_id + '\'' +
                ", vendor_ooscope_id='" + vendor_ooscope_id + '\'' +
                '}';
    }
}
