package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "recon_framechain", schema = "recon", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name", "day", "frame_chain_csv"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconFramechain implements Serializable {
    @Column
    private String name;

    @Column
    private String frame_chain_csv;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date day;

    @Column
    private Long count;

    public ReconFramechain() {
    }

    public ReconFramechain(String name, String frame_chain_csv, Long id, Date day, Long count) {
        this.name = name;
        this.frame_chain_csv = frame_chain_csv;
        this.id = id;
        this.day = day;
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFrame_chain_csv() {
        return frame_chain_csv;
    }

    public void setFrame_chain_csv(String frame_chain_csv) {
        this.frame_chain_csv = frame_chain_csv;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconFramechain that = (ReconFramechain) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (frame_chain_csv != null ? !frame_chain_csv.equals(that.frame_chain_csv) : that.frame_chain_csv != null)
            return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (day != null ? !day.equals(that.day) : that.day != null) return false;
        return count != null ? count.equals(that.count) : that.count == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (frame_chain_csv != null ? frame_chain_csv.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (day != null ? day.hashCode() : 0);
        result = 31 * result + (count != null ? count.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconFramechain{" +
                "name='" + name + '\'' +
                ", frame_chain_csv='" + frame_chain_csv + '\'' +
                ", id=" + id +
                ", day=" + day +
                ", count=" + count +
                '}';
    }
}
