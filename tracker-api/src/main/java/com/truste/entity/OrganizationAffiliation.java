package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "organization_affiliation", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OrganizationAffiliation implements Serializable {
    @Id
    private String affiliation_id;

    @Id
    private String organization_id;

    @ManyToOne
    @JoinColumn(name = "organization_id", insertable = false, updatable = false)
    private Organization organization;

    @ManyToOne
    @JoinColumn(name = "affiliation_id", insertable = false, updatable = false)
    private Affiliation affiliation;

    public OrganizationAffiliation() {
    }

    public OrganizationAffiliation(String affiliation_id, String organization_id) {
        this.affiliation_id = affiliation_id;
        this.organization_id = organization_id;
    }

    public String getAffiliation_id() {
        return affiliation_id;
    }

    public void setAffiliation_id(String affiliation_id) {
        this.affiliation_id = affiliation_id;
    }

    public String getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(String organization_id) {
        this.organization_id = organization_id;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public Affiliation getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(Affiliation affiliation) {
        this.affiliation = affiliation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrganizationAffiliation that = (OrganizationAffiliation) o;

        if (affiliation_id != null ? !affiliation_id.equals(that.affiliation_id) : that.affiliation_id != null)
            return false;
        return organization_id != null ? organization_id.equals(that.organization_id) : that.organization_id == null;
    }

    @Override
    public int hashCode() {
        int result = affiliation_id != null ? affiliation_id.hashCode() : 0;
        result = 31 * result + (organization_id != null ? organization_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrganizationAffiliation{" +
                "affiliation_id='" + affiliation_id + '\'' +
                ", organization_id='" + organization_id + '\'' +
                '}';
    }
}
