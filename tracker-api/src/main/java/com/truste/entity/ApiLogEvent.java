package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "api_log_event", schema = "api")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ApiLogEvent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String host;

    @Column
    private String authority;

    @Column
    private String caller;

    @Column
    private Boolean iserror;

    @Column
    private String info;

    @Column
    private String action;

    @Column
    private Date created;

    public ApiLogEvent() {
    }

    public ApiLogEvent(String host, String authority, String caller, Boolean iserror, String info, String action,
            Date created) {
        this.host = host;
        this.authority = authority;
        this.caller = caller;
        this.iserror = iserror;
        this.info = info;
        this.action = action;
        this.created = created;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getCaller() {
        return caller;
    }

    public void setCaller(String caller) {
        this.caller = caller;
    }

    public Boolean getIserror() {
        return iserror;
    }

    public void setIserror(Boolean iserror) {
        this.iserror = iserror;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiLogEvent that = (ApiLogEvent) o;

        if (host != null ? !host.equals(that.host) : that.host != null) return false;
        if (authority != null ? !authority.equals(that.authority) : that.authority != null) return false;
        if (caller != null ? !caller.equals(that.caller) : that.caller != null) return false;
        if (iserror != null ? !iserror.equals(that.iserror) : that.iserror != null) return false;
        if (info != null ? !info.equals(that.info) : that.info != null) return false;
        if (action != null ? !action.equals(that.action) : that.action != null) return false;
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = host != null ? host.hashCode() : 0;
        result = 31 * result + (authority != null ? authority.hashCode() : 0);
        result = 31 * result + (caller != null ? caller.hashCode() : 0);
        result = 31 * result + (iserror != null ? iserror.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ApiLogEvent{" +
                "host='" + host + '\'' +
                ", authority='" + authority + '\'' +
                ", caller='" + caller + '\'' +
                ", iserror=" + iserror +
                ", info='" + info + '\'' +
                ", action='" + action + '\'' +
                ", created=" + created +
                '}';
    }
}
