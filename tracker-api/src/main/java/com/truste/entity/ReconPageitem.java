package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "recon_pageitem", schema = "recon", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name", "page_url", "domain"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ReconPageitem implements Serializable {

    @Column
    private Boolean evaluated;

    @Column
    private String name;

    @Column
    private String value;

    @Column
    private String domain;

    @Column
    private Date collected;

    @Column
    private String page_url;

    @Column
    private String collectedby;

    @Column
    private Date created;

    @Column
    private Date updated;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String use;

    public ReconPageitem() {
    }

    public ReconPageitem(Boolean evaluated, String name, String value, String domain, Date collected, String
            page_url, String collectedby, Date created, Date updated, Long id, String use) {
        this.evaluated = evaluated;
        this.name = name;
        this.value = value;
        this.domain = domain;
        this.collected = collected;
        this.page_url = page_url;
        this.collectedby = collectedby;
        this.created = created;
        this.updated = updated;
        this.id = id;
        this.use = use;
    }

    public Boolean getEvaluated() {
        return evaluated;
    }

    public void setEvaluated(Boolean evaluated) {
        this.evaluated = evaluated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Date getCollected() {
        return collected;
    }

    public void setCollected(Date collected) {
        this.collected = collected;
    }

    public String getPage_url() {
        return page_url;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public String getCollectedby() {
        return collectedby;
    }

    public void setCollectedby(String collectedby) {
        this.collectedby = collectedby;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReconPageitem that = (ReconPageitem) o;

        if (evaluated != null ? !evaluated.equals(that.evaluated) : that.evaluated != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (domain != null ? !domain.equals(that.domain) : that.domain != null) return false;
        if (collected != null ? !collected.equals(that.collected) : that.collected != null) return false;
        if (page_url != null ? !page_url.equals(that.page_url) : that.page_url != null) return false;
        if (collectedby != null ? !collectedby.equals(that.collectedby) : that.collectedby != null) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        return use != null ? use.equals(that.use) : that.use == null;
    }

    @Override
    public int hashCode() {
        int result = evaluated != null ? evaluated.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (domain != null ? domain.hashCode() : 0);
        result = 31 * result + (collected != null ? collected.hashCode() : 0);
        result = 31 * result + (page_url != null ? page_url.hashCode() : 0);
        result = 31 * result + (collectedby != null ? collectedby.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (use != null ? use.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ReconPageitem{" +
                "evaluated=" + evaluated +
                ", name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", domain='" + domain + '\'' +
                ", collected=" + collected +
                ", page_url='" + page_url + '\'' +
                ", collectedby='" + collectedby + '\'' +
                ", created=" + created +
                ", updated=" + updated +
                ", id=" + id +
                ", use='" + use + '\'' +
                '}';
    }
}
