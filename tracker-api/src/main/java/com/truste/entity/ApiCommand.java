package com.truste.entity;

import net.sf.ehcache.transaction.xa.commands.Command;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "api_command", schema = "api")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ApiCommand implements Serializable {

    private static final long serialVersionUID = -8300622377176554634L;

    @Id
    private String api_command_id;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private String help;

    @Column
    private String isactive;

    @OneToMany(mappedBy = "apiCommand")
    private Set<CommandWhitelist> commandWhitelists = new HashSet<CommandWhitelist>();

    public ApiCommand() {
    }

    public ApiCommand(String api_command_id, String name, String description, String help, String isactive) {
        this.api_command_id = api_command_id;
        this.name = name;
        this.description = description;
        this.help = help;
        this.isactive = isactive;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getApi_command_id() {
        return api_command_id;
    }

    public void setApi_command_id(String api_command_id) {
        this.api_command_id = api_command_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public Set<CommandWhitelist> getCommandWhitelists() {
        return commandWhitelists;
    }

    public void setCommandWhitelists(Set<CommandWhitelist> commandWhitelists) {
        this.commandWhitelists = commandWhitelists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApiCommand that = (ApiCommand) o;

        if (api_command_id != null ? !api_command_id.equals(that.api_command_id) : that.api_command_id != null)
            return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (help != null ? !help.equals(that.help) : that.help != null) return false;
        return isactive != null ? isactive.equals(that.isactive) : that.isactive == null;
    }

    @Override
    public int hashCode() {
        int result = api_command_id != null ? api_command_id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (help != null ? help.hashCode() : 0);
        result = 31 * result + (isactive != null ? isactive.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ApiCommand{" +
                "api_command_id='" + api_command_id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", help='" + help + '\'' +
                ", isactive='" + isactive + '\'' +
                '}';
    }
}