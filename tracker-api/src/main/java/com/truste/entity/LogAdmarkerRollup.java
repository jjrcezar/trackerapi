package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "log_admarker_rollup", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LogAdmarkerRollup implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private Date start_time;

    @Column
    private Date end_time;

    @Column
    private String partner_id;

    @Column
    private String advertiser_id;

    @Column
    private String campaign_id;

    @Column
    private String ad_size;

    @Column
    private Long impressions;

    @Column
    private Long clicks;

    public LogAdmarkerRollup() {
    }

    public LogAdmarkerRollup(Date start_time, Date end_time, String partner_id, String advertiser_id, String
            campaign_id, String ad_size, Long impressions, Long clicks) {
        this.start_time = start_time;
        this.end_time = end_time;
        this.partner_id = partner_id;
        this.advertiser_id = advertiser_id;
        this.campaign_id = campaign_id;
        this.ad_size = ad_size;
        this.impressions = impressions;
        this.clicks = clicks;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getAdvertiser_id() {
        return advertiser_id;
    }

    public void setAdvertiser_id(String advertiser_id) {
        this.advertiser_id = advertiser_id;
    }

    public String getCampaign_id() {
        return campaign_id;
    }

    public void setCampaign_id(String campaign_id) {
        this.campaign_id = campaign_id;
    }

    public String getAd_size() {
        return ad_size;
    }

    public void setAd_size(String ad_size) {
        this.ad_size = ad_size;
    }

    public Long getImpressions() {
        return impressions;
    }

    public void setImpressions(Long impressions) {
        this.impressions = impressions;
    }

    public Long getClicks() {
        return clicks;
    }

    public void setClicks(Long clicks) {
        this.clicks = clicks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogAdmarkerRollup that = (LogAdmarkerRollup) o;

        if (start_time != null ? !start_time.equals(that.start_time) : that.start_time != null) return false;
        if (end_time != null ? !end_time.equals(that.end_time) : that.end_time != null) return false;
        if (partner_id != null ? !partner_id.equals(that.partner_id) : that.partner_id != null) return false;
        if (advertiser_id != null ? !advertiser_id.equals(that.advertiser_id) : that.advertiser_id != null)
            return false;
        if (campaign_id != null ? !campaign_id.equals(that.campaign_id) : that.campaign_id != null) return false;
        if (ad_size != null ? !ad_size.equals(that.ad_size) : that.ad_size != null) return false;
        if (impressions != null ? !impressions.equals(that.impressions) : that.impressions != null) return false;
        return clicks != null ? clicks.equals(that.clicks) : that.clicks == null;
    }

    @Override
    public int hashCode() {
        int result = start_time != null ? start_time.hashCode() : 0;
        result = 31 * result + (end_time != null ? end_time.hashCode() : 0);
        result = 31 * result + (partner_id != null ? partner_id.hashCode() : 0);
        result = 31 * result + (advertiser_id != null ? advertiser_id.hashCode() : 0);
        result = 31 * result + (campaign_id != null ? campaign_id.hashCode() : 0);
        result = 31 * result + (ad_size != null ? ad_size.hashCode() : 0);
        result = 31 * result + (impressions != null ? impressions.hashCode() : 0);
        result = 31 * result + (clicks != null ? clicks.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LogAdmarkerRollup{" +
                "start_time=" + start_time +
                ", end_time=" + end_time +
                ", partner_id='" + partner_id + '\'' +
                ", advertiser_id='" + advertiser_id + '\'' +
                ", campaign_id='" + campaign_id + '\'' +
                ", ad_size='" + ad_size + '\'' +
                ", impressions=" + impressions +
                ", clicks=" + clicks +
                '}';
    }
}
