package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "tracker_scope", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"scope_type", "scope_type_id"})})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TrackerScope implements Serializable {
    @Column
    private String scope_type;

    @Column
    private String scope_type_id;

    @Id
    private String tscope_id;

    @Column
    private String parent_tscope_id;

    @OneToMany(mappedBy = "trackerScope")
    private Set<TscopeCategory> tscopeCategories = new HashSet<TscopeCategory>();

    public TrackerScope() {
    }

    public TrackerScope(String scope_type, String scope_type_id, String tscope_id, String parent_tscope_id) {
        this.scope_type = scope_type;
        this.scope_type_id = scope_type_id;
        this.tscope_id = tscope_id;
        this.parent_tscope_id = parent_tscope_id;
    }

    public String getScope_type() {
        return scope_type;
    }

    public void setScope_type(String scope_type) {
        this.scope_type = scope_type;
    }

    public String getScope_type_id() {
        return scope_type_id;
    }

    public void setScope_type_id(String scope_type_id) {
        this.scope_type_id = scope_type_id;
    }

    public String getTscope_id() {
        return tscope_id;
    }

    public void setTscope_id(String tscope_id) {
        this.tscope_id = tscope_id;
    }

    public String getParent_tscope_id() {
        return parent_tscope_id;
    }

    public void setParent_tscope_id(String parent_tscope_id) {
        this.parent_tscope_id = parent_tscope_id;
    }

    public Set<TscopeCategory> getTscopeCategories() {
        return tscopeCategories;
    }

    public void setTscopeCategories(Set<TscopeCategory> tscopeCategories) {
        this.tscopeCategories = tscopeCategories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrackerScope that = (TrackerScope) o;

        if (scope_type != null ? !scope_type.equals(that.scope_type) : that.scope_type != null) return false;
        if (scope_type_id != null ? !scope_type_id.equals(that.scope_type_id) : that.scope_type_id != null)
            return false;
        if (tscope_id != null ? !tscope_id.equals(that.tscope_id) : that.tscope_id != null) return false;
        return parent_tscope_id != null ? parent_tscope_id.equals(that.parent_tscope_id) : that.parent_tscope_id ==
                null;
    }

    @Override
    public int hashCode() {
        int result = scope_type != null ? scope_type.hashCode() : 0;
        result = 31 * result + (scope_type_id != null ? scope_type_id.hashCode() : 0);
        result = 31 * result + (tscope_id != null ? tscope_id.hashCode() : 0);
        result = 31 * result + (parent_tscope_id != null ? parent_tscope_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TrackerScope{" +
                "scope_type='" + scope_type + '\'' +
                ", scope_type_id='" + scope_type_id + '\'' +
                ", tscope_id='" + tscope_id + '\'' +
                ", parent_tscope_id='" + parent_tscope_id + '\'' +
                '}';
    }
}
