package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "tscope_category", schema = "ads")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TscopeCategory implements Serializable {
    @Id
    private String category_id;

    @Id
    private String tscope_id;

    @Column
    private Boolean isprimary;

    @ManyToOne
    @JoinColumn(name = "tscope_id", insertable = false, updatable = false)
    private TrackerScope trackerScope;

    public TscopeCategory() {
    }

    public TscopeCategory(String category_id, String tscope_id, Boolean isprimary) {
        this.category_id = category_id;
        this.tscope_id = tscope_id;
        this.isprimary = isprimary;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getTscope_id() {
        return tscope_id;
    }

    public void setTscope_id(String tscope_id) {
        this.tscope_id = tscope_id;
    }

    public Boolean getIsprimary() {
        return isprimary;
    }

    public void setIsprimary(Boolean isprimary) {
        this.isprimary = isprimary;
    }

    public TrackerScope getTrackerScope() {
        return trackerScope;
    }

    public void setTrackerScope(TrackerScope trackerScope) {
        this.trackerScope = trackerScope;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TscopeCategory that = (TscopeCategory) o;

        if (category_id != null ? !category_id.equals(that.category_id) : that.category_id != null) return false;
        if (tscope_id != null ? !tscope_id.equals(that.tscope_id) : that.tscope_id != null) return false;
        return isprimary != null ? isprimary.equals(that.isprimary) : that.isprimary == null;
    }

    @Override
    public int hashCode() {
        int result = category_id != null ? category_id.hashCode() : 0;
        result = 31 * result + (tscope_id != null ? tscope_id.hashCode() : 0);
        result = 31 * result + (isprimary != null ? isprimary.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TscopeCategory{" +
                "category_id='" + category_id + '\'' +
                ", tscope_id='" + tscope_id + '\'' +
                ", isprimary=" + isprimary +
                '}';
    }
}
