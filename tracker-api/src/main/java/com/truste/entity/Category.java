package com.truste.entity;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "category", schema = "ads", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Category {
    @Column
    private String name;

    @Id
    private String category_id;

    @Column
    private String description;

    @OneToMany(mappedBy = "category")
    private Set<OoscopeCategory> ooscopeCategories = new HashSet<OoscopeCategory>();

    public Category() {
    }

    public Category(String name, String category_id, String description) {
        this.name = name;
        this.category_id = category_id;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<OoscopeCategory> getOoscopeCategories() {
        return ooscopeCategories;
    }

    public void setOoscopeCategories(Set<OoscopeCategory> ooscopeCategories) {
        this.ooscopeCategories = ooscopeCategories;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (name != null ? !name.equals(category.name) : category.name != null) return false;
        if (category_id != null ? !category_id.equals(category.category_id) : category.category_id != null)
            return false;
        return description != null ? description.equals(category.description) : category.description == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (category_id != null ? category_id.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", category_id='" + category_id + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
