package com.truste.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@Table(name = "dataloadtrackerdb0203_affiliation", schema = "import")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Dataloadtrackerdb0203Affiliation implements Serializable {
    @Id
    private String organization;

    @Id
    private String affiliation;

    public Dataloadtrackerdb0203Affiliation() {
    }

    public Dataloadtrackerdb0203Affiliation(String organization, String affiliation) {
        this.organization = organization;
        this.affiliation = affiliation;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dataloadtrackerdb0203Affiliation that = (Dataloadtrackerdb0203Affiliation) o;

        if (organization != null ? !organization.equals(that.organization) : that.organization != null) return false;
        return affiliation != null ? affiliation.equals(that.affiliation) : that.affiliation == null;
    }

    @Override
    public int hashCode() {
        int result = organization != null ? organization.hashCode() : 0;
        result = 31 * result + (affiliation != null ? affiliation.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Dataloadtrackerdb0203Affiliation{" +
                "organization='" + organization + '\'' +
                ", affiliation='" + affiliation + '\'' +
                '}';
    }
}
